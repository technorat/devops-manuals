# Vaults

## Что такое Vaults и какие бывают?

Vault - это инструмент для управления секретами, который помогает организациям безопасно управлять, хранить и использовать конфиденциальные данные, такие как пароли, ключи API и сертификаты. 
Он обеспечивает централизованное хранение и выдачу секретов для различных приложений и сервисов, а также предоставляет дополнительные функции, такие как аудит, мониторинг и контроль доступа. 

## Какие есть известные продукты с этой функцией?

* Hashicorp Vault
* Onboardbase (Saas)
* GitHub Secrets - басплатная фича в GitHub с ограничениями по количеству секретов
* AWS Secrets Manager - в облачной инфраструктуре AWS. $0.4 за секрет в месяц
* Akeyless - (Saas)
* Kubernetes Secrets
* Infisical
* Mozilla SOPS
* Cyberark Conjur


## Hashicorp Vault

Hashicorp Vault — это инструмент управления секретами с открытым исходным кодом для команд разработчиков, но он может быть не идеальным для малого и среднего бизнеса.
Во-первых, установка требует множества шагов настройки: эксперт DevOps настраивает группы пользователей, дает инструкции другим разработчикам по извлечению секретов в зависимости от доступных 
методов аутентификации и управляет ключами шифрования. Вряд ли вы сможете сделать это самостоятельно, не обладая техническими знаниями и подвергая свою компанию риску, а пользовательский 
интерфейс не особо помогает в командной совместной работе. Хотя Vault бесплатен, если вы выбираете самоуправляемый маршрут, вам также потребуются технические ноу-хау для его настройки и 
управления. Облачный вариант стоит дороже по сравнению с альтернативами: цена начинается от 0,5 доллара в час. В обоих случаях вам понадобится выделенный бюджет и выделенные ресурсы, чтобы все 
заработало.

## [Onboardbase (Saas)](https://onboardbase.com)

Onboardbase — это SaaS для безопасного хранения секретов и конфиденциальных данных. Он создан с учетом командной работы и гибкости в различных средах развертывания. Onboardbase бесплатен для 
двух товарищей по команде, затем 75 долларов в месяц и выше для дальнейшего использования с неограниченным количеством товарищей по команде.

**Функции**
* Секретное хранилище с ролевым контролем доступа и шифрованием E2E.
* Совместный пользовательский интерфейс
* Доступ через CLI и API
* Предотвращение утечек в режиме реального времени с мониторингом использования секретов, управлением устройствами и обнаружением секретов кодовой базы.
* Обмен секретами через одноразовые ссылки в чате

**Плюсы**
* Однострочная команда установки из любого репозитория проекта
* Самостоятельный вариант
* Требуются низкие технические знания
* Отличный опыт разработчика

В отличие от Hashicorp Vault, Onboardbase — это универсальное решение, созданное для стартапов, которым нужен простой и простой способ защитить свои рабочие процессы разработки. Его 
дружественный интерфейс позволяет легко делиться секретами с любым членом команды, но не дайте себя обмануть: это также мощный интерфейс командной строки и API, которые можно интегрировать 
в любой инструмент DevOps, будь то на локальном компьютере или в производственной среде. Onboardbase также автоматически обрабатывает конфигурации среды.

## GitHub Secrets - басплатная фича в GitHub с ограничениями по количеству секретов

GitHub Secrets — это функция Github для безопасного хранения конфиденциальной информации в репозитории. Вы также можете определить секреты на уровне организации. Любой, у кого есть доступ к 
репозиториям, может получить доступ к секретам. GitHub Secrets — бесплатная функция, но вы ограничены 1000 секретами организации, 100 секретами репозитория и 100 секретами среды. Размер не 
может превышать 48 КБ.

**Функции**

* Доступно для использования в рабочих процессах GitHub Actions и кодовых пространствах Github.
* Шифрует секреты в безопасном хранилище.
* Политики доступа, позволяющие контролировать, какие репозитории могут использовать секреты организации. Требуется одобрение.
* Секреты уровня организации для обмена секретами между несколькими репозиториями.
* Доступ к секретам можно получить через CLI и API.

**Плюсы**

* Простота использования с репозиториями GitHub.
* Нечего устанавливать
* Бесплатно
* Возможное программное извлечение для использования секретов за пределами Github с использованием CLI и API.
* Бесплатное секретное сканирование для предотвращения утечек

**Минусы**

* Графический интерфейс не создан для удобного командного сотрудничества: нет детального контроля доступа со стороны отдельного участника.
* Лимит хранения
* Ограниченные функции CLI (список, создание, удаление)
* Никакого управления средой: вам нужно создавать новые секреты для каждой среды, поэтому их непросто использовать, например, на локальном компьютере.
* Вы не можете напрямую вводить секреты в команду сборки, если не используете действия Github или кодовые пространства. Вам нужно будет создать собственное решение.

Поскольку большая часть программного обеспечения находится в репозитории git на Github, Github Secrets предлагает гораздо более простой способ защитить секреты, чем Hashicorp Vault. Любой 
разработчик знаком с Github, а настройка и использование секретов занимает всего несколько минут.

## AWS Secrets Manager - в облачной инфраструктуре AWS. $0.4 за секрет в месяц

GitHub Secrets — это функция Github для безопасного хранения конфиденциальной информации в репозитории. Вы также можете определить секреты на уровне организации. Любой, у кого есть доступ к 
репозиториям, может получить доступ к секретам. GitHub Secrets — бесплатная функция, но вы ограничены 1000 секретами организации, 100 секретами репозитория и 100 секретами среды. Размер не 
может превышать 48 КБ.

**Функции**

* Доступно для использования в рабочих процессах GitHub Actions и кодовых пространствах Github.
* Шифрует секреты в безопасном хранилище.
* Политики доступа, позволяющие контролировать, какие репозитории могут использовать секреты организации. Требуется одобрение.
* Секреты уровня организации для обмена секретами между несколькими репозиториями.
* Доступ к секретам можно получить через CLI и API.

**Плюсы**

* Простота использования с репозиториями GitHub.
* Нечего устанавливать
* Бесплатно
* Возможное программное извлечение для использования секретов за пределами Github с использованием CLI и API.
* Бесплатное секретное сканирование для предотвращения утечек

**Минусы**

* Графический интерфейс не создан для удобного командного сотрудничества: нет детального контроля доступа со стороны отдельного участника.
* Лимит хранения
* Ограниченные функции CLI (список, создание, удаление)
* Никакого управления средой: вам нужно создавать новые секреты для каждой среды, поэтому их непросто использовать, например, на локальном компьютере.
* Вы не можете напрямую вводить секреты в команду сборки, если не используете действия Github или кодовые пространства. Вам нужно будет создать собственное решение.

Поскольку большая часть программного обеспечения находится в репозитории git на Github, Github Secrets предлагает гораздо более простой способ защитить секреты, чем Hashicorp Vault. Любой 
разработчик знаком с Github, а настройка и использование секретов занимает всего несколько минут.

## Akeyless - (Saas)

Akeyless — это еще одно универсальное SaaS-решение, предлагающее унифицированное управление секретами для централизованной защиты сертификатов, учетных данных и ключей, но с акцентом на 
дополнительные функции корпоративного уровня. Бесплатно до 5 мест, затем 15 долларов в месяц за место.

**Функции**

* Магазин секретов
* Автоматическая ротация и истечение срока действия учетных данных
* Обмен секретами
* Расширения для управления паролями и базой данных + шифрование файлов
* API + CLI + веб-панель

**Плюсы**

* Шифрование с нулевым разглашением
* Автоматическая миграция из Hashicorp Vault
* Обширная документация
* Самостоятельный вариант
* Соответствие международным стандартам (SOC 2 Type II, ISO 27001)

**Минусы**

* Дороже, чем у конкурентов для больших команд
* Не с открытым исходным кодом

Akeyless представляет собой корпоративную альтернативу Hashicorp Vault, которую гораздо проще использовать для разработчиков. Это также дает возможность делиться секретами с коллегами через 
временные ссылки, но веб-панель, похоже, не предназначена для подключения всей вашей команды.

## Kubernetes Secrets

Kubernetes, также известный как K8s, — это система с открытым исходным кодом для автоматизации развертывания, масштабирования и управления контейнерными приложениями. K8s Secrets — это функция 
для управления конфиденциальными данными, такими как пароль, токен или ключ API. Бесплатное использование и размещение. Вы платите за веб-серверы или управляемые службы для размещения 
Kubernetes.

**Функции**

* Управляйте секретами приложений через CLI, API или файлы конфигурации.
* Функции Devops для непрерывного развертывания и интеграции ваших приложений, размещенных на sef.

**Плюсы**

* Открытый исходный код и готовность к производству
* Независимость от любого облачного провайдера
* Самое легкое решение

**Минусы**

* Требуются дополнительные технические знания для настройки и обслуживания.
* Нет функций для совместной работы и обмена секретами.

Kubernetes Secrets — это, вероятно, самая легкая и безопасная для конфиденциальности альтернатива Hashicorp Vault, если вы используете Kubernetes для развертывания программного обеспечения в 
большом масштабе. Несмотря на то, что вам, вероятно, потребуется бэкэнд-инженер для его настройки, его все равно на порядок проще поддерживать, чем Hashicorp Vault, поскольку он напрямую 
интегрирован в ваш конвейер CI/CD.

## [Infisical](https://infisical.com/)

Infisical — это платформа управления секретами с открытым исходным кодом. Он предоставляет комплексный набор инструментов, охватывающих все аспекты управления секретами: от безопасного 
хранения секретов с контролем версий до ротации секретов, интеграции в инфраструктуре, секретного сканирования и предотвращения утечек.

**Функции**

* открытый исходный код под лицензией MIT
* неограниченное количество экземпляров, пользователей, проектов, сред, секретов и т. д.
* базовый контроль доступа;
* все способы доступа к секретам, включая CLI, API, SDK, Webhooks, Agent и т. д.
* интеграция с Kubernetes, Terraform, Ansible и т. д.
* интеграция с инструментами CI/CD, такими как GitHub Actions, Circle CI, Jenkins и т. д.
* интеграция с облачными платформами, такими как Vercel, Heroku и другими секретными менеджерами;
* секретные ссылки, импорт и переопределение;
* секретное сканирование и предотвращение утечек.

Другие более продвинутые функции, такие как репликация в нескольких регионах или настраиваемое управление доступом на основе ролей, доступны через корпоративную лицензию

## Mozilla SOPS

Mozilla SOPS (Secrets OperationS) — это инструмент, который обеспечивает простой и безопасный способ управления секретами (такими как ключи API, пароли или сертификаты) в ваших проектах. Он 
предназначен для упрощения процесса шифрования и дешифрования файлов, обеспечивая безопасность конфиденциальных данных. Это не полноценный менеджер секретов, а скорее инструмент для хранения 
конфиденциальных данных в зашифрованных файлах. SOPS хвалят за простоту и легкость настройки. Он хорошо интегрируется с инструментами конвейера CI/CD, а его метод хранения файлов в 
зашифрованном виде на диске и их расшифровки в оперативной памяти для редактирования считается безопасной и удобной функцией. SOPS может интегрироваться с облачными сервисами, такими как AWS 
KMS, GCP KMS и Azure Key Vault, для управления ключами. Это означает, что вы можете использовать эти службы для контроля доступа к ключам шифрования. Хотя это быстрое и дешевое решение для 
хранения конфиденциальных данных, SOPS имеет свои ограничения, особенно когда речь идет о доступе к секретам через API. Он не предназначен для сложных задач управления секретами и может не 
подойти для расширенных вариантов использования. Будучи открытым исходным кодом, SOPS можно использовать бесплатно, что делает его доступным для широкого круга проектов, особенно для тех, 
которые стремятся минимизировать затраты. Из-за сокращения бюджета Mozilla SOPS в настоящее время активно не поддерживается, что может привести к неустраненным ошибкам и проблемам безопасности. Следует отметить, что в настоящее время проект передается в другую организацию, что может прояснить его будущее.

## Cyberark Conjur

