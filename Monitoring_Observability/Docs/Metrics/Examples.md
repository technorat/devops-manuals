# Примеры запросов метрик

## PromQL (Prometheus)

| Запрос | Описание | Примечание |
|---|---|---|
| up | Запрос показывает, живой сервер или нет (на основе удачного скрейпинга) | Метрика формируется сервером автоматически |
| container_cpu_load_average_10s{name=~".+"} |  |  |
| container_tasks_state{ip_address="10.125.6.191"} |  |  |
| container_tasks_state{ip_address="10.125.6.191", state!="0"} |  |  |
| (((count(count(node_cpu_seconds_total{instance="$node",job="$job"}) by (cpu))) - avg(sum by (mode)(irate(node_cpu_seconds_total{mode='idle',instance="$node",job="$job"}[5m])))) * 100) / count(count(node_cpu_seconds_total{instance="$node",job="$job"}) by (cpu)) | Busy state of all CPU cores together |  |
| count(count(node_cpu_seconds_total{instance="$node",job="$job"}) by (cpu)) | Количество процов/ядер |  |
| node_memory_MemTotal_bytes{instance="$node",job="$job"} | Всего памяти |  |
| ((node_memory_MemTotal_bytes{instance="$node",job="$job"} - node_memory_MemFree_bytes{instance="$node",job="$job"}) / (node_memory_MemTotal_bytes{instance="$node",job="$job"} )) * 100 | Загрузка памяти |  |
| 100 - ((node_memory_MemAvailable_bytes{instance="$node",job="$job"} * 100) / node_memory_MemTotal_bytes{instance="$node",job="$job"}) | Загрузка памяти |  |
| 100 - ((node_filesystem_avail_bytes{instance="$node",job="$job",mountpoint="/",fstype!="rootfs"} * 100) / node_filesystem_size_bytes{instance="$node",job="$job",mountpoint="/",fstype!="rootfs"}) | Загрузка корня |  |
| node_filesystem_size_bytes{instance="$node",job="$job",mountpoint="/",fstype!="rootfs"} | Корневой раздел |  |
| irate(node_network_receive_bytes_total{instance="$node",job="$job"}[5m])*8 | Трафик |  |
|  | Трафик |  |
| label_values(kube_pod_container_info{system="$cluster", env="$env", namespace="$namespace"},container) | выбрать приложения |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |



## MetricsQL (VictoriaMetrics)

