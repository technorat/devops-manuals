# Основные понятия и определения

## Оглавление
- [Метрики](https://gitlab.com/technorat/devops-manuals/-/tree/main/Monitoring_Observability/Docs/Metrics) это краткий/компактный набор обработанных и подготовленных 
значений, информации о системе. Мы собираем не все детали, как в логах, а только готовую выжимку: например, количество запросов к сервису.
