# Ссылки по Vmagent-у
- https://github.com/VictoriaMetrics/VictoriaMetrics/blob/master/app/vmagent/README.md
- https://github.com/VictoriaMetrics/VictoriaMetrics
- https://github.com/VictoriaMetrics/VictoriaMetrics/wiki/vmagent/e6b8ae3d09c0f0c473c30d2602f406778686dda4
- https://monhouse.tech/vmagent-kombajn-dlya-monitoringa-nikolaj-hramchihin-victoriametrics/
- https://docs.victoriametrics.com/vmagent.html
- Релизы: https://github.com/VictoriaMetrics/VictoriaMetrics/releases/tag/v1.94.0
- https://github.com/VictoriaMetrics/VictoriaMetrics/wiki/Single-server-VictoriaMetrics#operation
- https://docs.victoriametrics.com/#how-to-scrape-prometheus-exporters-such-as-node-exporter
