# Blackbox exporter

## Описание

Экспортер для Prometheus, который реализует сбор метрик внешних сервисов через HTTP, HTTPS, DNS, TCP, ICMP.
Посещение http://localhost:9115/probe?target=google.com&module=http_2xx вернет показатели для HTTP-зонда на google.com. Метрика Probe_success указывает, была 
ли проверка успешной. Добавление параметра debug=true вернет отладочную информацию для этого зонда. Метрики, касающиеся работы самого экспортера, доступны в 
конечной точке http://localhost:9115/metrics.

## Установка из бинарников.

Качаем со [страницы релизов](https://github.com/prometheus/blackbox_exporter/releases) и запускаем с нужными параметрами командной строки

```
# ./blackbox_exporter <flags>
```

## Запуск в Docker-е. Для начала создайте файл конфига в папке /opt/blackbox_exporter/blackbox-config.yml

```
# docker run --rm \
  -p 9115/tcp \
  --name blackbox_exporter \
  -v /opt/blackbox_exporter:/config \
  quay.io/prometheus/blackbox-exporter:latest --config.file=/config/blackbox-config.yml
```

## Config-файл

Файл записывается в формате YAML, определенном схемой, описанной ниже. Скобки указывают, что параметр является необязательным. Для параметров, не входящих в список, 
устанавливается значение, указанное по умолчанию.
Общие поля настраиваются следующим образом:
- <boolean>: логическое значение, которое может принимать значения true или false.
- <int>: обычное целое число
- <duration>: длительность, соответствующая регулярному выражению [0-9]+(ms|[smhdwy])
- <имя файла>: действительный путь в текущем рабочем каталоге.
- <строка>: обычная строка
- <секрет>: обычная строка, являющаяся секретом, например пароль.
- <regex>: регулярное выражение
Остальные параметры указываются отдельно.