# Дока по Linux

## Оглавление

* [Команды Linux] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/Commands.md)
* [Репозитории Linux] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/Repos.md)
* [Linux ЧАВО/FAQ] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/FAQ.md)
* [Установка локального репозитория пакетов Ubuntu из бинарного файла] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/Local_repository_(bin).md)
* [Как И ЗАЧЕМ удалить Snap-менеджер пакетов из Linux] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/Snap_remove.md)

## Ссылки

[Хорошие мануалы РэдОС Linux] (https://redos.red-soft.ru/base/manual/safe-redos/sshd_config/)