# Часто задаваемые вопросы (ЧАВО/FAQ) по Linux

## Оглавление

* [apt update: NO_PUBKEY 58403026387EE263](https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/FAQ.md#no_pubkey)

* [Как импортировать открытый .asc ключ, например Oracle](https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/FAQ.md#import_asc)

###  apt update: NO_PUBKEY 58403026387EE263 <a name="no_pubkey"></a>

**Ошибка:** http://wine.budgetdedicated.com jaunty Release: Следующие подписи не могут быть проверены, так как недоступен открытый ключ: NO_PUBKEY 58403026387EE263

**Решение:** 
Для решения сложившейся проблемы нам потребуется:
Проверить упоминание ключа на официальных серверах. Это требуется для подтверждения подлинности репозитория.
Серверы расположены в порядке значимости, поэтому нахождение ключа хотя бы на одном сервере является положительным результатом.

```shell
# gpg --keyserver keyserver.ubuntu.com --recv 58403026387EE263
# gpg --keyserver pgp.mit.edu --recv 58403026387EE263
# gpg --keyserver keyserver.pgp.com --recv 58403026387EE263
```

В ответ на экране должно отобразиться следующее:

```shell
gpg: запрашиваю ключ 387EE263 с hkp сервера keyserver.ubuntu.com
gpg: ключ 387EE263: "Scott Ritchie " не изменен
gpg: Всего обработано: 1
gpg: неизмененных: 1
```

Следующим этапом добавляем недостающий ключ репозитория одним из указанных ниже способов.
Начиная с Ubuntu 20.10, выдается предупреждение о том, что apt-key и add-apt-repository устарели. Необходимо вместо них использовать команду "gpg". Первый пример вроде бы самый кошерный:

```shell
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

Альтернативные или устаревшие варианты:

```shell
# gpg --export --armor 58403026387EE263 | sudo apt-key add -
# apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 387EE263 
# gpg --keyserver subkeys.pgp.net --recv-keys 07DC563D1F41B907
# gpg --export 07DC563D1F41B907 | apt-key add -
# gpg --keyserver subkeys.pgp.net --recv-keys 07DC563D1F41B907 && gpg --export 07DC563D1F41B907 | apt-key add -
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
# wget -O - http://deb.opera.com/archive.key | apt-key add -
```

Последняя команда работает в случае, если есть ссылка на конкретный ключ. Но она уже устарела и использование крайне нежелательно.

### Как импортировать открытый .asc ключ, например Oracle <a name="import_asc"></a>

1. cd /tmp 

2. wget -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc

3. gpg --yes --output /etc/apt/keyrings/oracle-virtualbox-2016.gpg --dearmor < oracle_vbox_2016.asc

### No usable dialog-like program is installed <a name="no_pubkey"></a>

**Ошибка:** No usable dialog-like program is installed
Нет программы для диалогов

**Решение:** 
Установить программу для диалогов

```shell
# apt-get install dialog
```

### ?????? вместо русских букв в mc <a name="no_pubkey"></a>

**Решение:** 

В источнике описан один из способов русификации Midnight Commander в Arch Linux. Для этого нужно выполнить следующие шаги:

* В /etc/locale.gen найти строчку #ru_RU.UTF-8 UTF-8 и раскомментировать её, убрав решётку (#).

* Скопировать эту строчку в буфер или просто запомнить, а файл сохранить с изменениями.

* Открыть /etc/rc.conf и в разделе LOCALIZATION найти строчку LOCALE и привести её к такому виду: LOCALE="ru_RU.UTF-8".

* В этом же разделе назначить шрифт, чтобы ОС могла отображать русские буквы: CONSOLEFONT="cyr-sun16".

* Сохранить изменённый файл и в консоли из-под пользователя root ввести команду: locale-gen.

### Как узнать текущий менеджер окон (KDE/Gnome?)

**Решение:** 

```shell
# echo $XDG_CURRENT_DESKTOP
# echo $DESKTOP_SESSION
```

