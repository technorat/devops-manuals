# Первоначальная настройка рабочей станции Linux

## Оглавление

* [apt update: NO_PUBKEY 58403026387EE263] (https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/Docs/FAQ.md#no_pubkey)

## Установка времени, часового пояса, синхронизации времени

```shell
# timedatectl show
# timedatectl status
# timedatectl set-timezone Europe/Moscow
# apt install chrony
# systemctl enable chrony
```

###  apt update: NO_PUBKEY 58403026387EE263 <a name="no_pubkey"></a>

**Ошибка:** http://wine.budgetdedicated.com jaunty Release: Следующие подписи не могут быть проверены, так как недоступен открытый ключ: NO_PUBKEY 58403026387EE263

```shell
# 
```

Устанавливаем необходимые пакеты (Ubuntu)
mc
curl
wget
apt-transport-https
ca-certificates
gnupg-agent
spftware-properties-agent
