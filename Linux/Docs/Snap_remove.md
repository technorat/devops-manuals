# Удаляем менеджер пакетов Snap

---

## Почему?

Snap - это аналог виндового Portable-софта для Linux. В пакете содержатся множество зависимостей, поэтому софт такой большой. Мессенджер, имеющий deb-пакет размером в несколько десятков или
сотнен мегабайт, может в snap-виде занять несколько гигабайт на диске. Обновления для 2-3 десятков Snap-овских пакетов ОЧЕНЬ быстро могут сожрать 100Гб и более. Самое отвратительное в этом 
продукте, то, что установленный из репозиториев через .deb/apt пакет после обновления вдруг может быть заменен на snap-версию. Ты установил Viber, Telegram, Vs Code и Lens через Snap, а 
через пару месяцев окажется, что у тебя таких приложений стало 20-30 штук. Вы можете подумать "у меня диск 100500Тб, на место мне пофигу". Занимаемое место - это не самая большая проблема.
Snap-овские приложения очень часто глючные. В панели запущенных  задач у тебя вдруг появилось приложение без значка - это стандартное повыедение Snap-овских приложений. С одним еще можно 
смириться, когда у тебя такое одно приложение, но не когда 2-4 и более. Telegram не отправляет или не сохраняет полученные файлы? Telegram не понимает вложенные подгруппы? Telegram не может 
подконнектиться к серверу при том, что все программы работают нормально? Firefox может создавать папку, но сам же в нее не сохранять файлы? Или сохраняет в одну папку, но не сохраняет в 
соседнюю, хотя на обоих установлены права 777? Lens коннектится к 4 кубам из 5 настроенных, хотя вчера все работало и сетевая свзанность между всеми кубами и твоей тачкой полная? И т.д.
Это все - сраный Snap.
**Snap must die!**

---

## Как?

1. Смотрим список установленного снапом софта

```shell
# snap list
Name               Version          Rev    Tracking       Publisher   Notes
bare               1.0              5      latest/stable  canonical✓  base
core22             20231123         1033   latest/stable  canonical✓  base
firefox            120.0.1-1        3504   latest/stable  mozilla✓    -
gnome-42-2204      0+git.ff35a85    141    latest/stable  canonical✓  -
gtk-common-themes  0.1-81-g442e511  1535   latest/stable  canonical✓  -
snapd              2.60.4           20290  latest/stable  canonical✓  snapd
```

2. Удаляем по очереди пакеты. Как с apt-ом указать все пакеты сразу не получится. Требуется удаление в определенной последовательности. Одни пакеты имеют какие то зависимости из других пакетов
и пока не удалишь основной пакет, зависимости не удаляются. Не удаляется один пакет - пробуйте следующий. Это все - сраный Snap.
**Snap must die!**

```shell
# snap remove core22
error: cannot remove "core22": snap "core22" is not removable: snap is being used by snap
       gnome-42-2204.

# snap remove firefox
firefox removed

# snap remove gtk-common-themes
gtk-common-themes removed

# snap remove gnome-42-2204
gnome-42-2204 removed

# snap remove bare
bare removed

# snap remove core22
core22 removed

# snap list
Name   Version  Rev    Tracking       Publisher   Notes
snapd  2.60.4   20290  latest/stable  canonical✓  snapd

# snap remove snapd
snapd removed
```

---

3. Удаляем это говно из системы. Команда ‘apt-mark hold‘ означает, что пакет помечен как отложенный, что предотвратит автоматическую установку, обновление или удаление пакета.
**Snap must die!**

```shell
# systemctl stop snapd

# systemctl disable snapd

# apt-mark hold snapd

# apt remove --autoremove snapd gir1.2-snapd-2 plasma-discover-backend-snap gnome-software-plugin-snap
Чтение списков пакетов… Готово
Построение дерева зависимостей… Готово
Чтение информации о состоянии… Готово         
Следующие пакеты будут УДАЛЕНЫ:
  snapd
Обновлено 0 пакетов, установлено 0 новых пакетов, для удаления отмечено 1 пакетов, и 4 пакетов не обновлено.
После данной операции объём занятого дискового пространства уменьшится на 110 MB.
Хотите продолжить? [Y/n] y
(Чтение базы данных … на данный момент установлено 286795 файлов и каталогов.)
Удаляется snapd (2.60.4+23.10.1) …
Warning: Stopping snapd.service, but it can still be activated by:
  snapd.socket
Обрабатываются триггеры для gnome-menus (3.36.0-1.1ubuntu1) …
Обрабатываются триггеры для man-db (2.11.2-3) …
Обрабатываются триггеры для dbus (1.14.10-1ubuntu1) …
Обрабатываются триггеры для mailcap (3.70+nmu1ubuntu1) …
Обрабатываются триггеры для desktop-file-utils (0.26-1ubuntu5) …

# apt-mark hold snapd

# rm -rf ~/snap/
# rm -rf /snap
# rm -rf /var/snap
# rm -rf /var/lib/snapd
```

4. Запрещаем саму возможность установки снаповских пакетов. Создадим файл конфига для настройки apt
**Snap must die!**

```shell
# vi /etc/apt/preferences.d/nosnap.pref
Package: snapd
Pin: release a=*
Pin-Priority: -10
```
