# Изоляция процессов в Linux с помощью Cgroup

Cgroup (Control groups) задают лимиты и приоритеты на использование ресурсов (CPU, память, диск, ширина пропускания сети и т.д.). Это позволяет предотвратить использование 100% ресурсов одним процессом.
Все запускаемые в системе процессы объединяются в какие то Cgroup. Они построены иерархически. Одна большая Cgroup может состоять из более мелких Cgroup.
Просмотреть список Cgroups:

```shell
# systemd-cgls
CGroup /:
-.slice
├─user.slice
│ └─user-1000.slice
│   ├─user@1000.service …
│   │ ├─session.slice
│   │ │ ├─gvfs-goa-volume-monitor.service
│   │ │ │ └─17198 /usr/libexec/gvfs-goa-volume-monitor
│   │ │ ├─xdg-permission-store.service
│   │ │ │ └─2676 /usr/libexec/xdg-permission-store
│   │ │ ├─filter-chain.service
│   │ │ │ └─2467 /usr/bin/pipewire -c filter-chain.conf
│   │ │ ├─xdg-document-portal.service
│   │ │ │ ├─2672 /usr/libexec/xdg-document-portal
│   │ │ │ └─2683 fusermount3 -o rw,nosuid,nodev,fsname=portal,auto_unmount,subtype=portal -- /run/user/1000/doc
│   │ │ ├─xdg-desktop-portal.service
│   │ │ │ └─2663 /usr/libexec/xdg-desktop-portal
│   │ │ ├─plasma-ksmserver.service
│   │ │ │ └─2749 /usr/bin/ksmserver
│   │ │ ├─pipewire-pulse.service
│   │ │ │ └─2469 /usr/bin/pipewire-pulse
│   │ │ ├─wireplumber.service
│   │ │ │ └─2468 /usr/bin/wireplumber
│   │ │ ├─gvfs-daemon.service
│   │ │ │ ├─ 2601 /usr/libexec/gvfsd
│   │ │ │ ├─ 2608 /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f
│   │ │ │ ├─17203 /usr/libexec/gvfsd-trash --spawner :1.15 /org/gtk/gvfs/exec_spaw/0
│   │ │ │ ├─17218 /usr/libexec/gvfsd-network --spawner :1.15 /org/gtk/gvfs/exec_spaw/1
│   │ │ │ └─17234 /usr/libexec/gvfsd-dnssd --spawner :1.15 /org/gtk/gvfs/exec_spaw/3
│   │ │ ├─plasma-kwin_x11.service
│   │ │ │ └─2752 /usr/bin/kwin_x11 --replace
```

Очень удобно использовать с ключом "-u", так мы видим, какой сервис (unit-файл) какую cgroup создал. 
Загрузку каждой Cgroup показывает команда "systemd-cgtop":

```shell
# systemd-cgtop
CGroup                                                                                                              Tasks   %CPU   Memory  Input/s Output/s
/                                                                                                                    1796   15.9     8.1G        -        -
user.slice                                                                                                           1376   14.6    25.0G        -        -
user.slice/user-1000.slice                                                                                           1376   14.6    24.9G        -        -
user.slice/user-1000.slice/user@1000.service                                                                         1335   13.8    24.8G        -        -
system.slice                                                                                                          129    1.7    34.8G        -        -
system.slice/sddm.service                                                                                               4    1.6   192.9M        -        -
user.slice/user-1000.slice/session-4.scope                                                                             35    0.7   157.2M        -        -
system.slice/containerd.service                                                                                        16    0.1    54.0M        -        -
system.slice/fwupd.service                                                                                              6    0.0    24.0M        -        -
system.slice/systemd-oomd.service                                                                                       1    0.0     1.4M        -        -
```


