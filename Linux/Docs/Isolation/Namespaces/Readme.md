# Изоляция процессов в Linux с помощью Namespace

Namespace создает виртуальные окружения, изолируя независимые процессы друг от друга. Для каждого изолированного процесса есть "свой" сетевой стэк, пространство идентификаторов (ID процессов), точки монтирования файловой системы, UID- ы пользователей.

Namespace создает изолированный набор ресурсов для каждого запускаемого процесса:

* __PID__ - внутри каждого пространства имен свой уникальный набор идентификаторов программ

* __Сеть__ - изоляция сети. Внутри каждого пространства имен "свой" набор сетевых адаптеров, адресного пространства IP-адресов, таблиц маршрутизации и т.д.

* __ФС__ - изоляция файловой системы создает виртуальную уникальную корневую файловую систему для каждого процесса. Запущенные изолированные процессы работают только с одним подкаталогом корневой файловой системы.

* __Пользователи__ - внутри каждого пространства имен свой набор пользователей и пользовательских групп.

* __IPC__ - изоляция объектов System V IPC и очередей сообщений POSIX.

* __UTS__ - изоляция имен хостов и доменной системы.

* __Cgroup__ - изоляция сontrol groups корневого каталога.

* __Системные часы__ - изоляция системных часов для каждого пространства имен.

__В Linux пространства имён обычно представлены как файлы в директории /proc/<pid>/ns.__

В текущей стабильной версии ядра 5.7 есть семь пространств имён:

* __PID:__ изоляция дерева системных процессов;
* __NET:__ изоляция сетевого стека хоста;
* __MNT:__ изоляция точек монтирования файловой системы хоста;
* __UTS:__ изоляция имени хоста;
* __IPC:__ изоляция утилит межпроцессного взаимодействия (сегменты разделяемой памяти, семафоры);
* __USER:__ изоляция ID пользователей системы;
* __CGROUP:__ изоляция виртуальной файловой системы cgroup хоста.

Каждый процесс может воспринимать только одно пространство имён.

Просмотреть имеющиеся пространства:

```
# ls /proc/$$/ns -al
total 0
dr-x--x--x 2 root root 0 Oct  6 16:38 .
dr-xr-xr-x 9 root root 0 Oct  6 14:23 ..
lrwxrwxrwx 1 root root 0 Oct  6 16:38 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 mnt -> 'mnt:[4026531841]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 net -> 'net:[4026531840]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 pid -> 'pid:[4026531836]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 time -> 'time:[4026531834]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 time_for_children -> 'time:[4026531834]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 user -> 'user:[4026531837]'
lrwxrwxrwx 1 root root 0 Oct  6 16:38 uts -> 'uts:[4026531838]'
```

## Unshare

Мы можем вынести процесс в отдельное пространство имен с помощью команды "unshare":

```shell
# unshare --help

Usage:
 unshare [options] [<program> [<argument>...]]

Run a program with some namespaces unshared from the parent.

Options:
 -m, --mount[=<file>]      unshare mounts namespace
 -u, --uts[=<file>]        unshare UTS namespace (hostname etc)
 -i, --ipc[=<file>]        unshare System V IPC namespace
 -n, --net[=<file>]        unshare network namespace
 -p, --pid[=<file>]        unshare pid namespace
 -U, --user[=<file>]       unshare user namespace
 -C, --cgroup[=<file>]     unshare cgroup namespace
 -T, --time[=<file>]       unshare time namespace

 -f, --fork                fork before launching <program>
 --map-user=<uid>|<name>   map current user to uid (implies --user)
 --map-group=<gid>|<name>  map current group to gid (implies --user)
 -r, --map-root-user       map current user to root (implies --user)
 -c, --map-current-user    map current user to itself (implies --user)
 --map-auto                map users and groups automatically (implies --user)
 --map-users=<inneruid>:<outeruid>:<count>
                           map count users from outeruid to inneruid (implies --user)
 --map-groups=<innergid>:<outergid>:<count>
                           map count groups from outergid to innergid (implies --user)

 --kill-child[=<signame>]  when dying, kill the forked child (implies --fork)
                             defaults to SIGKILL
 --mount-proc[=<dir>]      mount proc filesystem first (implies --mount)
 --propagation slave|shared|private|unchanged
                           modify mount propagation in mount namespace
 --setgroups allow|deny    control the setgroups syscall in user namespaces
 --keep-caps               retain capabilities granted in user namespaces

 -R, --root=<dir>          run the command with root directory set to <dir>
 -w, --wd=<dir>            change working directory to <dir>
 -S, --setuid <uid>        set uid in entered namespace
 -G, --setgid <gid>        set gid in entered namespace
 --monotonic <offset>      set clock monotonic offset (seconds) in time namespaces
 --boottime <offset>       set clock boottime offset (seconds) in time namespaces

 -h, --help                display this help
 -V, --version             display version

For more details see unshare(1).
```

Параметры:

* __--fork__ - разделяет процессы при запуске

* __--mount__ - создает отдельную виртуальную корневую ФС для процесса

* __--mount-proc__ - делаем монтирвание файловой системы /proc

* __--kill-child__ - удалить дочерние процессы после завершения основного процесса

Применение всех имеющихся параметров содает очень изолированную "песочницу".

### Пример 1

Проверяем, какой у нас PID оболочки bash и пробуем запустить bash в отдельном пространстве имен

```shell
# echo $$
18395
# unshare --pid --fork --mount-proc --kill-child /bin/bash
#
```

После ввода команды возвращается на приглашение командной строки. Но если мы проверим PID данного процесса, то увидим, что его PID уже сменился на "1". Т.е. мы внутри отделенной программы. У нас и пространство идентификаторов процессов свое. Мы не видим всех процессов, которые "крутятся" в основной ОС:

```shell
# echo $$
1
# ps -A
    PID TTY          TIME CMD
      1 pts/11   00:00:00 bash
      8 pts/11   00:00:00 ps
```

Запустив какой то процесс в пространстве имен, мы его видим в списке процессов из основной ОС. Но там он виден с другим PID.




