# Установка локального репозитория пакетов Ubuntu из бинарного файла

1. Создаем папку репозитория

```shell
# mkdir /repository
```

---

2. Для начала нам потребуется ключ, которым будет подписан репозиторий. В данном примере мы создаем RSA-ключ длиной 4096 бита, который не имеет срока давности

```shell
# apt update
# apt install gpg
gpg (GnuPG) 2.2.27; Copyright (C) 2021 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Замечание: "gpg --full-generate-key" вызывает полнофункциональный диалог создания ключа.
GnuPG должен составить идентификатор пользователя для идентификации ключа.
Ваше полное имя: darksoft
Адрес электронной почты: s89053333338@gmail.com
Вы выбрали следующий идентификатор пользователя:
    "darksoft <s89053333338@gmail.com>"
Сменить (N)Имя, (E)Адрес; (O)Принять/(Q)Выход? 
Введите фразу-пароль для защиты нового ключа
Необходимо получить много случайных чисел. Желательно, чтобы Вы
в процессе генерации выполняли какие-то другие действия (печать
на клавиатуре, движения мыши, обращения к дискам); это даст генератору
случайных чисел больше возможностей получить достаточное количество энтропии.
Необходимо получить много случайных чисел. Желательно, чтобы Вы
в процессе генерации выполняли какие-то другие действия (печать
на клавиатуре, движения мыши, обращения к дискам); это даст генератору
случайных чисел больше возможностей получить достаточное количество энтропии.
gpg: ключ 246F2280045C64B6 помечен как абсолютно доверенный
gpg: создан каталог '/root/.gnupg/openpgp-revocs.d'
gpg: сертификат отзыва записан в '/root/.gnupg/openpgp-revocs.d/1793106A47106E7C17A42C52246F2280045C64B6.rev'.
открытый и секретный ключи созданы и подписаны.

pub   rsa3072 2023-11-30 [SC] [годен до: 2025-11-29]
      1793106A47106E7C17A42C52246F2280045C64B6
uid                      darksoft <s89053333338@gmail.com>
sub   rsa3072 2023-11-30 [E] [годен до: 2025-11-29]
```

---

3. Далее ключ нужно экспортировать в файл и в дальнейшем поместить в корень репозитория для дальнейшего импорта на клиентах.

```shell
# gpg --list-keys 
/root/.gnupg/pubring.kbx
------------------------
pub   rsa4096 2016-04-22 [SC]
      B9F8D658297AF3EFC18D5CDFA2F683C52980AECF
uid         [ неизвестно ] Oracle Corporation (VirtualBox archive signing key) <info@virtualbox.org>
sub   rsa4096 2016-04-22 [E]

pub   rsa3072 2023-11-30 [SC] [годен до: 2025-11-29]
      1793106A47106E7C17A42C52246F2280045C64B6
uid         [  абсолютно ] darksoft <s89053333338@gmail.com>
sub   rsa3072 2023-11-30 [E] [годен до: 2025-11-29]

# gpg --output keyFile --armor --export 1793106A47106E7C17A42C52246F2280045C64B6

# ls -la | grep key
-rw-r--r--   1 root root   2456 ноя 30 22:36 keyFile
```

---


4. Проверяем, какая версия дистра у нас стоит чтобы знать, какие пакеты выкачивать и какие папки в репозитории создавать

```shell
# cat /etc/*-release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=22.04
DISTRIB_CODENAME=jammy
DISTRIB_DESCRIPTION="Ubuntu 22.04.3 LTS"
PRETTY_NAME="Ubuntu 22.04.3 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04.3 LTS (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy
```

---

5. Пора подготовить дерево репозитория. У нас есть только один дистрибутив Ubuntu, только Intel x86_64 и нет пакетов с исходным кодом, поэтому о 
создании других элементов дерева мы не беспокоимся. Если пакеты собираются под разные дистрибутивы и имеют разные зависимости, дерево получится более 
развесистым, да и помянутые ниже правила сборки усложнятся. Переносим файл экспортированного ключа в новую папку и переходим в нее. Создаем служебные
подпапки под нужные дистрибутивы

```shell
# mv /keyFile !$
mv /keyFile /repository/
# cd !$
# mkdir -p conf  dists/jammy/contrib/binary-amd64/Packages dists/xenial/contrib/binary-amd64/Packages
```

---

6. Создадим файл конфигурации репозитория

```shell
# cat > /repository/conf/distributions <<EOF
Origin: DarkSoft
Label: DarkSoft private Ubuntu repo
Codename: jammy
Architectures: amd64
Components: contrib
Description: Our own and 3rd party software packaged internally
EOF
# echo SignWith: 1793106A47106E7C17A42C52246F2280045C64B6 >> /repository/conf/distributions
```

---

7. Пора настроить автоматическое обновление репозитория при появлении в нем новых пакетов. Файл InRelease со встроенной подписью запрашивается новыми 
пакетными менеджерами, а связка из двух файлов Release и Release.gpg нужна старым. Зависимости нужно продублировать для всех дистрибутивов, которые вы 
планируете поддерживать.

```shell
# cat > /repository/Makefile <<EOF
#!/usr/bin/make
#
# Update the repository every time when a new package arrives

all: repo

repo: dists/jammy/InRelease dists/jammy/Release.gpg

dists/jammy/InRelease: dists/jammy/Release
        gpg --clearsign --digest-algo SHA512 -o dists/jammy/InRelease.new dists/jammy/Release
        mv dists/jammy/InRelease.new dists/jammy/InRelease

dists/jammy/Release.gpg: dists/jammy/Release
        gpg -abs -o dists/jammy/Release.gpg-new dists/jammy/Release
        mv dists/jammy/Release.gpg-new dists/jammy/Release.gpg

dists/jammy/Release: conf/distributions contrib/binary-amd64/Packages.gz
        cat conf/distributions > dists/jammy/Release
        apt-ftparchive release . >> dists/jammy/Release

contrib/binary-amd64/Packages.gz: contrib/binary-amd64/Packages
        gzip --keep --force -9 ../../contrib/binary-amd64/Packages

contrib/binary-amd64/Packages: contrib/binary-amd64/*.deb
        dpkg-scanpackages contrib/binary-amd64 > contrib/binary-amd64/Packages.new
        mv contrib/binary-amd64/Packages.new contrib/binary-amd64/Packages
EOF
```

---

8. Установите Nginx (по другому мануалу). Надо обеспечить репозиторию доступность по HTTP. Пример базового конфига самого Nginx 

```shell
# cat /etc/nginx/nginx.conf 
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
}
```

В папке /etc/nginx/conf.d/ создаем файл виртуального сервера для репозитория

```shell
# vi /etc/nginx/conf.d/defaultserver.conf 
server {
        listen 80;
        server_name 192.168.255.200;
        root /repository;
        index index.php index.html index.htm;
        autoindex on;
        autoindex_exact_size on;
        autoindex_localtime on;
        access_log /var/log/nginx/vbox/access.log;
        error_log  /var/log/nginx/vbox/error.log  error;
        location ~ /(.*)/conf {
            deny all;
        }
}
```

Перезапускаем Nginx

```shell
# service nginx restart
```

---

9. И, наконец, прописываем свой репозиторий на клиентах

```shell
# wget -O - http://repo/keyFile | sudo apt-key add -
# echo 'deb [arch=amd64] http://repo/ xenial contrib' > /etc/apt/sources.list.d/mylovelyrepo.list
# apt update
```

---

10. При попытке что то скачать пишет ошибку. Оказалось, надо еще генерить индексный файл после каждого изменения папки. Устанавливаем для этого программу

```shell
# apt install dpkg-dev
```

---

11. Накидываем файло *.deb в папку и запускаем 

```shell
# cd /repository/dists/jammy/contrib/binary-amd64/
# apt-get download asbru-cm
# dpkg-scanpackages . /dev/null | gzip -9c > Packages.gz
```
Нужный пакет сформирован, теперь можно качать пакеты с локальной репы