# Установка времени, часового пояса, синхронизации времени

## Просмотр и установка часового пояса

```shell
# timedatectl show
# timedatectl status
# timedatectl set-timezone Europe/Moscow
```

## Настройка синхронизации часов. Установим службу для синхронизации времени. Разрешим ее автозапуск:

```shell
# apt install chrony
# systemctl enable chrony
```


