# Полезные приемы работы в CLI

1. Если вы не хотите, чтобы выполняемая команда сохранилась в истории просто поставьте перед ней пробел.

2. Вы можете выполнить последнюю команду просто набрав "!!".

3. Запустить команду в фоне чтобы не ждать ее завершения - добавляем после нее пробел и потом амперсант - "&". Если связь с удаленной машиной (например по SSH) будет потеряна, то сессия завершится и долгая команда не доработает до конца. Как этого избежать - читайте дальше.

4. Запустить команду в фоне чтобы не ждать ее завершения И ЧТОБЫ ОНА НЕ ВЫКЛЮЧИЛАСЬ ПРИ ОБРЫВЕ СВЯЗИ С ТЕРМИНАЛОМ - добавляем перед ней слово "nohup", а после нее пробел и потом амперсант - "&"

5. Чтобы выполнить несколько команд одну за другой нужно между ними вставить пробел, 2 апмерсанта, пробел:

```
ls && df
```

6. Если нужно вывести одной командой список файлов или других сущностей, а потом к каждому из них применить другую команду, то можно использовать команду xargs. Например, хотим найти в текущем каталоге только файлы с расширением "txt" и просмотреть их все:

```
ls *.txt | xargs cat
```

Или засунуть одну команду внутрь конструкции "$(<команда>)" и вставить ее в нужном месте:

```
cat $(ls *.txt)
```

Или засунуть одну команду внутрь конструкции "`<команда>` и вставить ее в нужном месте:

```
cat `ls *.txt`
```

7. Создать пустой файл или уничтожить все данные в файле поможет команда > file_name.txt. Еще создать пустой файл можно командами "touch file_name.txt" и "cat /dev/null > file_name.txt"

8. Если ты вводил команду, которая требует повышения привилегий, и забыл подставить sudo, можно воспользоваться таким трюком: sudo !!. Оболочка запустит предыдущую команду под рутом.

9. В качестве альтернативы сетевым командам ping и traceroute можно воспользоваться "mtr <URL или IP>.

10. Команда ps aux покажет много диагностических данных в удобном виде.

11. Если необходимо ввести команду, чтобы она не попала в лог истории, нужно подставить перед ней пробел. К примеру, ps aux.

12. Если ты набрал команду и хочешь добавить к ней аргументы из команд, набранных ранее, удерживай Alt или Esc и нажимай на точку. В строку ввода один за другим будут подставляться параметры предыдущих команд.

13. Для очистки терминала достаточно комбинации клавиш Ctrl + l. Аналог команды "clear"

14. Обращаться к домашнему каталогу пользователя не обязательно по полному имени. Вместо "/home/user_name" можно подставлять просто "~"

15. В командной строке удобно подставлять разные шаблоны для имен файлов. Например просмотреть в нужном каталоге файлы с суффиксом "-release":

```
cat /etc/*-release
```

Или скачать с сайта картинки с именами от file1.png до "file325.png":

```
wget http://site.ru/file{1..325}.png
```

Можно применять целые алгоритмы программирования для формирования списка файлов:

```
for i in `seq 1 3`; do wget http://site.ru/file$i.png; done
```

16. Выполнить команду "ls" в 15:22:

```
ls /etc | at 15:22
```
