

## Получить UPM-токен для админских запросов и загнать его в переменную

```shell
# export UPM_TOKEN=$(curl -I --user Login_админа:Gahjkm_flvbyf -H 'Accept: application/vnd.atl.plugins.installed+json' 'http://адрес_сервера/rest/plugins/1.0/?os_authType=basic'  2>/dev/null | grep 'upm-token' | cut -d " " -f 2 | tr -d '\r')
```

## Получить статус плагинов

1. Загоняем нужные параметры в переменные
export ADMIN_USRNAME=Ваш_логин_с_админскими_правами
export ADMIN_PWD=Пароль
export CONFLUENCE_BASE_URL=адес_сайта

2. Теперь запрашиваем

```shell
# curl --user $ADMIN_USRNAME:$ADMIN_PWD ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key?token='${UPM_TOKEN}
{"key":"soyTemplateRendererHelperContext","completeKey":"com.atlassian.confluence.plugins.soy:soyTemplateRendererHelperContext","links":{"self":"/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key","modify":"/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key","plugin":"/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key"},"enabled":false,"optional":false,"name":"Confluence Soy Template Renderer for Velocity","recognisableType":true,"broken":false}
```

3. Список всех плагинов

```shell
# curl --user $ADMIN_USRNAME:$ADMIN_PWD ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/?token='${UPM_TOKEN}
```shell


Get the current status of soyTemplateRendererHelperContext module. If you get a JSON response from that, it means this module is disabled and you are affected by this bug.

curl --user $ADMIN_USRNAME:$ADMIN_PWD ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key?token='${UPM_TOKEN} 2>/dev/null | grep '"enabled":false'

Get the current status of velocity.helper module. If you get a JSON response from that, it means this module is disabled and you are affected by this bug.

curl --user $ADMIN_USRNAME:$ADMIN_PWD ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.extra.officeconnector-key/modules/velocity.helper-key?token='${UPM_TOKEN} 2>/dev/null | grep '"enabled":false'

Get the current status of siteLogoHelperContext module. If you get a JSON response from that, it means this module is disabled and you are affected by this bug.

curl --user $ADMIN_USRNAME:$ADMIN_PWD ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.confluence-lookandfeel-key/modules/siteLogoHelperContext-key?token='${UPM_TOKEN} 2>/dev/null | grep '"enabled":false'

Using the UPM REST API to re-enable the affected module

    Get UPM token to be used on the next steps – it will be saved as an environment variable.

    CONFLUENCE_BASE_URL=<Confluence Base URL>
    ADMIN_USRNAME=<local admin account username>
    ADMIN_PWD=<local admin account password>

    UPM_TOKEN=$(curl -I --user ${ADMIN_USRNAME}:${ADMIN_PWD} -H 'Accept: application/vnd.atl.plugins.installed+json' ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/?os_authType=basic' 2>/dev/null | grep 'upm-token' | cut -d " " -f 2 | tr -d '\r')

    Get the current status of the soyTemplateRendererHelperContext module and save it in an environment variable, using sed to change the status to enabled ("enabled":false).

    MODIFIED_JSON_OUTPUT=$(curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key?token='${UPM_TOKEN} 2>/dev/null | sed 's/"enabled":false/"enabled":true/')
    echo ${MODIFIED_JSON_OUTPUT}

    Update the soyTemplateRendererHelperContext module status.

    curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} -H 'Content-Type: application/vnd.atl.plugins.plugin.module+json' -X PUT ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.soy-key/modules/soyTemplateRendererHelperContext-key?token='${UPM_TOKEN} --data '<manual output from ${MODIFIED_JSON_OUTPUT}>'

    Get the current status of the velocity.helper module and save it in an environment variable, using sed to change the status to enabled ("enabled":false).

    MODIFIED_JSON_OUTPUT=$(curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.extra.officeconnector-key/modules/velocity.helper-key?token='${UPM_TOKEN} 2>/dev/null | sed 's/"enabled":false/"enabled":true/')
    echo ${MODIFIED_JSON_OUTPUT}

    Update the velocity.helper module status.

    curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} -H 'Content-Type: application/vnd.atl.plugins.plugin.module+json' -X PUT ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.extra.officeconnector-key/modules/velocity.helper-key?token='${UPM_TOKEN} --data '<manual output from ${MODIFIED_JSON_OUTPUT}>'

    Get the current status of the siteLogoHelperContext module and save it in an environment variable, using sed to change the status to enabled ("enabled":false).

    MODIFIED_JSON_OUTPUT=$(curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.confluence-lookandfeel-key/modules/siteLogoHelperContext-key?token='${UPM_TOKEN} 2>/dev/null | sed 's/"enabled":false/"enabled":true/')
    echo ${MODIFIED_JSON_OUTPUT}

    Update the siteLogoHelperContext module status.

    curl --user ${ADMIN_USRNAME}:${ADMIN_PWD} -H 'Content-Type: application/vnd.atl.plugins.plugin.module+json' -X PUT ${CONFLUENCE_BASE_URL}'/rest/plugins/1.0/com.atlassian.confluence.plugins.confluence-lookandfeel-key/modules/siteLogoHelperContext-key?token='${UPM_TOKEN} --data '<manual output from ${MODIFIED_JSON_OUTPUT}>'

    Refresh the Confluence page and confirm the top bar is now accessible.
