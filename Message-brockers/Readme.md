# Брокеры сообщений

## Продукты

  - Apache Kafka
  - RabbitMQ
  - NATS (NATS Messaging System)
  - ActiveMQ
  - Redis Pub/Sub
  - Amazon Simple Notification Service (SNS)
  - Google Cloud Pub/Sub
  - Microsoft Azure Service Bus
  - и другие.

## Оглавление

  - [Основы](https://gitlab.com/technorat/devops-manuals/-/blob/main/Message-brockers/Docs/Basics.md?ref_type=heads)
