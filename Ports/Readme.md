# Порты различных сетевых служб
| Порт | Служба | Эндпоинты |
|---|---|---|
| 22 | SSH |  |
| 111 | Rpcbind |  |
| 631 | Cups |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
| 3000 | Grafana | /metrics - метрики |
| 3100 | Loki | /metrics - метрики |
| 5432 | Postgres |  |
| 6783 | Alertmanager |  |
| 8080 | cAdvisor | /metrics - метрики |
| 8080 | cAdvisor | /containers/ - список контейнеров |
| 8300 | Consul |  |
| 8428 | Victoriametrics | 0.0.0.0:8428 - список эндпоинтов |
| 8428 | Victoriametrics | /metrics - метрики |
| 8428 | Victoriametrics | /vmui/ - веб-морда |
| 8429 | Vmagent | 0.0.0.0:8429 - список эндпоинтов |
| 8429 | Vmagent | /metrics - метрики |
| 8429 | Vmagent | /targets - найденные цели |
| 8429 | Vmagent | /api/v1/targets - расширенная инфа о найденных целях |
| 8429 | Vmagent | /-/reload - перезагрузить конфиг |
| 8880 | Vmalert |  |
| 8900 | Node Exporter | /metrics - метрики |
| 9080 | Promtail | /metrics - метрики |
| 9115 | Blackbox exporter | 0.0.0.0:9115 - список эндпоинтов |
| 9115 | Blackbox exporter | /metrics - метрики |
| 9115 | Blackbox exporter | /config - конфиг |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
 