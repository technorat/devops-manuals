# Документация и заметки по Argo CD

## Оглавление

* [Ключевые концепции](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Docs/Core_concepts.md)
* [Возможности Argo CD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Docs/Features.md)
* [Установка в Kubernetes из официального манифеста](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Docs/Installing/K8s.md)
* [Создание приложения в Argo CD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Docs/Creating_application.md)
