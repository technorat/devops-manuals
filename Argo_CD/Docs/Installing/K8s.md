# Установка Argo CD в Kubernetes

## По официальной документации

__Завиcимости__:
* Должен быть установлен __kubectl__ на машине, откуда ставим
* Настроен __kubeconfig__ для работы с нужным кластером Kubernetes

1. Создаем пространство имен

```shell
# kubectl create namespace argocd
```

2. Применяем файл, который задеплоит в куб все нужные компоненты. Файл, на всякий случай, положил 
[здесь](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Manifests/Install_k8s_official.yaml)

```shell
# kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Если требуется развернуть HA-версию Argo CD, то выполняем:

```shell
# kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/ha/install.yaml
```

Файл, на всякий случай, положил [здесь](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Manifests/Install_k8s_HA_official.yaml).
Создается пространство имен "argocd" и много разных сущностей: Pod-ы, service-ы, replicaset-ы, statefulset-ы, deployment-ы. Просмотреть полный список 
можем так:

```shell
# kubectl get all -n argocd
```

3. По умолчанию, API и web-интерфейс Argo CD не имеют внешнего IP-адреса, службы работают в режиме ClusterIP и доступны только внутри кластера.
Чтобы получить доступ к серверу API, выберите один из следующих способов предоставления доступа к серверу API Argo CD:

* Сервис типа [Load Balancer](https://argo-cd.readthedocs.io/en/stable/getting_started/#service-type-load-balancer)
* [Ingress](https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/)
* [Port Forwarding](https://argo-cd.readthedocs.io/en/stable/getting_started/#port-forwarding). Это самый простой и самый быстрый способ, рассматривается
только как временное решение для разового подключения в рамках тестов и макетирования. 

Для тестов и быстрого подключения "просто посмотреть" нас вполне устроить **временный** вариант - Port Forwarding. Запускается он в отдельной консоли:

```shell
# kubectl port-forward svc/argocd-server -n argocd 8080:443
```


## Сервис типа Load Balancer

Измените тип службы argocd-server на LoadBalancer:

```shell
# kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

Чтобы подключиться к web-интерфейсу, нужно 

## LoadBalancer

Переадресацию портов Kubectl также можно использовать для подключения к серверу API без раскрытия службы. Доступ к серверу API можно получить с помощью 
https://localhost:8080.

```shell
# kubectl port-forward svc/argocd-server -n argocd 8080:443
```

4. Пароль админа по умолчанию зашифрован в base64 и лежит в секрете. Просто так его увидеть не получится, нужно выдергивать через kubectl:

```shell
# kubectl get secret -n argocd argocd-secret -o yaml | grep -Ei "  admin.password: " | awk '{print $2}' | base64 --decode > ~/argocd_password.txt
```

В домашнем каталоге текущего пользователя появится файл "argocd_password.txt" с расшифрованным паролем.





Установить Argo CD CLI можно отсюда: https://github.com/argoproj/argo-cd/releases/latest

```shell
# curl -sSL -o ~/argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
# install -m 755 ~/argocd-linux-amd64 /usr/local/bin/argocd
# rm ~/argocd-linux-amd64
```



