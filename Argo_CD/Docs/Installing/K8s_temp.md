# Ставим ArgoCD по видосу

__[URL видео](https://rutube.ru/video/8da2a23f5b64e0b9b4d7b1336622e841/?ysclid=lyzzt1fzwz677425325)__

1. Создаем Namespace и применяем манифест для деплоя ArgoCD в K8s

```shell
# kubectl create namespace argocd
# kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

1. Проверяем, что поды и сервисы запустились

2. Ставим на свою машину CLI. Возможно, потом захотим подключаться не через веб-морду, а управлять с помощью CLI.

```shell
# curl -sSL -o ~/argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
# install -m 755 ~/argocd-linux-amd64 /usr/local/bin/argocd
# rm ~/argocd-linux-amd64
```

4. Пробуем подключиться с помощью Port Forward - проброс порта из K8s напрямую в машину. Проброс порта работает пока не прервем работу команды в CLI через Ctrl+C или не перезагрузимся. Для долгосрочной работы надо настраивать Ingress, NodePort или LoadBalancer.

```shell
# kubectl port-forward svc/argocd-server -n argocd 8080:443
```

Открываем веб-морду по адресу __https://localhost:8080__

5. Первоначальный пароль админа у каждой инсталляции разный, т.к. генерируется во время установки. Начиная с Argo CD v1.9, сохраняется он как текстовое поле "password" в Secret-е K8s: __argocd-secret__. Выдергиваем его с помощью команды

```shell
# kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo

zToBSbbmcXXXeHXP
```

Колучаем какой то хэш. Пробуем зайти в админку с логином "admin" и этим паролем.

Можем попробовать подключиться к админке через CLI:

```shell
# argocd login <ARGOCD_SERVER>
```

__Изменить пароль админа можно через CLI или веб-морду__:

```shell
# argocd account update-password
```

![Смена пароля админа через CLI](../../Images/Change_password_CLI.png)

В веб-админке пароль меняем здесь: https://localhost:8080/user-info
User Info -> Сверху кнопка "Update password"

![Смена пароля админа](../../Images/Change_password.png)
