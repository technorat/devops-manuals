# Установка ArgoCD

Т.к. это специализированный инструмент для работы с Kubernetes, то на практике этот продукт устанавливают практически всегда в кластер K8s. Если в одном окружении (Dev, Uat, Prod, Preprod, Test) размещено несколько кластеров K8s, то ArgoCD может быть в любом из них. __Но для каждого из окружений (Dev, Uat, Prod, Preprod, Test) должен быть свой экземпляр ArgoCD__.

Подробнее об установке в K8s описано [здесь](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Docs/Installing/K8s.md).
