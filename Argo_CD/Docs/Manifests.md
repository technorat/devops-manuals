# Манифесты приложений Argo CD

Новое приложение можно создавать через web-интерфейс, а можно через подготовленный манифест. Пример манифеста можно увидеть 
[здесь](https://gitlab.com/technorat/devops-manuals/-/blob/main/Argo_CD/Manifests/Application-example.yml).
В source указываем, из какого репозитория и какой папки брать чарт.
В destination указываем кластер и namespace, в какой будем разворачивать.
А в project определяем, к какому проекту будет относиться наше приложение.
