# Создание приложения для синхронизации в ArgoCD

1. В веб-админке ArgoCD слева в меню выбираем пункт "Applications" и попадаем на [страницу управления приложениями](https://localhost:8080/applications)

![Страница управления приложениями](../Images/Create_app1.png)

2. Нажимаем кнопку "New App", открывается страница с полями для добавления приложения:

![Страница создания приложения](../Images/Create_app2.png)

## Заполнение полей при создании приложения

__GENERAL__

* Application Name: (Обязательно) Указываем имя приложения

* Project Name: ()

__Sync Policy__

* Выпадающий список 
    * Manyal
    * Automatic
    * "-"
  Выбираем Automatic

* Set Deletion Finalizer

__Sync Options__

* Skip Schema Validation
* Auto-Create Namespace
* Prune Last
* Apply Out of Sync Only
* Respect Ignore Differences
* Server-Side Apply
* Prune Propagation Policy - выпадающий список:
    * foreground
    * background
    * orphan
* Replace
* Retry

## SOURCE

* Repository URL

* GIT

* Revision

* Branches

* Path

## DESTINATION

* Cluster URL

* URL

* Namespace

## DIRECTORY - выпадающий список:

    * Directory
        - DIRECTORY RECURSE
        - TOP-LEVEL ARGUMENTS
        - EXTERNAL VARIABLES
        - INCLUDE
        - EXCLUDE
    * Helm
        - VALUES FILES
        - VALUES
    * Kustomize
        - VERSION
        - NAME PREFIX
        - NAME SUFFIX
        - NAMESPACE
    * Plugin
        - NAME
        - ENV
