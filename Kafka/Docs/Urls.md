# Ссылки

  - [Официальный сайт с документацией](https://kafka.apache.org/)

  - [https://docs.redhat.com/en/documentation/red_hat_streams_for_apache_kafka/2.7/html/using_streams_for_apache_kafka_on_rhel_with_zookeeper/assembly-getting-started-str#considerations-for-data-storage-str](https://docs.redhat.com/en/documentation/red_hat_streams_for_apache_kafka/2.7/html/using_streams_for_apache_kafka_on_rhel_with_zookeeper/assembly-getting-started-str#considerations-for-data-storage-str)

  - [https://docs.confluent.io/platform/current/kafka/deployment.html#disks](https://docs.confluent.io/platform/current/kafka/deployment.html#disks)
