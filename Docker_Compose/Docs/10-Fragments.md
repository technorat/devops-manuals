# [Фрагменты](https://github.com/compose-spec/compose-spec/blob/master/10-fragments.md)

С помощью Compose вы можете использовать встроенные функции YAML, чтобы сделать файл Compose более компактным и эффективным. Якоря и псевдонимы позволяют 
создавать повторно используемые блоки. Это полезно, если вы начнете находить общие конфигурации, охватывающие несколько служб. Наличие многоразовых блоков 
сводит к минимуму потенциальные ошибки.

Якоря создаются с помощью знака "&". За знаком следует псевдоним. Вы можете использовать этот псевдоним со знаком * позже для ссылки на значение, 
следующее за "якорем". Убедитесь, что между символами "&" и "*" и следующим псевдонимом нет пробела. Вы можете использовать более одного "якоря" и 
псевдонима в одном файле Compose. В приведенном ниже примере привязка тома по умолчанию создается на основе тома данных базы данных. Позже он повторно 
используется под псевдонимом *default-volume для определения объема метрик. Разрешение привязки происходит до 
[интерполяции переменных](https://github.com/compose-spec/compose-spec/blob/master/12-interpolation.md), поэтому переменные нельзя использовать для 
установки "якорей" или псевдонимов.
**Пример1**:

```
volumes:
  db-data: &default-volume
    driver: default
  metrics: *default-volume
```

Если у вас есть "якорь", который вы хотите использовать в нескольких службах, используйте его вместе с 
[расширением](https://github.com/compose-spec/compose-spec/blob/master/11-extension.md), чтобы упростить обслуживание файла Compose.
**Пример2**:

```
services:
  first:
    image: my-image:latest
    environment: &env
      - CONFIG_KEY
      - EXAMPLE_KEY
      - DEMO_VAR
  second:
    image: another-image:latest
    environment: *env
```

Возможно, вы захотите частично переопределить значения. Составление следует правилу, изложенному типом 
[слияния YAML](https://yaml.org/type/merge.html). В следующем примере спецификация тома метрик использует псевдоним, чтобы избежать повторения, но 
переопределяет атрибут имени:

```
services:
  backend:
    image: example/database
    volumes:
      - db-data
      - metrics
volumes:
  db-data: &default-volume
    driver: default
    name: "data"
  metrics:
    <<: *default-volume
    name: "metrics"
```

Вы также можете расширить "якорь", чтобы добавить дополнительные значения. [Слияние YAML](https://yaml.org/type/merge.html) применяется только к 
сопоставлениям и не может использоваться с последовательностями. В приведенном выше примере переменные среды должны быть объявлены с использованием 
синтаксиса отображения **FOO: BAR**, тогда как синтаксис последовательности **- FOO=BAR** действителен только в том случае, если не задействованы никакие 
фрагменты.



