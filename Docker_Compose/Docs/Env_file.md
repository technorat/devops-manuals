# .env - файл

Docker Compose просматривают по умолчанию текущий каталог. Если в нем оказывается .env - файл, то определенные в нем параметры добавляются к текущему
окружению среды при обработке compose.yaml. Значения в файле .env записываются в следующих обозначениях:

```
VARIABLE_NAME=value
```

К объявленным в .env - файле переменным можно обращаться в compose.yaml в виде "${VARIABLE_NAME}". Пример файла compose.yaml, который использует 
значения из файла .env, для установки переменных среды будущего контейнера:

```
version: '3'

services:
  example:
    image: ubuntu-alpine
    environment:
      - env_var_name=${VARIABLE_NAME}
```

**Подсказка**: при работе с файлом .env вы можете отлаживать файлы docker-compose.yml с помощью одной команды. Введите команду

```
docker-compose config
```

Таким образом, вы увидите, как выглядит содержимое файла docker-compose.yml после этапа замены, без запуска чего-либо еще.

**ВНИМАНИЕ!!!** Переменные среды на вашем хосте **переопределят** значения, указанные в вашем файле .env. Да, вы правильно прочитали. Подробнее читайте 
[здесь](https://vsupalov.com/override-docker-compose-dot-env/) или ознакомьтесь с 
[документацией](https://docs.docker.com/compose/environment-variables/envvars-precedence/) по приоритету env var для compose.