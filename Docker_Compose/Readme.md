# Docker Compose
---

## Оглавление

- [Часто задаваемые вопросы (ЧАВО/FAQ) по Docker Compose](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker_Compose/Docs/FAQ.md)

### Спецификация Compose-файла

03. [Compose-файл](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker_Compose/Docs/03-Compose_file.md)
10. [Фрагменты](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker_Compose/Docs/10-Fragments.md)

## Полезные ссылки

- [Примеры Docker-compose файлов (Awesome Docker Compose samples)](https://github.com/docker/awesome-compose)
- [Официальная документация по Docker Compose](docs.docker.com/compose/)
- [Краткое описание инструкций Docker-compose файлов](https://docs.docker.com/compose/intro/compose-application-model/)
- [Описание инструкций Docker-compose файлов](https://github.com/compose-spec/compose-spec)