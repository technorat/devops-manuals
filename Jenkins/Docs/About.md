# О Jenkins

## Что это

Что такое Jenkins

Jenkins – не просто инструмент CI/CD. Это Framework, потому что он:

* Гибок и расширяем. Jenkins — опенсорсный проект с множеством внешних расширений.

* Минимален из коробки. У Jenkins есть контроллер. Вы можете подключить к нему несколько слоев и уже на этом сетапе собрать минимальный пайплайн, который позволит автоматизировать работу по 
обновлению сервисов.

* Требует настройки. Jenkins — один из кубиков, с помощью которого можно построить большую систему автоматизации. Но прежде чем сделать что-то, его придётся настроить.

Jenkins — это Java-приложение. У него есть контроллер или Master Mode — управляющий центр, который занимается планированием задач. Он запускает задачи согласно установленному расписанию на 
слэйвах, которые вы к нему прикрепили. Помимо этого контроллер хранит логи наших задач. Вся история хранится только на Master Mode, поэтому важно помнить о настройке правильной ротации логов.

Слэйвы или агенты — это то, что непосредственно выполняет сами задания. 

Коротко их взаимодействие можно описать так: контроллер запускает задачу и говорит агенту выполнить её, агент выполняет задачу и возвращает результат контроллеру. Контроллер получает результат 
и сохраняет его в build-логе.

![Структура Jenkins](Images/About.png)

Jenkinsfile – это простой текстовый файл с кодом на языке Groovy, который используется для конфигурации трубы Jenkins. 

