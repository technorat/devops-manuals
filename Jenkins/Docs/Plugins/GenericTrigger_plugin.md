

## Кусок рабочего конфига:
GenericTrigger(
            genericVariables: [
                [key: 'pullRequest', value: '$.pullRequest'],
                [key: 'eventKey', value: '$.eventKey']
            ],
            causeString: 'Triggered on ${pullRequest_title}',
            token: 'UGnnhMBaAiWQx4',
            printContributedVariables: false
        )
А потом:
stage('Init PR properties') {
            steps {
                script {
                    if (!params.jiraCredentialId) {
                        error "Jira credentials are required"
                    } else if (!eventKey  eventKey != "pr:merged") {
                        error "Expected a pullrequest event, got ${eventKey}"
                    } else {
                        // For documentation on the payload strucure, see https://confluence.atlassian.com/bitbucketserver075/event-payload-1018785129.html#Eventpayload-Merged
                        def pr = readJSON text:pullRequest
                        gitReposUrl = pr.fromRef?.repository?.links?.clone?.find( { clone ->
                            clone.name == "ssh"
                        } )?.href
                        gitReposName = pr.fromRef?.repository?.name ?: ''
                        gitCommitId = pr.properties?.mergeCommit?.id
                        prTitle = pr.title ?: 'Pull Request'
                        prAuthor = pr.author?.user?.displayName ?: 'Unknown'
                        prLink = pr.links?.self?.href
                        if (!gitReposUrl  !gitCommitId) {
                            error "Could not parse trigger data as pullrequest"
                        }
                        def issueMatcher = prTitle =~ ~/SRCH-\d+/
                        while (issueMatcher.find()) {
                            issueKeys.add(issueMatcher.group())
                        }
                        currentBuild.displayName = "${gitReposName}/${pr.id