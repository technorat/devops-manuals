# Плагины Jenkins

## Оглавление


## Предварительная инфа

До того, как начали ставить плагины, при настройке задания в дженкинсе были только такие пункты:

**Общие** 
* GitHub project
* Throttle builds
* Удалять устаревшие сборки
* Это - параметризованная сборка
* Разрешить параллельный запуск задачи

**Управление исходным кодом**
* Нет
* Git

**Триггеры сборки**
* Trigger builds remotely (e.g., from scripts)
* Запустить по окончанию сборки других проектов
* Запускать периодически
* GitHub hook trigger for GITScm polling
* Опрашивать SCM об изменениях

**Среда сборки**
* Delete workspace before build starts
* Use secret text(s) or file(s)
* Add timestamps to the Console Output
* Inspect build log for published build scans
* Terminate a build if it's stuck
* With Ant

## Список

### Bitbucket Server Integration

После установки в настройках задачи появляются пункты:
**Управление исходным кодом **
* Bitbucket Server

**Триггеры сборки**
* Bitbucket webhook trigger
