# Jenkinsfile

## Базовый файл

JenkinsFile – это простой текстовый файл, который используется для создания пайплайна в виде кода в Jenkins. Он содержит код на Groovy Domain Specific Language (DSL), который прост в написании 
и удобочитаем. Вы можете запустить JenkinsFile отдельно, либо вы можете запустить этот код из Jenkins Web UI. Существует два способа создания пайплайна с помощью Jenkins.В файле Jenkinsfile 
описывается вся последовательность шагов Pipeline-а. Все инструкции должны быть внутри конструкции

```
pipeline {
    /* Ваши инструкции для конвейера */
}
```

Блок "pipeline" содержит разделы. Первым разделом является "agent". Раздел "agent" указывает, где будет выполняться pipeline Jenkins. Раздел должен быть определен на верхнем уровне внутри 
блока "pipeline".

```
В моём случае конвейер будет работать на сервере TestNode:
pipeline {
  agent {
    node {
      label 'TestNode' 
    }
  }
}
```

Обычно, для pipeline требуется указание набора каких то переменных и параметров. Указать переменные можно в том же блоке "pipeline":

```
В моём случае конвейер будет работать на сервере TestNode:
pipeline {
  agent {
    node {
      label 'TestNode' 
    }
  }

  environment {
    TEST_PREFIX = "test-IMAGE"
    TEST_IMAGE = "${env.TEST_PREFIX}:${env.BUILD_NUMBER}"
    TEST_CONTAINER = "${env.TEST_PREFIX}-${env.BUILD_NUMBER}"
    REGISTRY_ADDRESS = "my.registry.address.com"
    SLACK_CHANNEL = "#deployment-notifications"
    SLACK_TEAM_DOMAIN = "MY-SLACK-TEAM"
    SLACK_TOKEN = credentials("slack_token")
    DEPLOY_URL = "https://deployment.example.com/"
    COMPOSE_FILE = "docker-compose.yml"
    REGISTRY_AUTH = credentials("docker-registry")
    STACK_PREFIX = "my-project-stack-name"
  }
}
```



Раздел "options" позволяет настраивать параметры конвейера непосредственно из Jenkinsfile. Эти параметры также можно 
задавать в графическом интерфейсе Jenkins.
options {
    ansiColor('xterm')
    timestamps()
    disableConcurrentBuilds()
    timeout(time: 1, unit: 'HOURS') 
    buildDiscarder(logRotator(artifactDaysToKeepStr: '7', artifactNumToKeepStr: '10', daysToKeepStr: '7', numToKeepStr: '50'))
}
