# Мануалы и заметки по стеку DevOps

## "Дорожная карта" по стэку DevOps 

[Рабочий набросок](https://gitlab.com/technorat/devops-manuals/-/blob/main/RoadMap.md) "дорожной карты" пока еще в работе, будет дополняться

## Ansible

## Hashicorp Vault

* [Установка Hashicorp Vault](https://gitlab.com/technorat/devops-manuals/-/tree/main/Hashicorp%20Vault?ref_type=heads)

## Linux

* [Установка локального репозитория пакетов Ubuntu из бинарного файла](https://gitlab.com/technorat/devops-manuals/-/blob/main/Linux/docs/Local%20repository%20(bin).md?ref_type=heads)

## Сети

* [Порты сетевых служб](https://gitlab.com/technorat/devops-manuals/-/tree/main/Ports?ref_type=heads)


