# Подготовка своей рабочей тачки к работе
---

1. Настраиваем статический ip-адрес. В зависимости от ОС сетевые настройки делалаются в разных местах.
1.1. В центоси или старой убунте это делается в файле /etc/network/interfaces
* Сначала посмотрим названия интерфейсов

```shell
# ip a | grep mtu
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
2: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
```

или

```shell
# ls /sys/class/net
enp4s0  lo
```

* Создаем/редактируем файл /etc/network/interfaces

```shell
# vi /etc/network/interfaces
# The loopback network interface
auto lo
iface lo inet loopback

# The main network interface
auto eth0
iface eth0 inet static
address 192.168.2.33
gateway 192.168.2.1
netmask 255.255.255.0
```

```shell
# systemctl restart networking
```

1.2. В новых версиях убунты это делается в файле /etc/00-installer-config.yaml

```shell
network:
  version: 2
  ethernets:
    enp0s3:
      renderer: NetworkManager
      match: {}
      addresses:
      - "192.168.255.150/24"
      nameservers:
        addresses:
        - 192.168.255.1
        - 8.8.8.8
      networkmanager:
        uuid: "1eef7e45-3b9d-3043-bee3-fc5925c90273"
        name: "netplan-enp0s3"
        passthrough:
          connection.timestamp: "1697904504"
          ipv4.address1: "192.168.255.150/24,192.168.255.1"
          ipv4.ignore-auto-dns: "true"
          ipv4.method: "manual"
          ipv6.method: "disabled"
          ipv6.ip6-privacy: "-1"
          proxy._: ""

или

network:
  ethernets:
    enp0s3:
      dhcp4: no
      addresses: [192.168.255.103/24]
      gateway4: 192.168.255.1
      routes:
        - to: default
          via: 192.168.255.1
      nameservers:
        addresses:
          - 8.8.8.8
          - 8.8.4.4
  version: 2
```

* Перезапустим сетевые службы чтобы применить настройки

```shell
# systemctl restart networking
```

2. Настройте DNS. Если нет специфичного сервера внутри компании или провайдера, можно использовать стандартные от гугла. Можно еще прописать домен для поиска, который будет подставляться 
в качестве суффикса при поиске по короткому имени (example.com).

```shell
# vi /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
search example.com
```

3. Настраиваем репозитории apt и устанавливаем весь нужный софт

```shell
# apt update
# apt install curl wget mc python3 python3-pip far2l nmap net-tools git ansible kubectl software-properties-common apt-transport-https lens asbru-cm ca-certificates gnupg
# 
# wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
# ~~apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF~~
# add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
#
# apt update
# apt install code
#
# ~~curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg~~
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu mantic stable"
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  7EA0A9C3F273FCD8
# apt update
# apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-compose
```

4. Генерим SSH-ключи для работы с разными серверами. Тип шифрования лучше выбирать - ed25519. Это более надежное и быстрое шифрование, чем rsa. 

```shell
# ssh-keygen -t ed25519 -b 4096
Generating public/private ed25519 key pair.
Enter file in which to save the key (/root/.ssh/id_ed25519): /root/.ssh/dark
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/dark
Your public key has been saved in /root/.ssh/dark.pub
```

5. Чтобы настроить автоматическую работу с каким то сервером (например с gitlab.com) по ключам, в личной папке пользователя создаем конфиг-файл. Для каждого сервера
добавляем вот такой блок настроек

```shell
# vi cat ~/.ssh/config 
User sergey
Hostname gitlab.com
Host gitlab.com
IdentityFile ~/.ssh/dark
TCPKeepAlive yes
IdentitiesOnly yes
```

6. В гитлабе надо зайти в профиль, SSH Keys и добавить публичный ключ, созданный в предыдущем шаге

7. Настроим git для работы с gitlab.com

```shell
# git config --global user.email "sergey_privacy@mail.ru"
# git config --global user.name "technorat"
```

8. Если Asbru-cm не установился, то скачаем "портабельную" (.AppImage) версию отсюда: [https://github.com/asbru-cm/asbru-cm/releases](https://github.com/asbru-cm/asbru-cm/releases)
Ее просто скачиваем в папку и делаем исполняемой

```shell
# mkdir /asbru-cm
# cd /asbru-cm
# wget https://github.com/asbru-cm/asbru-cm/releases/download/6.4.0/asbru-cm-6.4.0-1-x86_64.AppImage
# chmod 777 asbru-cm-6.4.0-1-x86_64.AppImage
```

