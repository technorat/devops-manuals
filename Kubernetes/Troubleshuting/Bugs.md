# Ошибки при работе с Kubernetes

## Оглавление

1. [Job in version \"v1\" cannot be handled as a Job: json: cannot unmarshal object into Go struct field JobSpec.spec.backoffLimit of type int32", "reason": "BadRequest"]()

2. [Error from server (BadRequest): error when creating "nginx-deployment-with-resource.yaml": Deployment in version "v1" cannot be handled as a Deployment: quantities must match the regular expression '^([+-]?[0-9.]+)([eEinumkKMGTP]*[-+]?[0-9]*)$']()