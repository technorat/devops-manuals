# Ingress Controller
## Виды
* [Ingress-Nginx Controller] (https://kubernetes.github.io/ingress-nginx/)
* [HAproxy] (https://github.com/jcmoraisjr/ingress-nginx)
* [contour] (https://github.com/projectcontour/contour)
* [traefik] (https://github.com/traefik/traefik)
* [istio] (https://github.com/istio/istio)
* [kubernetes-sigs] (https://github.com/kubernetes-sigs/aws-load-balancer-controller)
* [ingress-gce] (https://github.com/kubernetes/ingress-gce)
* [ingress-gce] (https://github.com/Azure/application-gateway-kubernetes-ingress)

## Какие функции реализует
* динамическое обнаружение сервисов (service discovery)
* SSL-терминирование
* работа с websocket'ами
* reverse-proxy
* балансировка нагрузки (round-robin, rdp-cookie, sticky sessions)
* Basic, digest, oauth, external-auth
* Базовые механизмы защиты от DDOS и WAF
