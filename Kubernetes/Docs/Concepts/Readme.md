# Основные концепции

У Kubernetes есть свой набор терминов, который дает некоторое представление об организации ресурсов кластера. 
О **[ресурсах/объектах](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Objects.md)** нужно думать как о некотором наборе строительных блоков или абстракций, 
которые предоставляются API Kubernetes для запуска программного обеспечения в кластере. Подробно каждый из этих ресурсов мы рассмотрим в отдельных статьях, а сейчас просто 
перечислим их:

* Annotations - добавление произвольных неидентифицирующих метаданных к объектам;
* **[ConfigMaps](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Configuration/ConfigMaps.md)** - позволяет переопределить конфигурацию запускаемых подов;
* DaemonSet - гарантирует, что определенный под будет запущен на всех (или некоторых) нодах;
* Deployments - обеспечивает декларативные (declarative) обновления для Pods и ReplicaSets;
* Jobs (в том числе CronJob) - создает один (или несколько) подов и гарантирует, что после выполнения команды они будут успешно завершены (terminated);
* Labels and Selectors - пары ключ/значение, которые присваиваются объектам (например, подам). С помощью селекторов пользователь может идентифицировать объект;
* Namespaces - виртуальные кластеры размещенные поверх физического;
* Pods - минимальная сущность (юнит) для развертывания в кластере;
* ReplicaSets (ранее Replication Controller) - гарантирует, что в определенный момент времени будет запущено нужно кол-во контейнеров;
* Secrets - используются для хранения конфиденциальной информации (пароли, токены, ssh-ключи).
* **[Services (Сервисы)](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Concepts/Network_resources/Services.md)** - абстракция, которая определяет логический набор подов
 и политику доступа к ним;
* StatefulSets - используется для управления приложениями с сохранением состояния;

## [Список базовых ресурсов Kubernetes](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Concepts/Basic_resources.md)

* [Сервисы, балансировка нагрузки и сеть]
