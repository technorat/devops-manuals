# Сервисы, балансировка нагрузки и сеть

Какие есть способы "выпустить наружу" приложение, доступное только внутри кластера?

1. Ingress - рекомендовано к Production-окружению

2. Load balancing device - влияет на стоимость облака

3. NodePort - простой, но не очень гибкий, рекомендовано к DEV-окружению

* [Сервисы](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Network_resources/Services.md)
