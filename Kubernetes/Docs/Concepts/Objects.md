# Объекты

## Что это?

Объекты Kubernetes — сущности, которые хранятся в Kubernetes. Kubernetes использует их для представления состояния кластера. В частности, они описывают следующую информацию:

* Какие контейнеризированные приложения запущены (и на каких узлах).
* Доступные ресурсы для этих приложений.
* Стратегии управления приложения, которые относятся, например, к перезапуску, обновлению или отказоустойчивости.

После создания объекта Kubernetes будет следить за существованием объекта. Создавая объект, вы таким образом указываете системе Kubernetes, какой должна быть рабочая нагрузка кластера; это 
требуемое состояние кластера. Для работы с объектами Kubernetes – будь то создание, изменение или удаление — нужно использовать API Kubernetes. Например, при использовании CLI-инструмента 
kubectl, он обращается к API Kubernetes. 

## Создание объектов 

Создавать объекты можно хоть напрямую (высшая магия Хоггвардса), передавая запросы в JSON-формате через curl. Наиболее удобный и часто используемый на 
практике вариант - это создание объектов через чарты и шаблоны Helm.
Почти в каждом объекте Kubernetes есть два вложенных поля-объекта, которые управляют конфигурацией объекта: spec и status. При создании объекта в поле spec указывается требуемое состояние 
(описание характеристик, которые должны быть у объекта). Поле status описывает текущее состояние объекта, которое создаётся и обновляется самим Kubernetes и его компонентами. Плоскость 
управления Kubernetes непрерывно управляет фактическим состоянием каждого объекта, чтобы оно соответствовало требуемому состоянию, которое было задано пользователем.
Информацию по API смотрим [здесь](https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md)

Пример .yaml-манифеста, для создания объекта Deployment в Kubernetes:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

Чтобы использовать этот манифест, используем команду 

```shell
# kubectl apply -f ./deployment.yaml --record
```

Команда "kubectl" переделает удобный людям yaml в удобный машине JSON от отправит на API Kubernetes. 
В файле .yaml создаваемого объекта Kubernetes необходимо указать значения для следующих полей:

* apiVersion — используемая для создания объекта версия API Kubernetes
* kind — тип создаваемого объекта
* metadata — данные, позволяющие идентифицировать объект (name, UID и необязательное поле namespace)
* spec — требуемое состояние объекта

Конкретный формат поля-объекта spec зависит от типа объекта Kubernetes и содержит вложенные 
поля, предназначенные только для используемого объекта. В [справочнике API Kubernetes](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.29/) можно найти формат спецификации 
любого объекта Kubernetes. Ключ "--record" нужен для того, чтобы эти действия отражались в журнале аудита.

## Управление объектами

Есть 3 тактики управления объектами:

* **Императивные команды** - плохой подход, рекомендуется применять только в домашней среде на этапе изучения. При использовании императивных команд пользователь работает непосредственно с 
активными (текущими) объектами в кластере. Пользователь указывает выполняемые операции команде kubectl в качестве аргументов или флагов (kubectl create, run, delete и т.д.). Например запуск 
nginx, посредством создания объекта Deployment

```
# kubectl run nginx --image nginx
```

или

```
# kubectl create deployment nginx --image nginx
```

* **Императивная конфигурация объекта** - В случае использования императивной конфигурации объекта команде kubectl устанавливают действие (создание, замена и т.д.), необязательные флаги и как 
минимум одно имя файла. Файл должен содержать полное определение объекта в формате YAML или JSON. 
Вариант чуть лучше предыдущено, но хуже декларативного подхода. В серьезном проекте преимущественно 3-й вариант применяется.
Предупреждение: Императивная команда replace заменяет существующую спецификацию новой 
(переданной), удаляя все изменения в объекте, которые не определены в конфигурационном файле. Такой подход не следует использовать для типов ресурсов, спецификации которых обновляются 
независимо от конфигурационного файла. Например, поле externalIPs в сервисах типа LoadBalancer обновляется кластером независимо от конфигурации. Например:

Создать объекты, определенные в конфигурационном файле:

```
# kubectl create -f nginx.yaml
```

Удалить объекты, определенные в двух конфигурационных файлах:

```
# kubectl delete -f nginx.yaml -f redis.yaml
```

Обновить объекты, определенные в конфигурационном файле, перезаписав текущую конфигурацию:

```
# kubectl replace -f nginx.yaml
```

* **Декларативная конфигурация объекта**. При использовании декларативной конфигурации объекта пользователь работает с локальными конфигурационными файлами объекта, при этом он не определяет 
операции, которые будут выполняться над этими файлами. Операции создания, обновления и удаления автоматически для каждого объекта определяются kubectl. Этот механизм позволяет работать с 
директориями, в ситуациях, когда для разных объектов может потребоваться выполнение других операций.
Примечание: Декларативная конфигурация объекта сохраняет изменения, сделанные другими, даже если эти изменения не будут зафиксированы снова в конфигурационный файл объекта. Это достигается путем использования API-операции patch, чтобы записать только обнаруженные изменения, а не использовать для этого API-операцию replace, которая полностью заменяет конфигурацию объекта.
Примеры:
Обработать все конфигурационные файлы объектов в директории configs и создать либо частично обновить активные объекты. Сначала можно выполнить diff, чтобы посмотреть, какие изменения будут внесены, и только после этого применить их:

```
kubectl diff -f configs/
kubectl apply -f configs/
```

Рекурсивная обработка директорий:

```
kubectl diff -R -f configs/
kubectl apply -R -f configs/
```

## Имена

Каждый объект в кластере имеет уникальное имя для конкретного типа ресурса. Кроме этого, у каждого объекта Kubernetes есть собственный уникальный идентификатор (UID) в пределах кластера.
Например, в одном и том же пространстве имён может быть только один Pod-объект с именем "myapp-1234", и при этом существовать объект Deployment с этим же названием "myapp-1234".

###Требования к именам ресурсов:###

**Имена поддоменов DNS**

Большинству типов ресурсов нужно указать имя, используемое в качестве имени поддомена DNS в соответствии с RFC 1123. Соответственно, имя должно:

- содержать не более 253 символов
- иметь только строчные буквенно-цифровые символы, '-' или '.'
- начинаться с буквенно-цифрового символа
- заканчивается буквенно-цифровым символом

**Имена меток DNS**

Некоторые типы ресурсов должны соответствовать стандарту меток DNS, который описан в RFC 1123. Таким образом, имя должно:

- содержать не более 63 символов
- содержать только строчные буквенно-цифровые символы или '-'
- начинаться с буквенно-цифрового символа
- заканчивается буквенно-цифровым символом

**Имена сегментов пути**

Определённые имена типов ресурсов должны быть закодированы для использования в качестве сегмента пути. Проще говоря, имя не может быть "." или "..", а также не может содержать "/" или "%".

**Уникальные идентификаторы**

Уникальная строка, сгенерированная самим Kubernetes, для идентификации объектов. У каждого объекта, созданного в течение всего периода работы кластера Kubernetes, есть собственный уникальный 
идентификатор (UID). Он предназначен для выяснения различий между событиями похожих сущностей. Уникальные идентификатор (UID) в Kubernetes — это универсальные уникальные идентификаторы 
(известные также как Universally Unique IDentifier, сокращенно UUID).

## Пространства имён (Namespaces)

Kubernetes поддерживает несколько виртуальных кластеров в одном физическом кластере. Такие виртуальные кластеры называются пространствами имён. Пространства имен удобно использовать для 
отделения ресурсов одного проекта от ресурсов другого. Имена ресурсов должны быть уникальными в пределах одного и того же пространства имён. Пространства имён не могут быть вложенными, а 
каждый ресурс Kubernetes может находиться только в одном пространстве имён. Используйте следующую команду, чтобы вывести список существующих пространств имён в кластере:

```
# kubectl get namespace
```

**По умолчанию в Kubernetes определены три пространства имён**:

- "default" — пространство имён по умолчанию для объектов без какого-либо другого пространства имён.
- "kube-system" — пространство имён для объектов, созданных Kubernetes
- "kube-public" — создаваемое автоматически пространство имён, которое доступно для чтения всем пользователям (включая также неаутентифицированных пользователей). Как правило, это пространство 
имён используется кластером, если некоторые ресурсы должны быть общедоступными для всего кластера. Главная особенность этого пространства имён — оно всего лишь соглашение, а не требование.
Используйте флаг --namespace, чтобы определить пространство имён только для текущего запроса:

```
# kubectl get pods --namespace=<namespace>
```

**Можно определить пространство имён, которое должно использоваться для всех выполняемых команд kubectl в текущем контексте:**

```
# kubectl config set-context --current --namespace=<insert-namespace-name-here>
```

Проверка

```
#kubectl config view --minify | grep namespace:
```

## Пространства имён и DNS

При создании сервиса создаётся соответствующая ему DNS-запись. Эта запись вида <service-name>.<namespace-name>.svc.cluster.local означает, что если контейнер использует только <service-name>, 
то он будет локальным сервисом в пространстве имён. Это позволит применять одну и ту же конфигурацию в нескольких пространствах имен (например, development, staging и production). Если нужно 
обращаться к другим пространствам имён, то нужно использовать полностью определенное имя домена (FQDN).


## Объекты без пространства имён

Большинство ресурсов Kubernetes (например, поды, сервисы, контроллеры репликации и другие) расположены в определённых пространствах имён. При этом сами объекты типа Namespace не находятся 
ни в каких пространствах имён. А так же низкоуровневые ресурсы, как узлы и persistentVolumes, не принадлежат ни одному пространству имён.
Чтобы посмотреть, какие ресурсы Kubernetes находятся в пространстве имён, а какие — нет, используйте следующие команды:

**Ресурсы в пространстве имён**

```
# kubectl api-resources --namespaced=true
```

**Ресурсы, не принадлежавшие ни одному пространству имён**

```
# kubectl api-resources --namespaced=false
```

## Создание объекта Namespace

**Императивное создание:**

```
# kubectl create namespace <namespace>
```

**Создание с помощью манифеста:**

```
# cat namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: <namespace>

# kubectl create -f ./namespace.yaml
```