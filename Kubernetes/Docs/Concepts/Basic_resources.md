# Базовые ресурсы K8s
| Название | Алиас | Версия API | Namespace | Описание |
|---|---|---|---|---|
| bindings | | v1 | да | Binding | 
| csidrivers | | storage.k8s.io/v1 | нет | CSIDriver | 
| csinodes | | storage.k8s.io/v1 | нет | CSINode | 
| csistoragecapacities | | storage.k8s.io/v1beta1 | да | CSIStorageCapacity | 
| componentstatuses | cs | v1 | нет | ComponentStatus | 
| configmaps | cm | v1 | да | ConfigMap | 
| endpoints | ep | v1 | да | Endpoints | 
| events | ev | v1 | да | Event | 
| limitranges | limits | v1 | да | LimitRange | 
| namespaces | ns | v1 | нет | Namespace | 
| nodes | no | v1 | нет | Node | 
| persistentvolumeclaims | pvc | v1 | да | PersistentVolumeClaim | 
| persistentvolumes | pv | v1 | нет | PersistentVolume | 
| pods | po | v1 | да | Pod | 
| podtemplates | | v1 | да | PodTemplate | 
| replicationcontrollers | rc | v1 | да | ReplicationController | 
| resourcequotas | quota | v1 | да | ResourceQuota | 
| secrets | | v1 | да | Secret | 
| serviceaccounts | sa | v1 | да | ServiceAccount | 
| services | svc | v1 | да | Service | 
| mutatingwebhookconfigurations | | admissionregistration.k8s.io/v1 | нет | MutatingWebhookConfiguration | 
| validatingwebhookconfigurations | | admissionregistration.k8s.io/v1 | нет | ValidatingWebhookConfiguration | 
| customresourcedefinitions | crd,crds | apiextensions.k8s.io/v1 | нет | CustomResourceDefinition | 
| apiservices | | apiregistration.k8s.io/v1 | нет | APIService | 
| controllerrevisions | | apps/v1 | да | ControllerRevision | 
| daemonsets | ds | apps/v1 | да | DaemonSet | 
| deployments | deploy | apps/v1 | да | Deployment | 
| replicasets | rs | apps/v1 | да | ReplicaSet | 
| statefulsets | sts | apps/v1 | да | StatefulSet | 
| tokenreviews | | authentication.k8s.io/v1 | нет | TokenReview | 
| localsubjectaccessreviews | | authorization.k8s.io/v1 | да | LocalSubjectAccessReview | 
| selfsubjectaccessreviews | | authorization.k8s.io/v1 | нет | SelfSubjectAccessReview | 
| selfsubjectrulesreviews | | authorization.k8s.io/v1 | нет | SelfSubjectRulesReview | 
| subjectaccessreviews | | authorization.k8s.io/v1 | нет | SubjectAccessReview | 
| horizontalpodautoscalers | hpa | autoscaling/v2 | да | HorizontalPodAutoscaler | 
| cronjobs | cj | batch/v1 | да | CronJob | 
| jobs | | batch/v1 | да | Job | 
| certificatesigningrequests | csr | certificates.k8s.io/v1 | нет | CertificateSigningRequest | 
| leases | | coordination.k8s.io/v1 | да | Lease | 
| bgpconfigurations | | crd.projectcalico.org/v1 | нет | BGPConfiguration | 
| bgppeers | | crd.projectcalico.org/v1 | нет | BGPPeer | 
| blockaffinities | | crd.projectcalico.org/v1 | нет | BlockAffinity | 
| caliconodestatuses | | crd.projectcalico.org/v1 | нет | CalicoNodeStatus | 
| clusterinformations | | crd.projectcalico.org/v1 | нет | ClusterInformation | 
| felixconfigurations | | crd.projectcalico.org/v1 | нет | FelixConfiguration | 
| globalnetworkpolicies | | crd.projectcalico.org/v1 | нет | GlobalNetworkPolicy | 
| globalnetworksets | | crd.projectcalico.org/v1 | нет | GlobalNetworkSet | 
| hostendpoints | | crd.projectcalico.org/v1 | нет | HostEndpoint | 
| ipamblocks | | crd.projectcalico.org/v1 | нет | IPAMBlock | 
| ipamconfigs | | crd.projectcalico.org/v1 | нет | IPAMConfig | 
| ipamhandles | | crd.projectcalico.org/v1 | нет | IPAMHandle | 
| ippools | | crd.projectcalico.org/v1 | нет | IPPool | 
| ipreservations | | crd.projectcalico.org/v1 | нет | IPReservation | 
| kubecontrollersconfigurations | | crd.projectcalico.org/v1 | нет | KubeControllersConfiguration | 
| networkpolicies | | crd.projectcalico.org/v1 | да | NetworkPolicy | 
| networksets | | crd.projectcalico.org/v1 | да | NetworkSet | 
| endpointslices | | discovery.k8s.io/v1 | да | EndpointSlice | 
| events | ev | events.k8s.io/v1 | да | Event | 
| flowschemas | | flowcontrol.apiserver.k8s.io/v1beta2 | нет | FlowSchema | 
| prioritylevelconfigurations | | flowcontrol.apiserver.k8s.io/v1beta2 | нет | PriorityLevelConfiguration | 
| nodes | | metrics.k8s.io/v1beta1 | нет | NodeMetrics | 
| pods | | metrics.k8s.io/v1beta1 | да | PodMetrics | 
| alertmanagerconfigs | amcfg | monitoring.coreos.com/v1alpha1 | да | AlertmanagerConfig | 
| alertmanagers | am | monitoring.coreos.com/v1 | да | Alertmanager | 
| podmonitors | pmon | monitoring.coreos.com/v1 | да | PodMonitor | 
| probes | prb | monitoring.coreos.com/v1 | да | Probe | 
| prometheusagents | promagent | monitoring.coreos.com/v1alpha1 | да | PrometheusAgent | 
| prometheuses | prom | monitoring.coreos.com/v1 | да | Prometheus | 
| prometheusrules | promrule | monitoring.coreos.com/v1 | да | PrometheusRule | 
| scrapeconfigs | scfg | monitoring.coreos.com/v1alpha1 | да | ScrapeConfig | 
| servicemonitors | smon | monitoring.coreos.com/v1 | да | ServiceMonitor | 
| thanosrulers | ruler | monitoring.coreos.com/v1 | да | ThanosRuler | 
| ingressclasses | | networking.k8s.io/v1 | нет | IngressClass | 
| ingresses | ing | networking.k8s.io/v1 | да | Ingress - сущность, в которой описана конфигурация для ingress controller |
| networkpolicies | netpol | networking.k8s.io/v1 | да | NetworkPolicy | 
| runtimeclasses | | node.k8s.io/v1 | нет | RuntimeClass | 
| poddisruptionbudgets | pdb | policy/v1 | да | PodDisruptionBudget | 
| podsecuritypolicies | psp | policy/v1beta1 | нет | PodSecurityPolicy | 
| clusterrolebindings | | rbac.authorization.k8s.io/v1 | нет | ClusterRoleBinding | 
| clusterroles | | rbac.authorization.k8s.io/v1 | нет | ClusterRole | 
| rolebindings | | rbac.authorization.k8s.io/v1 | да | RoleBinding | 
| roles | | rbac.authorization.k8s.io/v1 | да | Role | 
| priorityclasses | pc | scheduling.k8s.io/v1 | нет | PriorityClass | 
| volumeattachments | | storage.k8s.io/v1 | нет | VolumeAttachment | 
| storageclasses | sc | storage.k8s.io/v1 | нет | StorageClass | 
