# Ingress и Ingress Controller
[Официальная документация по ingress controller на базе nginx] (https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/)
Ingress - сущность кластера kubernetes, в которой вы описываете конфигурацию для ingress controller.
Ingress Controller - занимается непосредственно обработкой траффика. Его конфигурация формируется из всех ingress, которые есть в кластере.

Реализация ingress controller может быть на базе различного софта - nginx, haproxy, traefik и т.д. Стандартно Kubernetes использует контроллер на базе Nginx. Это самый 
простой и популярный вариант. В проде обычно ingress контроллеры вешают на отдельные узлы с внешними ip адресами и используют в качестве точки подключения к сервисам. Ingress 
controller может как сам принимать на себя весь трафик, так и работать за каким-то внешним балансером. Нужно понимать, что ingress работает на 7-м уровне сетевой модели OSI. Если вам нужен 3-й уровень, используйте другие способы публикации приложений:
* ClusterIP
* NodePort
* LoadBalancer
* ExternalName
* ExternalIP

Так как внутри Ingress контроллера обычный Nginx, вы можете очень гибко управлять распределением запросов по доменам, location и т.д. Вам доступно все, что умеет nginx в функционале [proxy_pass](https://serveradmin.ru/nginx-proxy_pass/).
[Официальная документация по ingress controller на базе nginx](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/).