# [ConfigMap-ы](https://kubernetes.io/docs/concepts/configuration/configmap/)

ConfigMap — это объект API, используемый для хранения неконфиденциальных данных в парах ключ-значение. Модули могут использовать ConfigMaps как переменные среды, аргументы командной строки 
или как файлы конфигурации в томе.

ConfigMap позволяет отделить конфигурацию, специфичную для среды, от образов контейнеров, чтобы ваши приложения можно было легко переносить.

**Внимание:** ConfigMap не обеспечивает секретность или шифрование. Если данные, которые вы хотите сохранить, являются конфиденциальными, используйте Secret, а не ConfigMap, или используйте 
дополнительные (сторонние) инструменты для обеспечения конфиденциальности ваших данных.

Используйте ConfigMap для установки данных конфигурации отдельно от кода приложения. Например, представьте, что вы разрабатываете приложение, которое можно запускать на своем компьютере 
(для разработки) и в облаке (для обработки реального трафика). Вы пишете код для поиска в переменной среды с именем DATABASE_HOST. Локально вы устанавливаете для этой переменной значение 
localhost. В облаке вы устанавливаете ссылку на службу Kubernetes, которая предоставляет компонент базы данных вашему кластеру. Это позволяет получить образ контейнера, работающий в облаке, 
и при необходимости отладить тот же код локально.

**Примечание.** ConfigMap не предназначен для хранения больших объемов данных. Данные, хранящиеся в ConfigMap, не могут превышать 1 МБ. Если вам необходимо сохранить настройки, превышающие этот 
предел, вы можете рассмотреть возможность подключения тома или использования отдельной базы данных или файловой службы.

Объект ConfigMap

ConfigMap — это объект API, который позволяет хранить конфигурацию для использования другими объектами. В отличие от большинства объектов Kubernetes, имеющих спецификацию, ConfigMap имеет поля 
данных и двоичных данных. Эти поля принимают в качестве значений пары ключ-значение. И поле данных, и двоичные данные являются необязательными. Поле данных предназначено для хранения строк 
UTF-8, а поле двоичных данных предназначено для хранения двоичных данных в виде строк в кодировке Base64.

Имя ConfigMap должно быть допустимым именем субдомена DNS.

Каждый ключ в поле данных или в поле двоичных данных должен состоять из буквенно-цифровых символов -, _ или .. Ключи, хранящиеся в данных, не должны перекрываться с ключами в поле двоичных 
данных. Начиная с версии 1.19, вы можете добавить неизменяемое поле в определение ConfigMap, чтобы создать неизменяемый ConfigMap.

## ConfigMaps и модули

Вы можете написать спецификацию модуля, которая ссылается на ConfigMap и настраивает контейнеры в этом модуле на основе данных в ConfigMap. Pod и ConfigMap должны находиться в одном 
пространстве имен.

**Примечание.** Спецификация статического модуля не может ссылаться на ConfigMap или любые другие объекты API.

Вот пример ConfigMap, в котором есть ключи с одиночными значениями и другие ключи, значения которых выглядят как фрагмент формата конфигурации.

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: game-demo
data:
  # property-like keys; each key maps to a simple value
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.properties"

  # file-like keys
  game.properties: |
    enemy.types=aliens,monsters
    player.maximum-lives=5    
  user-interface.properties: |
    color.good=purple
    color.bad=yellow
    allow.textmode=true    
```

There are four different ways that you can use a ConfigMap to configure a container inside a Pod:

    Inside a container command and args
    Environment variables for a container
    Add a file in read-only volume, for the application to read
    Write code to run inside the Pod that uses the Kubernetes API to read a ConfigMap

These different methods lend themselves to different ways of modeling the data being consumed. For the first three methods, the kubelet uses the data from the ConfigMap when it launches container(s) for a Pod.

The fourth method means you have to write code to read the ConfigMap and its data. However, because you're using the Kubernetes API directly, your application can subscribe to get updates whenever the ConfigMap changes, and react when that happens. By accessing the Kubernetes API directly, this technique also lets you access a ConfigMap in a different namespace.

Here's an example Pod that uses values from game-demo to configure a Pod (configmap/configure-pod.yaml):

```
apiVersion: v1
kind: Pod
metadata:
  name: configmap-demo-pod
spec:
  containers:
    - name: demo
      image: alpine
      command: ["sleep", "3600"]
      env:
        # Define the environment variable
        - name: PLAYER_INITIAL_LIVES # Notice that the case is different here
                                     # from the key name in the ConfigMap.
          valueFrom:
            configMapKeyRef:
              name: game-demo           # The ConfigMap this value comes from.
              key: player_initial_lives # The key to fetch.
        - name: UI_PROPERTIES_FILE_NAME
          valueFrom:
            configMapKeyRef:
              name: game-demo
              key: ui_properties_file_name
      volumeMounts:
      - name: config
        mountPath: "/config"
        readOnly: true
  volumes:
  # You set volumes at the Pod level, then mount them into containers inside that Pod
  - name: config
    configMap:
      # Provide the name of the ConfigMap you want to mount.
      name: game-demo
      # An array of keys from the ConfigMap to create as files
      items:
      - key: "game.properties"
        path: "game.properties"
      - key: "user-interface.properties"
        path: "user-interface.properties"
```

ConfigMap не различает однострочные значения свойств и многострочные значения, подобные файлам. Важно то, как поды и другие объекты используют эти значения.

В этом примере определение тома и его монтирование внутри демонстрационного контейнера как /config создает два файла: /config/game.properties и /config/user-interface.properties, хотя в ConfigMap имеется четыре ключа. Это связано с тем, что определение Pod указывает массив элементов в разделе томов. Если вы полностью опустите массив элементов, каждый ключ в ConfigMap станет файлом с тем же именем, что и ключ, и вы получите 4 файла.
Использование ConfigMaps

ConfigMaps можно монтировать как тома данных. ConfigMaps также могут использоваться другими частями системы без прямого доступа к поду. Например, ConfigMaps может хранить данные, которые другие части системы должны использовать для настройки.

Самый распространенный способ использования ConfigMaps — настройка параметров для контейнеров, работающих в модуле Pod в том же пространстве имен. Вы также можете использовать ConfigMap отдельно.

Например, вы можете встретить надстройки или операторы, которые корректируют свое поведение на основе ConfigMap.
Использование ConfigMaps как файлов из модуля

Чтобы использовать ConfigMap в томе модуля:

    Создайте ConfigMap или используйте существующий. Несколько модулей Pod могут ссылаться на один и тот же ConfigMap.
    Измените определение Pod, добавив том в .spec.volumes[]. Назовите том как угодно и установите поле .spec.volumes[].configMap.name для ссылки на ваш объект ConfigMap.
    Добавьте .spec.containers[].volumeMounts[] к каждому контейнеру, которому требуется ConfigMap. Укажите .spec.containers[].volumeMounts[].readOnly = true и .spec.containers[].volumeMounts[].mountPath для неиспользуемого имени каталога, в котором вы хотите, чтобы ConfigMap отображался.
    Измените изображение или командную строку, чтобы программа искала файлы в этом каталоге. Каждый ключ в карте данных ConfigMap становится именем файла в mountPath.

Это пример модуля, который монтирует ConfigMap в том:

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
      readOnly: true
  volumes:
  - name: foo
    configMap:
      name: myconfigmap
```

аждый ConfigMap, который вы хотите использовать, должен быть указан в .spec.volumes.

Если в модуле имеется несколько контейнеров, то каждому контейнеру нужен собственный блок VolumeMounts, но для каждого ConfigMap требуется только один .spec.volumes.
Подключенные ConfigMaps обновляются автоматически.

Когда ConfigMap, используемый в настоящее время в томе, обновляется, проецируемые ключи в конечном итоге также обновляются. Кубелет проверяет, является ли смонтированный ConfigMap свежим при каждой периодической синхронизации. Однако кубелет использует свой локальный кеш для получения текущего значения ConfigMap. Тип кэша можно настроить с помощью поля configMapAndSecretChangeDetectionStrategy в структуре KubeletConfiguration. ConfigMap может распространяться либо путем наблюдения (по умолчанию), либо на основе ttl, либо путем перенаправления всех запросов непосредственно на сервер API. В результате общая задержка с момента обновления ConfigMap до момента проецирования новых ключей в под может составлять столько же, сколько период синхронизации kubelet + задержка распространения кеша, где задержка распространения кеша зависит от выбранного кеша. тип (оно равно наблюдаемой задержке распространения, ttl кэша или нулю соответственно).

ConfigMaps, используемые как переменные среды, не обновляются автоматически и требуют перезапуска модуля.
Примечание. Контейнер, использующий ConfigMap в качестве монтирования тома subPath, не будет получать обновления ConfigMap.
Неизменяемые карты конфигурации
СОСТОЯНИЕ ФУНКЦИИ: Kubernetes v1.21 [стабильная]

Функция Kubernetes Immutable Secrets и ConfigMaps предоставляет возможность сделать отдельные секреты и ConfigMaps неизменяемыми. Для кластеров, которые широко используют ConfigMap (по крайней мере, десятки тысяч уникальных подключений ConfigMap к Pod), предотвращение изменений в их данных имеет следующие преимущества:

    защищает вас от случайных (или нежелательных) обновлений, которые могут привести к сбоям в работе приложений
    повышает производительность вашего кластера за счет значительного снижения нагрузки на kube-apiserver, закрывая наблюдения за ConfigMaps, помеченными как неизменяемые.

Вы можете создать неизменяемый ConfigMap, установив для неизменяемого поля значение true. Например:

```
apiVersion: v1
kind: ConfigMap
metadata:
  ...
data:
  ...
immutable: true
```

Если ConfigMap помечен как неизменяемый, невозможно отменить это изменение или изменить содержимое данных или поля двоичных данных. Вы можете только удалить и воссоздать ConfigMap. Поскольку 
существующие модули поддерживают точку подключения к удаленному ConfigMap, рекомендуется воссоздать эти модули.





