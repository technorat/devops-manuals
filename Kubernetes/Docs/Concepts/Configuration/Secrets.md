# Секреты (Secrets)

Для работы приложений требуются пароли, токены или ключи. Сохранять их в образ контейнера или в манифесты небезопасно. Для этого используют Секрет — специальный объект Kubernetes, который содержит небольшое количество конфиденциальных данных.
Секреты аналогичны ConfigMaps, но предназначены специально для хранения конфиденциальных данных, но разница в том, что он используется для хранения учетных данных secret data и хранит эти данные не в обычном текстовом формате, а в формате с кодировкой base64.
Всего существует восемь типов secrets:

| Тип | Как используется |
|---|---|
| Opaque | Arbitrary user-defined data |
| kubernetes.io/service-account-token | ServiceAccount token |
| kubernetes.io/dockercfg | serialized ~/.dockercfg file |
| kubernetes.io/dockerconfigjson | serialized ~/.docker/config.json file |
| kubernetes.io/basic-auth | credentials for basic authentication |
| kubernetes.io/ssh-auth | credentials for SSH authentication |
| kubernetes.io/tls | data for a TLS client or server |
| bootstrap.kubernetes.io/token | bootstrap token data |

### Opaque 

Позволяет хранить внутри себя любые данные, универсальный тип.

### Service account token

Связан с service account и хранит для него JWT-токен. Практически всегда при создании service account автоматически создаётся его секрет. Внутри такого секрета всегда есть указание на то, для какого service account был выдан JWT-токен.

### Dockercfg и dockerconfigjson

Позволяет хранить внутри себя настройки для доступа к docker container registry. Dockercfg — просто config-формат, configjson —  json-формат. 

### Basic-auth

Логин:пароль в Base64 формате для Basic-аутентификации.

### Ssh-auth

Может использоваться для аутентификации при установке соединений с использованием протокола SSH. Внутри такого секрета находится приватный ключ от ключевой пары. В поле Data манифеста мы должны указать ssh-privatekey.

### TLS

Хранит сертификат и приватный ключ от него. Можно использовать его в качестве серверного сертификата. Например, cert-manager оперирует именно таким секретом —  выпускает TLS-сертификат с приватным ключом и кладет в secret с типом TLS. Особенность секрета в том, что в поле data в манифесте мы должны указать ключи tls.crt и tls.key, где в Base64-кодировке поместить сертификат и его приватный ключ.

### Bootstrap token

Позволяет обеспечить безопасное добавление новых k8s-нод в кластер.