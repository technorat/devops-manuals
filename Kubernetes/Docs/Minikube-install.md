# Установка Minikube

**Мануал подглядывать можно [здесь](https://kubernetes.io/ru/docs/tasks/tools/install-minikube/) **

**Описание возможных сред исполнения [здесь](https://kubernetes.io/ru/docs/setup/learning-environment/minikube/#%D1%83%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B4%D1%80%D0%B0%D0%B9%D0%B2%D0%B5%D1%80%D0%B0-%D0%B2%D0%B8%D1%80%D1%82%D1%83%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D0%B9-%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D1%8B) **

1. Отключаем в /etc/fstab swap-раздел. Перезагружаемся

2. Устанавливаем kubectl

2.1. Добавляем ключ репозитория

```shell
# apt install -y apt-transport-https
# apt update
# curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor | tee /usr/share/keyrings/kubernetes.gpg > /dev/null
```

2.2. Добавляем репозиторий в /etc/apt/sources.list.d

```shell
# vi /etc/apt/sources.list.d/kubernetes.list
# cat /etc/apt/sources.list.d/kubernetes.list
deb [arch=amd64 signed-by=/usr/share/keyrings/kubernetes.gpg] https://apt.kubernetes.io/ kubernetes-xenial main
```

2.3. Устанавливаем

```shell
# apt update
# apt install kubectl
```

3. Устанавливаем Virtualbox

```shell
# apt install virtualbox virtualbox-ext-pack
```


4. Устанавливаем и запускаем MiniKube

```shell
# curl -s https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 > /usr/local/bin/minikube
# chmod 755 /usr/local/bin/minikube
```

5. Запуск  MiniKube

**Если хотите просто запустить с дефолтными параметрами, то запускаем так:**

```shell
# minikube start --driver=virtualbox --force
```

**Если хотите просто запустить с кастомными параметрами (ram, cpu, cnii), то добавляем параметры в командную строку:**

Дадим 2 проца, ОЗУ - 4 Гб , CNI - Flannel

```shell
# minikube start --driver=virtualbox --cpus=2 --cni=flannel --install-addons=true --memory=4g --force
```

Если не хватает ресурсов Minikub-у, то надо его остановить, удалить и запустить с увеличенными параметрами

```shell
# minikube stop
# minikube delete
# minikube start --memory 8192 --cpus 2
```

Чтобы не указывать каждый раз параметры, можно изменить значения по умолчанию

```shell
# minikube stop
# minikube config set memory 8192
# minikube config set cpus 8
# minikube start
```

5. **Можно** (но не обязательно) добавить Dashboard (грузится долго)

```shell
# minikube addons enable metrics-server
# minikube dashboard
```

Узнать URL для подключения к панели управлени можно так: 

```shell
# minikube dashboard --url
```


6. Проверяем конфиг kubectl 

```shell
# # kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /root/.minikube/ca.crt
    extensions:
    - extension:
        last-update: Sun, 11 Feb 2024 14:41:16 UTC
        provider: minikube.sigs.k8s.io
        version: v1.32.0
      name: cluster_info
    server: https://192.168.59.100:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    extensions:
    - extension:
        last-update: Sun, 11 Feb 2024 14:41:16 UTC
        provider: minikube.sigs.k8s.io
        version: v1.32.0
      name: context_info
    namespace: default
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /root/.minikube/profiles/minikube/client.crt
    client-key: /root/.minikube/profiles/minikube/client.key
```

6. Проверяем работу kubectl 

```shell
# kubectl get nodes
NAME       STATUS   ROLES           AGE   VERSION
minikube   Ready    control-plane   22m   v1.28.3
```

