# Kubectl CheatSheet (Описание команд Kubectl)

## Config (Конфиг)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl config view | Просмотреть конфиг | (секретные данные не отображает) |
| kubectl config view --minify | Просмотреть конфиг в текущем контексте | (секретные данные не отображает) |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |

## Contexts (Контексты)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl config get-contexts | Просмотреть доступные контексты |  |
| kubectl config current-context | Просмотреть текущий контекст |  |
| kubectl config use-context clusterB | Переключить текущий контекст | **Изменяет поле "current-context" в файле ~/.kube/config** |
| kubectl config set context clusterB  | Переключить текущий контекст | (**уточнить**) |
| kubectl config delete-context <context-name> | Удалить контекст |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |

## Cluster (Кластер)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl cluster-info | Инфа, к какому мастеру сейчас подключены |  |
| kubectl cluster-info dump | Много, очень много информации о кластере |  |
|  |  |  |
|  |  |  |

## Deployments (развертывания)

| Команда | Описание | Примечание |
|---------|------------|---------|
| kubectl get deployments --all-namespaces | Показать работающие deployments во всех namespace-ах | |
| kubectl create deployment my-deployment-name --image adv4000/k8sphp:latest | Создать deployment my-deployment-name с образом adv4000/k8sphp:latest | |
| kubectl run nginx --image nginx | Запустите экземпляр контейнера nginx, посредством создания объекта Deployment |  |
| kubectl create deployment nginx --image nginx | Запустите экземпляр контейнера nginx, посредством создания объекта Deployment |  |
| kubectl delete deployment <deployment> | Удалить деплоймент |  |
|  |  |  |
|  |  |  |
|  |  |  |

## Jobs (Задания)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl get jobs --all-namespaces | Получить краткое описание статуса задачи во всех namespace-ах | |
| kubectl get job hello -n <namespace> | Получить краткое описание статуса задачи в указанном namespace | |
| kubectl describe job hello  -n <namespace> | Получить подробное описание задачи в указанном namespace |  |
| kubectl get job hello -o yaml  -n <namespace> | Получить подробное описание задачи в указанном namespace в формате yaml | Это не чистый манифест, по которому надо запускать задачу |
|  |  |  |
|  |  |  |

## Namespaces (Пространства имен)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl get namespace | Просмотреть доступные пространства имён в кластере |  |
| kubectl <любая команда> --namespace=<название namespace-а> | Используйте флаг --namespace, чтобы определить пространство имён только для текущего запроса. |  |
| kubectl config set-context --current --namespace=<название namespace-а> | Определить пространство имён, которое должно использоваться для всех выполняемых команд kubectl в текущем контексте. |  |
|  |  |  |
|  |  |  |
|  |  |  |

## Networks (Сеть, маршрутизация)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl port-forward vault-0 30080:8200 | Пробросить порт 8200 пода vault-0 на комп, с которого подключаешься с портом 30080 | Открывает VPN до куба. Стучимся в под через браузер http://localhost:30080 |
|  |  |  |

## Nodes (Ноды)

| Команда | Описание | Примечание |
|---------|------------|---------|
| kubectl get nodes | Показать ноды кластера | |
| kubectl label node <имя_ноды> node-role.kubernetes.io/worker=worker | Сделать ноду worker-ом, если у нее такой роли нет |  |
|  |  |  |
|  |  |  |
|  |  |  |

## Objects (Объекты и ресурсы)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl api-resources -o wide | Просмотреть, какими ресурсами можно управлять |  |
| kubectl create -f nginx.yaml | Создать объекты, определенные в конфигурационном файле |  |
| kubectl delete -f nginx.yaml -f redis.yaml | Удалить объекты, определенные в двух конфигурационных файлах |  |
| kubectl replace -f nginx.yaml | Обновить объекты, определенные в конфигурационном файле, перезаписав текущую конфигурацию |  |
| kubectl get all -n <namespace> | Просмотреть все созданные ресурсы в пространстве имен <namespace> |  |
| kubectl apply -f <file_name1> -f <file_name2> | Создать ресурсы из нескольких файлов |  |
| kubectl apply -f <path_name>/<dir_name> | Создать ресурсы из всех файлов манифеста в директории |  |
|  |  |  |

## Pods (Поды)

| Команда | Описание | Примечание |
|---------|------------|---------|
| kubectl get pods --all-namespaces | Показать работающие поды во всех namespace-ах | |
| kubectl get pods --all-namespaces -o wide | Показать работающие поды во всех namespace-ах с большим количеством инфы | |
| kubectl exec --stdin --tty ubuntu -- /bin/bash | Подключиться к работающему Pod-у |  |
| kubectl describe pod <имя_pod-а> -n <namespace> | Получить подробное описание пода в указанном namespace |  |
| kubectl get pod argocd-application-controller-0 -n argocd -o yaml | Получить подробное описание пода в указанном namespace в формате yaml | Это не чистый манифест, по которому надо запускать под |
|  |  |  |
|  |  |  |

## Service Accounts (Сервисные учетные записи)

| Команда | Описание | Примечание |
|---|---|---|
| kubectl get serviceAccounts | Просмотреть список Service Account-ов в текущем Namespace |  |
| kubectl get sa --all-namespaces | Просмотреть список Service Account-ов во всех Namespace-ах |  |
|  |  |  |
|  |  |  |
