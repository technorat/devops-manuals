# Установка Kubernetes с помощью Kubespray

## Требования к софту и железу

* Мастер ноды - от 1,5 Гб ОЗУ (у меня 4 Гб и 1 ЦПУ)
* Worker ноды - от 1 Гб ОЗУ (у меня 4 Гб и 3 ЦПУ)
* Linux (смотрим на сайте), из разрешенных выбрали Ubuntu 22.04 LTS
* Kubernetes is v1.27+
* Ansible v2.14+ (на машине, откуда запускаем установку)
* Jinja 2.11+ 
* python-netaddr
* Целевые серверы должны иметь доступ к Интернету, чтобы получать образы Docker.
* Целевые серверы настроены на разрешение пересылки IPv4.
* При использовании IPv6 для модулей и служб целевые серверы настроены на разрешение пересылки IPv6.
* Firewall-ы отключены
* Если kubespray запускается из-под учетной записи пользователя без полномочий root, на целевых серверах должен быть настроен правильный метод повышения привилегий. Затем следует указать флаг 
ansible_become или параметры команды --become или -b.

## Поддерживаемые компоненты

**Core**
* kubernetes v1.29.1
* etcd v3.5.10
* docker v24.0 (see Note)
* containerd v1.7.13
* cri-o v1.29.1 (experimental: see CRI-O Note. Only on fedora, ubuntu and centos based OS)
**Network Plugin**
* cni-plugins v1.2.0
* calico v3.26.4
* cilium v1.13.4
* flannel v0.22.0
* kube-ovn v1.11.5
* kube-router v2.0.0
* multus v3.8
* weave v2.8.1
* kube-vip v0.5.12
**Application**
* cert-manager v1.13.2
* coredns v1.11.1
* ingress-nginx v1.9.6
* krew v0.4.4
* argocd v2.8.4
* helm v3.13.1
* metallb v0.13.9
* registry v2.8.1
**Storage Plugin**
* cephfs-provisioner v2.1.0-k8s1.11
* rbd-provisioner v2.1.1-k8s1.11
* aws-ebs-csi-plugin v0.5.0
* azure-csi-plugin v1.10.0
* cinder-csi-plugin v1.22.0
* gcp-pd-csi-plugin v1.9.2
* local-path-provisioner v0.0.24
* local-volume-provisioner v2.5.0

## Адресный план

* Сеть - 192.68.255.0/24
* Шлюз - 192.68.255.1
* DNS - 192.68.255.1
* Master - u22-k8s-m1 - 192.68.255.101
* Master - u22-k8s-m2 - 192.68.255.102
* Master - u22-k8s-m3 - 192.68.255.103
* Worker - u22-k8s-w1 - 192.68.255.111
* Worker - u22-k8s-w2 - 192.68.255.112
* Worker - u22-k8s-w3 - 192.68.255.113

## Подготовка серверов

1. Установка ОС на 1-м мастере (остальные ВМ склонируем с неё после настройки

2. Задаем пароль учетки (dark) и (root). От рута будем подключаться для установки на тестовых стендах. **На работе так не делаем!!!**

3. Обновляем кэш установочных пакетов, устанавливаем:
* mc
* curl
* wget

4. Отрубаем своп

5. Перезагружаем

6. Настраиваем SSH с доступом рута. **На работе так не делаем!!!**. Рестартуем службу. Пробуем подрубиться по SSH

7. Пробрасываем ключ SSH

8. Настраиваем хосты

```shell
# cat /etc/hosts 
127.0.0.1 localhost
127.0.1.1 u22-k8s-m1
192.68.255.101 u22-k8s-m1
192.68.255.102 u22-k8s-m2 
192.68.255.103 u22-k8s-m3 
192.68.255.111 u22-k8s-w1 
192.68.255.112 u22-k8s-w2 
192.68.255.113 u22-k8s-w3
```

9. На машине, откуда будем запускать Kubespray, устанавливаем Ansible. У нас в репах лежит версия 2.14.9. В зависимостях он подтягивает и нужный нам пакет python-netaddr

```shell
# apt install ansible-core
........ Поскипано ........
Следующие НОВЫЕ пакеты будут установлены:
  ansible ansible-core ieee-data python3-argcomplete python3-dnspython python3-jmespath python3-kerberos python3-libcloud python3-lockfile python3-netaddr python3-ntlm-auth python3-packaging
  python3-passlib python3-requests-kerberos python3-requests-ntlm python3-requests-toolbelt python3-resolvelib python3-selinux python3-simplejson python3-winrm python3-xmltodict

```
10. На машине, откуда будем запускать Kubespray, пробуем поставить python3-jinja2

```shell
# apt install python3-jinja2
```

11. Добавляем на целевом сервере в файл /etc/sysctl.conf или раскомментируем строку

```shell
net.ipv4.ip_forward=1
```

12. Проверяем статус firewall-а. Если включен - отключаем

```shell
# ufw status
```

13. Рестартуем сервер