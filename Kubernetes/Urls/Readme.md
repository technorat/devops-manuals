# Полезные ссылки
## Kubernetes
[Официальный (кастрированный) мануал на русском] (https://kubernetes.io/ru/docs/concepts/architecture/control-plane-node-communication/)
[Официальный мануал (более полный) на английском](https://kubernetes.io/docs/concepts/architecture/controller/)
[Команды kubectl] (https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)
[Шпаргалка и доки на русском] (https://kubernetes.io/ru/docs/reference/kubectl/cheatsheet/)
[Как ставить куба вручную, инструкция] (https://github.com/kelseyhightower/kubernetes-the-hard-way?ysclid=l6noyp483w216189912)
https://github.com/kubernetes-sigs/metrics-server/blob/master/charts/metrics-server/README.md
[Лабы/стенд по кубу] (https://labs.play-with-k8s.com/)
[Доки по Kubectl](https://kubectl.docs.kubernetes.io/)

### Инсталляторы
[KOPS - разворачивание и управление кубом на амазоне] (https://github.com/kubernetes/kops?ysclid=l6np3orme3425708287)
[Kubespray - ансибловые роли для разворачивания кластера куба] (https://github.com/kubespray/kubespray) 
https://halfoff.ru/ru/devops/kubespray

## Etcd
[Официальная дока по EtcD] (https://etcd.io/docs/)




