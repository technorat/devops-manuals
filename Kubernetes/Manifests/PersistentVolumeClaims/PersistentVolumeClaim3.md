apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc3
spec:
  accessModes:
  - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
  storageClassName: ""