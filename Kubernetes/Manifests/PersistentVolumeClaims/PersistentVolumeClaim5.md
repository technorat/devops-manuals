apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc5
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: 