apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc7
spec:
  accessModes:
  - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
  storageClassName: ""