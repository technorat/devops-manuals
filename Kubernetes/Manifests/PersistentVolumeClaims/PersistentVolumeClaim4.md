apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc4
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: mystorage