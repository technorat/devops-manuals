# Deployment для сервера nginx.
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dark-test-ubuntu
spec:
  selector:
    matchLabels:
      app: ubuntu
  replicas: 1
  template:
    metadata:
      labels:
        app: ubuntu
    spec:
      containers:
      - name: ubuntu
        image: ubuntu:22.04
        command: ["/bin/sleep", "3650d"]
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 22
      restartPolicy: Always
