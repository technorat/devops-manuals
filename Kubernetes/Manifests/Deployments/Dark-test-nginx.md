# Deployment для сервера nginx.
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dark-test-nginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
#        image: nginx:1.14.2
        image: nginx:1.25.4-alpine-slim
        ports:
        - containerPort: 80