# каждое используемое значение должно быть закодировано в base64:
#
# echo -n 'admin' | base64
# YWRtaW4=
#
# echo -n 'B7wItYlHeRR1' | base64
# Qjd3SXRZbEhlUlIx


apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  username: YWRtaW4=
  password: Qjd3SXRZbEhlUlIx