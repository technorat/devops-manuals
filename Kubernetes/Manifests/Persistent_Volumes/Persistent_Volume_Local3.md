apiVersion: v1
kind: PersistentVolume
metadata:
  name: mystorage-20gi
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: mystorage
  local:
    path: /mnt/local-storage
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - u22-k8s-w1
          - u22-k8s-w2
          - u22-k8s-w3
