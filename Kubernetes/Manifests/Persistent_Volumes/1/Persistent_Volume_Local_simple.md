apiVersion: v1
kind: PersistentVolume
metadata:
  name: mystorage-w3
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: mystorage
  local:
    path: /mnt/local-storage
