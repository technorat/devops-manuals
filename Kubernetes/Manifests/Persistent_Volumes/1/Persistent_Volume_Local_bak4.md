apiVersion: v1
kind: PersistentVolume
metadata:
  name: mystorage
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
#  persistentVolumeReclaimPolicy: Delete
  persistentVolumeReclaimPolicy: Retain
  storageClassName: mystorage
  local:
    path: /mnt/local-storage
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - u22-k8s-w1
