# Минимальный манифест Ingress-а

```
apiVersion: networking.k8s.io/v1			# Пока что такая версия указывается у всех
kind: Ingress						# Тип ресурса - Ingress
metadata:						# Метаданные - набор ключевой служебной информации об объекте Kubernetes
  name: minimal-ingress					# Имя сервиса, как он будет называться в кластере
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
```