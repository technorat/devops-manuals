# Манифест сервиса, тип: LoadBalancer

```
apiVersion: v1							# Пока что такая версия указывается у всех
kind: Service							# Тип ресурса - сервис
metadata:							# Метаданные - набор ключевой служебной информации об объекте Kubernetes
  name: loadbalancer-name					# Имя сервиса, как он будет называться в кластере
  labels:
    app: nginx
  annotations:
    loadbalancer.openstack.org/keep-floatingip: "true"
spec:
  type: LoadBalancer
  selector:
    app: nginx
  ports:
  - port: 80
    protocol: TCP
```