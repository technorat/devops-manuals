# Манифест сервиса, тип: NodePort
NodePort указывает, какой порт открыть на узлах. Если мы не укажем этот порт, он выберет случайный. В большинстве случаев дайте Kubernetes самому выбрать 
порт.

```
apiVersion: v1				# Пока что такая версия указывается у всех
kind: Service				# Тип ресурса - сервис
metadata:				# Метаданные - набор ключевой служебной информации об объекте Kubernetes
  name: <service_name>			# Имя сервиса, как он будет называться в кластере
  namespace: <namespace_name>
spec:
  type: NodePort
  selector:
    app: <application_name>
  ports:
  - name: http
    port: 80
    targetPort: 80
    nodePort: 30036
    protocol: TCP
```