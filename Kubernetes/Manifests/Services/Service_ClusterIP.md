# Манифест сервиса, тип: ClusterIP

```
apiVersion: v1			# Пока что такая версия указывается у всех
kind: Service			# Тип ресурса - сервис
metadata:			# Метаданные - набор ключевой служебной информации об объекте Kubernetes
 name: my-internal-service	# Имя сервиса, как он будет называться в кластере
selector:
 app: my-app
spec:
 type: ClusterIP
 ports: 
 - name: http
   port: 80
   targetPort: 80
   protocol: TCP
```