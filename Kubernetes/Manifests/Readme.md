# Манифесты Kubernetes

## Оглавление

* [Для создания Deplyment](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Readme.md)
* Команды kubelet
* [Полезные ссылки](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Urls/Readme.md)
* [Базовые ресурсы K8s, получаемые через "kubectl get имя_ресурса"](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Notes/Basic_resources.md)