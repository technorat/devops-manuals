apiVersion: batch/v1						                            # используемая для создания объекта версия API Kubernetes
kind: Job						                                        # тип создаваемого объекта
metadata:						                                        # данные, позволяющие идентифицировать объект (name, UID и необязательное поле namespace)
  name: hello
spec:						                                            # требуемое состояние объекта
  backoffLimit: 2						                                # Job будет пытаться выполнить задачу не более двух раз
  activeDeadlineSeconds: 60						                      # Job будет пытаться выполнить Pod-ы в рамках задачи не дольше 60 секунд
  template:						                                      # описание пода, который будет выполнять задачу
    spec:
      containers:
      - name: hello
        image: busybox
        args: 
        - /bin/sh
        - -c
        - date; echo Hello from the Kubernetes cluster
      restartPolicy: Never