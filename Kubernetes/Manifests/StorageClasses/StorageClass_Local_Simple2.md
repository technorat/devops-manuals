apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: mystorage2
reclaimPolicy: Retain
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer