apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: mystorage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
reclaimPolicy: Delete
provisioner: kubernetes.io/no-provisioner
volumeBindingMode: WaitForFirstConsumer