apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: mystorage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"		# Параметр storageclass.kubernetes.io/is-default-class в манифесте указывает, является ли указанный класс хранения классом по умолчанию для данного кластера Kubernetes. Если значение равно true, то этот класс хранения будет использоваться по умолчанию при создании новых PersistentVolume (постоянных томов) без указания класса хранения.
provisioner: kubernetes.io/no-provisioner			# Возможный вариант: csi-driver.example-vendor.example
reclaimPolicy: Retain						# контроллер не удаляет PersistentVolume, даже если он больше не используется. Это значение полезно для сохранения данных, которые могут потребоваться в будущем.
volumeBindingMode: WaitForFirstConsumer
#allowVolumeExpansion: true
#mountOptions:
#  - discard # this might enable UNMAP / TRIM at the block storage layer
#parameters:
#  guaranteedReadWriteLatency: "true" # provider-specific