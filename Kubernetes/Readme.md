# Дока по Kubernetes

## Разное

* __[Программа изучения тем, типа "дорожной карты"](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Learning_roadmap.md)__
* [Kubectl CheatSheet (Описание команд Kubectl)](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Kubectl_cheatsheet/Readme.md)
* [Полезные ссылки](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Urls/Readme.md)
* [Базовые ресурсы K8s, получаемые через "kubectl get имя_ресурса"](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Notes/Basic_resources.md)
* [Конфиг-файлы для доступа к Kubernetes](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Configs/Readme.md)
* [How-to, секреты, хитрости](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Howto.md)

# Дока по Kubernetes
* [Основные концепции](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Concepts/Readme.md)
    * **Конфигурирование**
        * [ConfigMap-ы](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Configuration/ConfigMaps.md)
    * **[Сетевые ресурсы](https://gitlab.com/technorat/devops-manuals/-/tree/main/Kubernetes/Docs/Concepts/Network_resources/)**
        * [Сервисы](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Network_resources/Services.md)
    * **Workloads (Рабочие нагрузки)**
        * **Management (Management)**
            * [Работы, задания (Jobs)](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Workloads/Management/Jobs.md)
            * [StatefulSet](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Workloads/Management/StatefulSets.md)
    * **[Хранилища](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Readme.md)**
        * [AzureFile_CSI_migration](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/AzureFile_CSI_migration.md)
        * [CephFS](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/CephFS.md)
        * [ConfigMap](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/ConfigMap.md)
        * [CSI_Volume_Cloning](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/CSI_Volume_Cloning.md)
        * [DownwardAPI](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/DownwardAPI.md)
        * [Dynamic_Volume_Provisioning](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Dynamic_Volume_Provisioning.md)
        * [EmptyDir_Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/EmptyDir_Volumes.md)
        * [Ephemeral_Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Ephemeral_Volumes.md)
        * [FC](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/FC.md)
        * [HostPath_Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/HostPath_Volumes.md)
        * [iSCSI](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/iSCSI.md)
        * [Local](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Local.md)
        * [Node-specific_Volume_Limits](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Node-specific_Volume_Limits.md)
        * [NSF](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/NSF.md)
        * [PersistentVolumeClaims](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/PersistentVolumeClaims.md)
        * [Persistent_Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Persistent_Volumes.md)
        * [Projected_Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Projected_Volumes.md)
        * [RBD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/RBD.md)
        * [Secret](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Secret.md)
        * [Storage_Capacity](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Storage_Capacity.md)
        * [Storage_Classes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Storage_Classes.md)
        * [StorageClass](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/StorageClass.md)
        * [Volume_Attributes_Classes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Volume_Attributes_Classes.md)
        * [Volume_Health_Monitoring](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Volume_Health_Monitoring.md)
        * [Volumes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Volumes.md)
        * [Volume_Snapshot_Classes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Volume_Snapshot_Classes.md)
        * [Volume_Snapshots](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kubernetes/Docs/Concepts/Storage/Volume_Snapshots.md)


