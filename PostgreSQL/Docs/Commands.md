# Команды PostgreSQL

## CLI

Вся работа с PostgreSQL осуществляется под пользователем postgres.

```shell
$sudo su postgres
```

Перенос БД без промежуточных файлов, напрямую с сервера на сервер

```shell
# pg_dump -Fc -v --no-acl --dbname=postgresql://$DB_SOURCE_USERNAME:$DB_SOURCE_PASSWORD@$DB_SOURCE_HOST:$DB_SOURCE_PORT/$DB_SOURCE_NAME | pg_restore -Fc -v --no-acl --no-owner --single-transaction --role=$DB_DESTINATION_USERNAME --dbname=postgresql://$DB_DESTINATION_USERNAME:$DB_DESTINATION_PASSWORD@$DB_DESTINATION_HOST:$DB_SOURCE_PORT/$DB_DESTINATION_NAME 
```






