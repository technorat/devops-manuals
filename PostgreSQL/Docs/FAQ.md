# ЧАсто задаваемые ВОпросы (FAQ)

## Оглавление

- [2024-07-22 Npgsql.PostgresException (0x80004005): 28000: pg_hba.conf rejects connection for host "<IP-адрес>", user "<Логин>", database "<БД>", no encryption](https://gitlab.com/technorat/devops-manuals/-/blob/main/PostgreSQL/Docs/FAQ.md#nossl_reject)

## Вопросы

### Npgsql.PostgresException (0x80004005): 28000: pg_hba.conf rejects connection  <a name="nossl_reject"></a>

Полное сообщение выглядит так:

```
2024-07-22 Npgsql.PostgresException (0x80004005): 28000: pg_hba.conf rejects connection for host "<IP-адрес>", user "<логин>", database "<имя_БД>", no encryption
```

Причина: В настройках PostgreSQL стоит запрет на подключение без SSL (метод - reject). Пример конфига:

```
# cat pgdata/pgroot/data/pg_hba.conf

# <TYPE>    <DATABASE>      <USER>      <IP-ADDRESS>    <[IP-MASK]>     <METHOD>
local       all             all                                         trust
host        all             all         127.0.0.1/32                    trust
hostnossl   all             all         all                             reject
hostssl     all             +db_admin   192.168.5.0     255.255.255.0   pam
hostssl     all             +db_admin   172.16.38.0/24                  pam
```

__Решение:__

1. В строке подключения передаем нужные параметры

```
ConnectionString="Host=<Сервер>;Port=5432;Database=<БД>;Username=<Логин>;Password=<пароль>;SSL Mode=Require;Trust Server Certificate=true"
```

2. В манифесте Kubernetes:

```
env:
  - name: DB_HOST
  value: "<Сервер>"
  - name: DB_PORT
  value: "5432"
  - name: DEFAULT_DB_NAME
  value: "<БД>"
  - name: DEFAULT_DB_USERNAME
  value: "<Логин>"
  - name: DEFAULT_DB_PASSWORD
  value: "<пароль>"
  - name: PGSSLMODE
  value: "require"
  - name: NODE_TLS_REJECT_UNAUTHORIZED
  value: "0"
```

