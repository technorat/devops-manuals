# Команды Helm

## Работа с чартами

| Команда | Описание | Коммент |
|---|---|---|
| helm create basic | Создает папку со структурой папок и файлов чарта |  |
| helm template basic | Проверить, что именно будет развернуто в кластере | Команда выведет каждый YAML, созданный всеми шаблонами |
| helm template basic -x templates/service.yaml | Увидеть результат только одного шаблона |  |
| helm install basic | Установить чарт |  |
| helm install stable/mysql --generate-name | Установить чарт и сгенерировать имя автоматически|  |
| helm install happy-panda bitnami/wordpress | Установить чарт bitnami/wordpress и задать имя happy-panda |  |
| helm install -f values.yaml happy-panda bitnami/wordpress | Установить чарт bitnami/wordpress с переменными из values.yaml и задать имя happy-panda |  |
| helm install --dry-run --debug <name> <chart-folder> | Сгенерированный шаблон протестировать на кластере без установки |
| helm lint basic | Проверка чарта на синтаксические ошибки |  |
| helm show chart stable/mysql | Краткое описание чарта |  |
| helm show values bitnami/wordpress | Увидеть, какие параметры есть в chart-е |  |
| helm pull openproject/openproject |  |  |
| helm pull openproject/openproject --untar --destination ~/Openproject/2/ |  |  |
| helm fetch --prov openproject/openproject |  |  |
| helm gpg verify openproject-*.tgz |  |  |
| helm upgrade --create-namespace --namespace openproject --install my-openproject openproject/openproject |  |  |
| helm get manifest ha-vault | Показать манифест, который развернут в чарте |  |
|  |  |  |
|  |  |  |
|  |  |  |


## Работа с релизами

| Команда | Описание | Коммент |
|---|---|---|
| helm list | Список того, что установлено Helm-ом | Аналог - helm ls |
| helm ls | Список того, что установлено Helm-ом | Аналог - helm list |
| helm list --kube-context <имя контекста> | Список того, что установлено Helm-ом в контексте, отличном от заданного в ~/.kube/config | Аналог - helm ls |
| helm status <имя_релиза> --namespace <имя_неймспейса> | Просмотреть статус релиза | "--namespace <имя_неймспейса>" - необязательный параметр 
| helm get all <имя_релиза> --namespace <имя_неймспейса> | Много разной инфы по релизу |  |
| helm uninstall smiling-penguin | Деинсталлировать релиз smiling-penguin |  |
| helm status smiling-penguin | Просмотреть информацию о релизе |  |
| helm rollback | Восстановить релиз после удаления | Если включен флаг --keep-history |
| helm upgrade --create-namespace --namespace openproject --install my-openproject openproject/openproject |  |  |
|  |  |  |
|  |  |  |
|  |  |  |


## Работа с репозиториями чартов

| helm repo list | Показать список хранилищ чартов |  |
| helm repo add | Добавить репозиторий чартов |  |
| helm repo add openproject https://charts.openproject.org | Добавить репозиторий чартов |  |
| helm repo add stable https://charts.helm.sh/stable | Добавить репозиторий чартов |  |
| helm repo update | Обновлять информацию о доступных диаграммах локально из репозиториев чартов |  |
| helm repo index | Создать индексный файл, учитывая каталог, содержащий упакованные чарты |  |
| helm repo remove | Удалить один или несколько репозиториев |  |
| helm search hub | Ищет в Artifact Hub, который агрегирует списки из различных хранилищ |  |
| helm search hub wordpress | Ищем в Artifact Hub чарт wordpress |  |
| helm search repo | Поиск в репозиториях, которые вы добавили в свой локальный каталог |  |
| helm search repo stable | Посмотреть charts, которые вы можете установить |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |


## Темплейты

| Команда | Описание | Коммент |
|---|---|---|
| helm template sparkmagic-0.1.0.tgz  --output-dir out |  |  |
| helm template kafka-ui kafka-ui/kafka-ui --output-dir . |  |  |
| helm template kafka-ui kafka-ui/kafka-ui --output-dir . -f values.yaml |  |  |
| helm template kafka-ui kafka-ui/kafka-ui --output-dir . --set existingConfigMap="kafka-ui-config" |  |  |
| helm template openproject/openproject openproject.tgz |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |


