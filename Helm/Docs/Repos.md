# Репозитории Helm

* helm repo add brigade https://brigadecore.github.io/charts
* helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx# Список репозиториев Helm
| Репозиторий | Адрес | Коммент |
|---|---|---|
| Helm artifact hub |  |  |
| bitnami | helm repo add bitnami https://charts.bitnami.com/bitnami | Некоторые пакеты, которые не включены в официальный репозиторий stable |
| brigadecore | helm repo add brigade https://brigadecore.github.io/charts |  |
| kubernetes-charts | helm repo add stable https://kubernetes-charts.storage.googleapis.com/ | Заменен на Helm Stable |
| incubator | helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/ | Официальный репозиторий. Пакеты, которые не готовы для использования в stable. Инструкции по использованию репозитория |
| charts.gitlab.io | helm repo add gitlab https://charts.gitlab.io | В России заблочен |
| Helm Stable | helm repo add stable https://charts.helm.sh/stable |  |
| Openproject | https://charts.openproject.org |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |

* Существует специально отобранный репозиторий чартов, он называется stable. В основном он включает в себя обычные чарты, которые вы можете найти на GitHub. 
Helm не поддерживает этот репозиторий по умолчанию, поэтому нужно добавить его вручную.  Этот репозиторий указывает на объект Google Storage по адресу 
<https://kubernetes-charts.storage.googleapis.com>. Источник репозитория stable можно найти в <https://github.com/helm/charts/tree/master/stable>
* Официальный репозиторий incubator, содержащий пакеты, которые еще не готовы для использования в репозитории stable.
