helm delete vault
helm delete ha-vault
kubectl delete pvc data-vault-0
kubectl delete pvc data-vault-2
kubectl delete pvc data-vault-3
kubectl delete pv mystorage-w4
kubectl delete pv mystorage-w5
kubectl delete secret vault-injector-certs
kubectl delete service vault-ui
kubectl apply -f /gitlab/gitlab.com/technorat/devops-manuals/Kubernetes/Manifests/Persistent_Volumes/PV_Local_SC_RWO_w4.md
kubectl apply -f /gitlab/gitlab.com/technorat/devops-manuals/Kubernetes/Manifests/Persistent_Volumes/PV_Local_SC_RWO_w5.md
kubectl get pv,pvc
