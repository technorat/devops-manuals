helm delete consul
kubectl delete pvc data-default-consul-consul-server-0
kubectl delete pvc data-default-consul-consul-server-1
kubectl delete pvc data-default-consul-consul-server-2
kubectl delete pv mystorage-w1
kubectl delete pv mystorage-w2
kubectl delete pv mystorage-w3
kubectl apply -f /gitlab/gitlab.com/technorat/devops-manuals/Kubernetes/Manifests/Persistent_Volumes/PV_Local_SC_RWO_w1.md
kubectl apply -f /gitlab/gitlab.com/technorat/devops-manuals/Kubernetes/Manifests/Persistent_Volumes/PV_Local_SC_RWO_w2.md
kubectl apply -f /gitlab/gitlab.com/technorat/devops-manuals/Kubernetes/Manifests/Persistent_Volumes/PV_Local_SC_RWO_w3.md
kubectl get pv,pvc
