# Helm-чарты, из которых что то разворачивалось

| Продукт | Чарт | Родной URL | Коммент |
|---|---|---|---|
| Hashicorp Consul | https://gitlab.com/technorat/devops-manuals/-/tree/main/Helm/Charts/Hashicorp/consul-k8s/ | https://github.com/hashicorp/consul-k8s/ | HA-сластер на 3 ноды Consul как обнаружение сервисов и БД для Vault. Ставится первым |
| Hashicorp Vault | https://gitlab.com/technorat/devops-manuals/-/tree/main/Helm/Charts/Hashicorp/vault-helm/ | https://github.com/hashicorp/vault-helm | HA-сластер на 3 ноды, использует сonsul как БД. Ставится вторым |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
