Полезные команды Ceph.

ceph status (она же ceph -s) Проверьте состояние кластера

ceph -w наблюдать за активностью кластера в режиме реального времени

ceph health

ceph health detail

ceph auth list позволяет легко отслеживать ключи

ceph config show mon.ceph-mon-01 просмотреть актуальный конфиг сервера монитора ("mon" в mon.ceph-mon-01), конкретное имя указано после точки в "mon.ceph-mon-01". Сервера OSD просматриваются как то так: "osd.ceph-osd-server-01" или "osd.ceph-osd-169"

ceph osd stat

ceph osd df tree

ceph osd tree создает карту ASCII art CRUSH tree с хостом, его OSD, независимо от того, работоспособен ли он, и его объем.

ceph osd utilization

ceph osd pool autoscale-status

ceph osd pool ls detail

ceph osd repair попытаться восстановить OSD

ceph tell osd. bench* Используйте ceph tell, чтобы увидеть, насколько хорошо он работает, выполнив простой тест производительности.
По умолчанию тест записывает всего 1 ГБ с шагом 4 МБ.

ceph osd crush reweight перебалансировка кластера. Если ваши OSD отличаются по своим ключевым атрибутам, используйте ceph osd crush reweight, чтобы изменить их объем CRUSH map, чтобы кластер был правильно сбалансирован, а OSD разных типов получали соответственно скорректированное количество запросов и данных ввода-вывода.

ceph osd create для добавления нового OSD в кластер. Если UUID не указан, он будет установлен автоматически при запуске OSD.

ceph osd rm <UUID> удалить OSD из CRUSH map

ceph osd pool create создать пул 

ceph osd pool delete удалить пул

To send a scrub command to a specific OSD, or to all OSDs (by using *), run the following command:

ceph osd scrub {osd-num}

To send a repair command to a specific OSD, or to all OSDs (by using *), run the following command:

ceph osd repair N

ceph df проверить использование данных в кластере и распределение данных между пулами. Эта команда предоставляет информацию о доступном и используемом пространстве хранения, а также список пулов и объем памяти, который использует каждый пул.

ceph df detail

ceph config dump Выводит всю базу данных конфигурации монитора.

ceph config get mon.ceph-mon-01 osd_pool_default_size Запросить из конфига значение параметра osd_pool_default_size сервера монитора (mon.) "ceph-mon-01".

ceph config set mon.ceph-mon-01 osd_pool_default_size Переопределить значение из конфига параметра osd_pool_default_size на сервере монитора (mon.) "ceph-mon-01".

ceph config show mon.ceph-mon-01 Запросить значение актуальных параметров сервера монитора (mon.) "ceph-mon-01", как взятых из конфига, так и переопределенных.

ceph config assimilate-conf -i <input file> -o <output file> принимает файл конфигурации из входного файла и перемещает все допустимые параметры в базу данных конфигурации монитора.

ceph tell mon.* config set osd_pool_default_size 2 применить на ходу, до перезагрузки, значение репликации на все мониторы

ceph tell mon.* config set osd_pool_default_min_size 1 применить на ходу, до перезагрузки, минимальное значение репликации на все мониторы

ceph pg dump Проверьте статистику групп

ceph pg stat

Чтобы отобразить кворум мониторов, включая то, какие мониторы участвуют и какой из них является лидером, выполните следующие команды:

ceph mon stat

ceph quorum_status

ceph auth ls Просмотреть ключи авторизации

To display the statistics for all placement groups (PGs), run the following command:

ceph pg dump [--format {format}]

Here the valid formats are plain (default), json json-pretty, xml, and xml-pretty. When implementing monitoring tools and other tools, it is best to use the json format. JSON parsing is more deterministic than the plain format (which is more human readable), and the layout is much more consistent from release to release. The jq utility is very useful for extracting data from JSON output.

To display the statistics for all PGs stuck in a specified state, run the following command:

ceph pg dump_stuck inactive|unclean|stale|undersized|degraded [--format {format}] [-t|--threshold {seconds}]

Here --format may be plain (default), json, json-pretty, xml, or xml-pretty.

The --threshold argument determines the time interval (in seconds) for a PG to be considered stuck (default: 300).

Для админа Ceph полезна шпаргалка с командами, особо помогающие при различных внештатных ситуациях. В комбинации с командой watch получается удобное наблюдение в режиме реального времени.
Общее для кластера

    ceph status (она же ceph -s или ceph -w)

    Самая полезная команда, позволяющая сразу понять многое: чем занят кластер, какое у него здоровье (надеюсь что у вас всё в порядке и HEALTH_OK), как там поживают placement group (PG), надеюсь они все active+clean, кто в строю из mon и osd, а кто пал смертью храбрых и т.д.
    ceph df detail

    Вы получите информацию какие пулы (pools) размещены в вашем кластере, сколько они занимают дискового пространства как есть (STORED) и реально занято (USED), учитывая ваш фактор репликации (по умолчанию 2) для пулов типа replicated или используемую вами формулу (k и m) для пулов типа erasure code. Если используете квоты (QUOTA) и сжатие (COMPR), то увидите не нулевые значения.
    ceph versions

    Совместно с watch удобно наблюдать при обновлении серверов кластера Ceph как меняется версия и количественный состав каждой версии.
    ceph config dump

    Не всё указаное вами находится в ceph.conf. Какие-то параметры были изменены "налету" и можно напомнить себе у какого компонента (WHO) какая опция (OPTION) и чему равна (VALUE).

Демоны хранения

Ceph - это программно-определяемое хранилище (software-defined storage, SDS), поэтому важнейшие команды связаны с object storage daemon (OSD).

    ceph osd status

    Информация больше про операции чтения-записи на каждый ваш OSD.
    ceph osd df tree (краткие вариации ceph osd df или ceph osd tree)

    Тут больше про занятое место как собственно данными (DATA), в базе данных ключ-значение object map (OMAP) и метаданные (META = journals + WAL + DB).
    ceph osd utilization

    PG физически размещены на конкретных OSD и команда позволяет видеть вам - а как равномерно это сделано? Вы получите среднее (avg), стандартное отклонение (Standard Deviation, stddev), минимальное и максимальное значения. Количество PG управляется как вручную, так и в автоматическом режиме в новых версиях Ceph. Узнать текущий режим (AUTOSCALE) - ceph osd pool autoscale-status
    ceph osd pool ls detail

    Информация напомнит о количестве пулов, их типах и текущих параметрах, количестве PG и кто управляет числом - вы вручную или автомат.
    ceph osd getcrushmap -o crushmap.txt && crushtool -d crushmap.txt -o crushmap-decompile.txt

    Можно получить в текстовый файл текущую CRUSH map для дальнейшего анализа и разбирательства.
    ceph device ls

    Команда напомнит как связаны между собой диски, номера OSD и имена хостов, на которых они размещены и работают.

Демоны мониторинга

    Кратко ceph mon dump и подробнее ceph quorum_status -f json-pretty

    Mon хранят различные карты (map) и без их правильной работы невозможна работа кластера. Поэтому кворум мониторов и их работоспособность важнейшие вещи.

Демоны-менеджеры

    ceph mgr dump > ceph_mgr_dump.json

    Вывод команды очень информативен, но человеку тяжело оперировать JSON форматом, поэтому лучше перенаправить вывод в файл или на утилиту jq.
    ceph mgr dump | jq -r '[.active_name, .active_addr, .active_change] | @csv'
    ceph -s | grep mgr

    Команды напомнят когда именно были выборы активного менеджера и кто выиграл эти выборы, а кто на скамейке запасных (standbys).
    ceph mgr module ls > ceph_mgr_module_ls.json

    Какие модули и в каком состоянии находятся: выключены, включены.

Placement Group

    ceph pg dump all
    ceph pg dump_stuck

    Ожидаешь вывод ok, но если это не так, то вывод покажет какие pg и в каком состоянии находятся: unclean, inactive, stale, undersized, degraded. В помощь также полезна команда rados list-inconsistent-pg ИМЯ_ПУЛА
    ceph pg dump pgs | tr -s ' ' | cut -d' ' -f1,21,23
    ceph pg dump --format json | jq -r '.pg_map | .pg_stats[] | [.pgid, .last_scrub_stamp, .last_deep_scrub_stamp] | @csv' > ceph_pg_dump.csv

    Список всех PG с датами последней проверки (scrub) и глубокой проверки (deep scrub).
    ceph pg dump pgs | tr -s ' ' | cut -d' ' -f1,16,17

    Список PG с указанием на каких именно OSD они находятся и какая OSD для первична.

Выявление проблем и узких мест

    rbd perf image iotop
    rbd perf image iostat

    Команды дают информацию по текущему I/O (чтение/запись, байты/задержки) подобно одноимённым командам. В современных версиях Ceph модуль rbd_support должен быть включён по умолчанию, но, если у вас это не так, то ceph ceph mgr module enable rbd_support
    watch -n 1 ceph osd perf

    В комбинации с watch команда ceph osd perf покажет задержки записи поступающих данных: время, затраченное на фиксацию операций в журнале (commit_latency) и время, затраченное на сброс (flush) изменений на диск (apply_latency).

    #!/bin/bash
    ceph osd df plain | grep -F up | tr -s ' ' | sed 's/^ *//g' | cut -d' ' -f1 | \
    while read osd_id; do
        echo --- OSD ${osd_id} ---
        ceph tell osd.${osd_id} heap stats 2>&1 | grep -F "Actual"
    done;

    Создайте исполняемый скрипт, который покажет сколько ОЗУ использует каждый рабочий демон OSD. Обычно на практике на обслуживание 1 Тб дискового пространства нужно 1 Гб ОЗУ. Если у вас аномальное потребление памяти, то требуется дальнейшее разбирательство.

Поломки

    ceph crash ls
    ceph crash info ИДЕНТИФИКАТОР

    Если модуль crash включён, то падения демонов mgr, mon, osd, mds фиксируются и информация хранится 1 год прежде чем удалится автоматически.

Различные полезняшки

    Когда был создан кластер Ceph - ceph mon dump | grep created
    Список хранящегося в конкретном пуле - rbd ls --long ИМЯ_ПУЛА

 And in ceph health, I noticed its sometimes reporting slow ops on some OSDs.

Для получения подробной информации о состоянии Ceph и его Placement Groups можно использовать утилиту cephadm. Она позволяет управлять кластерами Ceph и получать детальную информацию о их состоянии.

Чтобы узнать, какие Placement Groups деградировали в Ceph, выполните следующую команду:

cephadm mon dump | jq -r '.data.degraded_pgs[]'

Эта команда выведет список всех деградированных Placement Groups в формате JSON. Если вы хотите получить более подробную информацию о каждой деградированной Placement Group, используйте следующую команду:

cephadm pg detail <degraded-pg-name>

где <degraded-pg-name> - это имя деградированной Placement Group. Эта команда выведет информацию о состоянии каждого элемента Placement Group, включая его статус и причины деградации.



 Change Replication Factor in CEPH
January 10, 2023
To set the number of object replicas on a replicated pool, execute the following:

ceph osd pool set <poolname> size <num-replicas>

Output will be:
[ceph: root@ceph1 /]# ceph osd pool set .rgw.root size 3
set pool 1 size to 3

To query OSD subsystem status, run the following command:

ceph osd stat

To write a copy of the most recent OSD map to a file (see osdmaptool), run the following command:

ceph osd getmap -o file

To write a copy of the CRUSH map from the most recent OSD map to a file, run the following command:

ceph osd getcrushmap -o file

Note that this command is functionally equivalent to the following two commands:

ceph osd getmap -o /tmp/osdmap
osdmaptool /tmp/osdmap --export-crush file

To dump the OSD map, run the following command:

ceph osd dump [--format {format}]

The --format option accepts the following arguments: plain (default), json, json-pretty, xml, and xml-pretty. As noted above, JSON is the recommended format for tools, scripting, and other forms of automation.

To dump the OSD map as a tree that lists one OSD per line and displays information about the weights and states of the OSDs, run the following command:

ceph osd tree [--format {format}]

ceph osd pool get help
Invalid command: missing required parameter var(size|min_size|pg_num|pgp_num|crush_rule|hashpspool|nodelete|nopgchange|nosizechange|write_fadvise_dontneed|noscrub|nodeep-scrub|hit_set_type|hit_set_period|hit_set_count|hit_set_fpp|use_gmt_hitset|target_max_objects|target_max_bytes|cache_target_dirty_ratio|cache_target_dirty_high_ratio|cache_target_full_ratio|cache_min_flush_age|cache_min_evict_age|erasure_code_profile|min_read_recency_for_promote|all|min_write_recency_for_promote|fast_read|hit_set_grade_decay_rate|hit_set_search_last_n|scrub_min_interval|scrub_max_interval|deep_scrub_interval|recovery_priority|recovery_op_priority|scrub_priority|compression_mode|compression_algorithm|compression_required_ratio|compression_max_blob_size|compression_min_blob_size|csum_type|csum_min_block|csum_max_block|allow_ec_overwrites|fingerprint_algorithm|pg_autoscale_mode|pg_autoscale_bias|pg_num_min|target_size_bytes|target_size_ratio)

osd pool get <pool> size|min_size|pg_num|pgp_num|crush_rule|hashpspool|nodelete|nopgchange|nosizechange|write_fadvise_dontneed|noscrub|nodeep-scrub|hit_set_type|hit_set_period|hit_set_count|hit_set_fpp|use_gmt_hitset|target_max_objects|target_max_bytes|cache_target_dirty_ratio|cache_target_dirty_high_ratio|cache_target_full_ratio|cache_min_flush_age|cache_min_evict_age|erasure_code_profile|min_read_recency_for_promote|all|min_write_recency_for_promote|fast_read|hit_set_grade_decay_rate|hit_set_search_last_n|scrub_min_interval|scrub_max_interval|deep_scrub_interval|recovery_priority|recovery_op_priority|scrub_priority|compression_mode|compression_algorithm|compression_required_ratio|compression_max_blob_size|compression_min_blob_size|csum_type|csum_min_block|csum_max_block|allow_ec_overwrites|fingerprint_algorithm|pg_autoscale_mode|pg_autoscale_bias|pg_num_min|target_size_bytes|target_size_ratio :  
get pool parameter <var>
Error EINVAL: invalid command

[root@ceph-mon-01 ceph]# ceph osd pool set --help

 General usage:
 ==============
usage: ceph [-h] [-c CEPHCONF] [-i INPUT_FILE] [-o OUTPUT_FILE]
            [--setuser SETUSER] [--setgroup SETGROUP] [--id CLIENT_ID]
            [--name CLIENT_NAME] [--cluster CLUSTER]
            [--admin-daemon ADMIN_SOCKET] [-s] [-w] [--watch-debug]
            [--watch-info] [--watch-sec] [--watch-warn] [--watch-error]
            [-W WATCH_CHANNEL] [--version] [--verbose] [--concise]
            [-f {json,json-pretty,xml,xml-pretty,plain,yaml}]
            [--connect-timeout CLUSTER_TIMEOUT] [--block] [--period PERIOD]

Ceph administration tool

optional arguments:
  -h, --help            request mon help
  -c CEPHCONF, --conf CEPHCONF
                        ceph configuration file
  -i INPUT_FILE, --in-file INPUT_FILE
                        input file, or "-" for stdin
  -o OUTPUT_FILE, --out-file OUTPUT_FILE
                        output file, or "-" for stdout
  --setuser SETUSER     set user file permission
  --setgroup SETGROUP   set group file permission
  --id CLIENT_ID, --user CLIENT_ID
                        client id for authentication
  --name CLIENT_NAME, -n CLIENT_NAME
                        client name for authentication
  --cluster CLUSTER     cluster name
  --admin-daemon ADMIN_SOCKET
                        submit admin-socket commands ("help" for help
  -s, --status          show cluster status
  -w, --watch           watch live cluster changes
  --watch-debug         watch debug events
  --watch-info          watch info events
  --watch-sec           watch security events
  --watch-warn          watch warn events
  --watch-error         watch error events
  -W WATCH_CHANNEL, --watch-channel WATCH_CHANNEL
                        watch live cluster changes on a specific channel
                        (e.g., cluster, audit, cephadm, or '*' for all)
  --version, -v         display version
  --verbose             make verbose
  --concise             make less verbose
  -f {json,json-pretty,xml,xml-pretty,plain,yaml}, --format {json,json-pretty,xml,xml-pretty,plain,yaml}
  --connect-timeout CLUSTER_TIMEOUT
                        set a timeout for connecting to the cluster
  --block               block until completion (scrub and deep-scrub only)
  --period PERIOD, -p PERIOD
                        polling period, default 1.0 second (for polling
                        commands only)

 Local commands:
 ===============

ping <mon.id>           Send simple presence/life test to a mon
                        <mon.id> may be 'mon.*' for all mons
daemon {type.id|path} <cmd>
                        Same as --admin-daemon, but auto-find admin socket
daemonperf {type.id | path} [stat-pats] [priority] [<interval>] [<count>]
daemonperf {type.id | path} list|ls [stat-pats] [priority]
                        Get selected perf stats from daemon/admin socket
                        Optional shell-glob comma-delim match string stat-pats
                        Optional selection priority (can abbreviate name):
                         critical, interesting, useful, noninteresting, debug
                        List shows a table of all available stats
                        Run <count> times (default forever),
                         once per <interval> seconds (default 1)


 Monitor commands:
 =================
osd pool set <pool> size|min_size|pg_num|pgp_num|pgp_num_actual|crush_rule|hashpspool|nodelete|nopgchange|nosizechange|write_fadvise_dontneed|noscrub|nodeep-  set pool parameter <var> to <val>
 scrub|hit_set_type|hit_set_period|hit_set_count|hit_set_fpp|use_gmt_hitset|target_max_bytes|target_max_objects|cache_target_dirty_ratio|cache_target_dirty_
 high_ratio|cache_target_full_ratio|cache_min_flush_age|cache_min_evict_age|min_read_recency_for_promote|min_write_recency_for_promote|fast_read|hit_set_
 grade_decay_rate|hit_set_search_last_n|scrub_min_interval|scrub_max_interval|deep_scrub_interval|recovery_priority|recovery_op_priority|scrub_priority|
 compression_mode|compression_algorithm|compression_required_ratio|compression_max_blob_size|compression_min_blob_size|csum_type|csum_min_block|csum_max_
 block|allow_ec_overwrites|fingerprint_algorithm|pg_autoscale_mode|pg_autoscale_bias|pg_num_min|target_size_bytes|target_size_ratio <val> [--yes-i-really-
 mean-it]
osd pool set-quota <pool> max_objects|max_bytes <val>                                                                                                          set object or byte limit on pool
[root@ceph-mon-01 ceph]#



ceph -s - https://gist.github.com/victorhooi/a5fcc9d34bbba1569dedb2515933fc23

ceph health detail - https://gist.github.com/victorhooi/840f1279f43b79991f03ce7c208abaa0

ceph osd tree - https://gist.github.com/victorhooi/0865119dea4f870e5788fe362e6376ba 


https://www.youtube.com/watch?v=OB5jysMLjIM&embeds_referring_euri=http%3A%2F%2Fvasilisc.com%2F&feature=emb_imp_woyt
http://vasilisc.com/bluestore-ceph-2017
http://vasilisc.com/ceph
https://habr.com/ru/companies/slurm/articles/536884/
http://onreader.mdl.ru/LearningCeph/content/Ch08.html?ysclid=lrruaurexm87138659
https://russianblogs.com/article/31511553304/
https://habr.com/ru/companies/slurm/articles/534912/
https://habr.com/ru/company/southbridge/blog/532250/


