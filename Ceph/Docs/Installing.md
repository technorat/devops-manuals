# Установка Ceph

## Оглавление

* [Как можно устанавливать Ceph?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Ceph/Installing.md#how)
* [Установка Ceph вручную](https://gitlab.com/technorat/devops-manuals/-/blob/main/Ceph/Installing.md#manual)

## Как можно устанавливать Ceph? <a name="how"></a>

Существуют различные методы, которые вы можете использовать для развертывания кластера хранения Ceph.

* cephadm использует контейнерную технологию (в частности, контейнеры Docker) для развертывания сервисов Ceph и управления ими в кластере машин.
* Rook развертывает и управляет кластерами Ceph, работающими в Kubernetes, а также обеспечивает управление ресурсами хранения и предоставление ресурсов через API Kubernetes.
* ceph-ansible развертывает кластеры Ceph и управляет ими с помощью Ansible.
* ceph-salt устанавливает Ceph с помощью Salt и cephadm.
* jaas.ai/ceph-mon устанавливает Ceph с помощью Juju.
* Устанавливает Ceph через Puppet.
* Ceph также можно установить вручную.

## Установка Ceph вручную <a name="manual"></a>

1. 


![Как работает Ceph](Images/1.gif)

http://onreader.mdl.ru/CephCookbook/content/Ch09.html
https://dokk.org/documentation/ceph/v16.2.14/start/intro/

https://netpoint-dc.com/blog/sozdaem-objectnoe-hranilishe-s-ispolzovaniem-ceph-p1/
