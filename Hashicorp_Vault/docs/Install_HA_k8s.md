# Установка отказоустойчивого кластера Hashicorp Vault в Kubernetes

## Развертывание отказоустойчивого кластера Hashicorp Consul в Kubernetes

Отказоустойчивый кластер Hashicorp Consul будет выполнять функции Key-Value базы данных для Hashicorp Vault и конечно же, послужит в качестве службы обнаружения сервисов.

1. Клонируем к себе репозиторий с Helm-чартом по установке Hashicorp Consul
**(Раньше этот репозиторий был в https://github.com/hashicorp/consul-helm.git)**

```shell
# git clone git@github.com:hashicorp/consul-k8s.git
```

2. Helm-чарт расположен в папке consul-k8s/charts/consul. Проверяем настройки в файле consul-k8s/charts/consul/values.yaml
Например, захотите сменить имя кластера. В этом случае меняем "datacenter: dc1" на "datacenter: home-dc"

3. Проверяем, что и как будет устанавливаться в кластере Kubernetes

```shell
# cd /gitlab/github.com/hashicorp/consul-k8s/charts/consul
# helm install --dry-run --debug consul .
```

4. Если все нормально, то запускаем без ключей "--dry-run и --debug"





