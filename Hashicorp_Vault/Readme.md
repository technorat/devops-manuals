# Hashicorp Vault

## Оглавление

* [Что такое Hashicorp Vault](https://gitlab.com/technorat/devops-manuals/-/blob/main/Hashicorp_Vault/docs/About.md)
* [Установка одиночного сервера Hashicorp Vault в качестве службы Linux](https://gitlab.com/technorat/devops-manuals/-/blob/main/Hashicorp_Vault/docs/Install_standalone.md)
* [Установка HA-кластера Hashicorp Vault в качестве службы Linux](https://gitlab.com/technorat/devops-manuals/-/blob/main/Hashicorp_Vault/docs/Install_HA_linux.md)
* [Установка HA-кластера Hashicorp Vault в Kubernetes](https://gitlab.com/technorat/devops-manuals/-/blob/main/Hashicorp_Vault/docs/Install_HA_k8s.md)


## Urls

* https://developer.hashicorp.com/vault/docs/concepts/seal
* 