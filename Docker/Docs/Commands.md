# Команды Docker

## Синтаксис утилиты

```shell
# docker опции команда опции_команды аргументы
```

## Основные опции
-D - включить режим отладки;
-H - подключиться к серверу, запущенному на другом компьютере;
-l - изменить уровень ведения логов, доступно: debug,info,warn,error,fatal;

## Опции команды run
-e - переменные окружения для команды;
-h - имя хоста контейнера;
-i - интерактивный режим, связывающий stdin терминала с командой;
-m - ограничение памяти для команды;
-u - пользователь, от имени которого будет выполнена команда;
-t - связать tty с контейнером для работы ввода и вывода;
-v - примонтировать директорию основной системы в контейнер.

## Краткое описание команд

| Команда | Что делает |
|---|---|
| docker attach | подключиться к запущенному контейнеру |
| docker build | собрать образ из инструкций dockerfile |
| docker commit | создать новый образ из изменений контейнера |
| docker cp | копировать файлы между контейнером и файловой системой |
| docker create | создать новый контейнер |
| docker diff | проверить файловую систему контейнера |
| docker events | посмотреть события от контейнера |
| docker exec | выполнить команду в контейнере |
| docker export | извлечь содержимое контейнера в архив |
| docker history | посмотреть историю изменений образа |
| docker images | список установленных образов |
| docker import | создать контейнер из архива tar |
| docker info | посмотреть информацию о системе |
| docker inspect | посмотреть информацию о контейнере |
| docker kill | остановить запущенный контейнер |
| docker load | загрузить образ из архива |
| docker login | авторизация в официальном репозитории Docker |
| docker logout | выйти из репозитория Docker |
| docker logs | посмотреть логи контейнера |
| docker pause | приостановить все процессы контейнера |
| docker port | подброс портов для контейнера |
| docker ps | список запущенных контейнеров |
| docker pull | скачать образ контейнера из репозитория |
| docker push | отправить образ в репозиторий |
| docker restart | перезапустить контейнер |
| docker rm | удалить контейнер |
| docker run | выполнить команду в контейнере |
| docker save | сохранить образ в архив tar |
| docker search | поиск образов в репозитории по заданному шаблону |
| docker start | запустить контейнер |
| docker stats | статистика использования ресурсов контейнером |
| docker stop | остановить контейнер |
| docker top | посмотреть запущенные процессы в контейнере |
| docker unpause | проложить выполнение процессов в контейнере |
|  |  |  |

## Примеры команд с опциями

### Контейнеры и образы

| Команда | Что делает | Коммент |
|---|---|---|
| docker builder prune -af | Удаляет кэш сборок, занимающий все свободное место |  |
| docker container prune | Удаляет все неработающие контейнеры |  |
| docker image prune | Удаляет образы, не связанные с контейнерами |  |
| docker image rmi -f $(docker images -aq) | Удалить все неактивные образы (без контейнеров), включая проблемные |  |
| docker images -aq | Показать все идентификаторы образов | Удобно для удаления |
| docker ps -q | Вывести только идентификаторы работающих контейнеров | Полезно для последующей обработки |
| docker ps -qa | Вывести только идентификаторы работающих и нерабочих контейнеров | Полезно для последующей обработки |
| docker ps -a --format "{{.ID}} - {{.Status}} - {{.Names}}" | Вывести только идентификаторы, статус и имена работающих и нерабочих контейнеров | |
| docker run hello-world | Запустить образ "hello-world" | Это тестовый образ. Если на текущем сервере его не находит, сразу начинает качать из https://hub.docker.com/ |
|  |  |  |

### Тома и диск

| Команда | Что делает | Коммент |
|---|---|---|
| docker system df | Сколько места реально занято на вашей машине Docker’ом |  |
| docker system prune -af | Удаление неиспользуемых данных Docker-а (сети, образы, контейнеры, тома) без проблем и подтверждений |  |
| docker volume create <имя тома> | Создать том |  |
| docker volume inspect <имя тома> | Отобразить детальную информацию о томе |  |
| docker volume ls | вывести список томов |  |
| docker volume prune  | Удалить тома, которые не используются контейнерами | Удаляет не все |
| docker volume rm <название> | удалить неиспользуемый том |  |
| docker volume rm $(docker volume ls -q) | Удалить тома, которые не используются контейнерами | Удаляет все |
|  |  |  |

### Журнал событий, информация

| Команда | Что делает | Коммент |
|---|---|---|
|  |  |  |

### Сети

| Команда | Что делает | Коммент |
|---|---|---|
| docker network prune | Удаляет неиспользуемые сети |  |
|  |  |  |
