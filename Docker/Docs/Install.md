# Установка Docker

## Оглавление
* [Установка с помощью Ansible-роли **(дописать роль, засунуть в гитлаб, вставить ссылку)**)]()
* [Установка в Ubuntu из репоpитория](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/docs/FAQ.md#ubuntu_repo)

## Установка в Ubuntu из репоpитория <a name="ubuntu_repo"></a>

1. Перед тем как переходить к установке самой программы, нужно обновить систему до актуального состояния.

```shell
# apt update && sudo apt upgrade
```

2. Перед тем как установить Docker Ubuntu необходимо установить дополнительные пакеты ядра, которые позволяют использовать Aufs для контейнеров Docker. С помощью этой файловой системы 
мы сможем следить за изменениями и делать мгновенные снимки контейнеров. Ещё надо установить пакеты, необходимые для работы apt по https.

```shell
# apt install linux-image-extra-$(uname -r) linux-image-extra-virtual apt-transport-https ca-certificates curl software-properties-common
```

3. Сначала надо добавить ключ репозитория

```shell
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

или

```shell
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

4. Установка репозитория в список источников. Можно вручную добавить в конец файла /etc/apt/sources.list репозиторий. Или создать отдельный файл (менее удобно, но более феншуйно) 
/etc/apt/sources.list.d/docker.list

```
deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu mantic stable
```

5. Обновляем кэш пакетов и устанавливаем софт. Проверяем, что служба встала и запущена

```
# apt update
# apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```
