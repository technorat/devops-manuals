# [RUN](https://docs.docker.com/reference/dockerfile/#run)

Инструкция RUN выполняет SHELL-команды для создания нового слоя поверх текущего изображения. **Набор поддерживаемых команд зависит от используемого базового образа**. Если нужной команды в базовом образе нет, то можно доустановить компонент, реализующий ее или сменить базовый образ.

Добавленный слой используется на следующем шаге в Dockerfile. RUN имеет две формы - [Shell и Exec](https://docs.docker.com/reference/dockerfile/#shell-and-exec-form):

```
# Shell-форма:
RUN [OPTIONS] <command> ...
# Exec-форма:
RUN [OPTIONS] [ "<command>", ... ]
```
