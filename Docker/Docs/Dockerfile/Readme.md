# Создание образов с помощью Dockerfile

## Оглавление
* [Введение](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Readme.md#basics)
* [Инструкции/команды](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Readme.md#commands)
* [Shell и Exec формы команд](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Shell_exec_forms.md)

## Введение <a name="basics"></a>

В файлах Dockerfile содержатся инструкции по созданию образа.
Команды в Dockerfile набраны заглавными буквами. После инструкций идут их аргументы. Инструкции, при сборке образа, обрабатываются сверху вниз.
Dockerfile - текстовые файлы, сообщающие Docker-у о том, как собирать образы, на основе которых создаются контейнеры. Контейнер - это набор слоев. Базовый образ является исходным слоем (или 
слоями) создаваемого образа. При запуске команды **docker build** для создания нового образа, подразумевается, что Dockerfile находится в текущей рабочей директории. Если этот файл находится 
в другом месте, его расположение надо указать с использованием флага -f.

Слои в итоговом образе создают только инструкции FROM, RUN, COPY, и ADD. Другие инструкции что-то настраивают, описывают метаданные, или сообщают Docker о том, что во время выполнения 
контейнера нужно что-то сделать, например — открыть какой-то порт или выполнить какую-то команду.

Образы Docker состоят из слоев, доступных только для чтения, каждый из которых является результатом инструкции в Dockerfile. Слои укладываются последовательно, и каждый из них представляет собой 
дельту, отражающую изменения, примененные к предыдущему слою.

## Инструкции/команды <a name="commands"></a>

| Команда | Описание | Комментарий |
|---|---|---|
| [ADD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Add.md) | копирует файлы и папки в контейнер, может распаковывать локальные .tar-файлы. | СОЗДАЕТ ДОПОЛНИТЕЛЬНЫЙ СЛОЙ |
| [ARG](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Arg.md) | задаёт переменные для передачи Docker во время сборки образа. |  |
| [CMD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Cmd.md) | описывает команду с аргументами, которую нужно выполнить когда контейнер будет запущен. | Аргументы могут быть переопределены при запуске контейнера. В файле может присутствовать лишь одна инструкция CMD |
| [COPY](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Copy.md) | копирует в контейнер файлы и папки. | СОЗДАЕТ ДОПОЛНИТЕЛЬНЫЙ СЛОЙ |
| [ENTRYPOINT](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Entrypoint.md) | предоставляет команду с аргументами для вызова во время выполнения контейнера. Аргументы не переопределяются. |  |
| [ENV](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Env.md) | устанавливает постоянные переменные среды. |  |
| [EXPOSE](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Expose.md) | указывает на необходимость открыть порт. |  |
| [FROM](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/From.md) | задаёт базовый (родительский) образ. |  |
| [HEALTHCHECK](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Healthcheck.md) | Проверить работоспособность контейнера при запуске. |  |
| [LABEL](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Label.md) | описывает метаданные. Например — сведения о том, кто создал и поддерживает образ. |  |
| [MAINTAINER](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Maintainer.md) | Укажите автора создаваемого образа |  |
| [ONBUILD](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Onbuilt.md) | Укажите инструкции, когда образ используется в сборке |  |
| [RUN](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Run.md) | выполняет команду и создаёт слой образа. Используется для установки в контейнер пакетов. | СОЗДАЕТ ДОПОЛНИТЕЛЬНЫЙ СЛОЙ |
| [SHELL](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Shell.md) | Установите оболочку образа по умолчанию |  |
| [STOPSIGNAL](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Stopsignal.md) | Укажите сигнал системного вызова для выхода из контейнера |  |
| [USER](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/User.md) | Установите идентификатор пользователя и группы |  |
| [VOLUME](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Volume.md) | создаёт точку монтирования для работы с постоянным хранилищем. |  |
| [WORKDIR](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/Dockerfile/Workdir.md) | задаёт рабочую директорию для следующей инструкции. |  |

