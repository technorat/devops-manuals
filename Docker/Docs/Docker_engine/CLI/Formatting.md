# Форматирование команды и вывод журнала

Docker поддерживает [шаблоны Go](https://golang.org/pkg/text/template/) в CLI.
Вы можете их использовать для управления форматом вывода определенных команд и отображения журналов. Обычно это используется в команде "Docker Inspect", или других командах, имеющих флаг --format. Далее - примеры

## join

"join" объединяет список строк для создания одной строки. Он помещает разделитель между каждым элементом в списке.

```
# docker inspect --format '{{join .Args " , "}}' container
```

## table

"table" указывает, какие поля вы хотите видеть в ее выводе.

```
# docker image list --format "table {{.ID}}\t{{.Repository}}\t{{.Tag}}\t{{.Size}}"
```

## json

json кодирует элемент как строку json.

```
# docker inspect --format '{{json .Mounts}}' container
```

## lower

lower преобразует строку в ее представление в нижнем регистре.

```
# docker inspect --format "{{lower .Name}}" container
```

## split

split разрезает строку на список строк, разделенных разделителем.

```
# docker inspect --format '{{split .Image ":"}}' container
```

## title

title делает первый символ строки заглавным.

```
# docker inspect --format "{{title .Name}}" container
```

## upper

upper преобразует строку в ее представление в верхнем регистре.

```
# docker inspect --format "{{upper .Name}}" container
```

## println

println преобразует строку в ее представление в верхнем регистре.

```
# docker inspect --format='{{range .NetworkSettings.Networks}}{{println .IPAddress}}{{end}}' container
```

## Hint

Чтобы узнать, какие данные можно распечатать, покажите весь контент в формате JSON

```
# docker container ls --format='{{json .}}'
```
