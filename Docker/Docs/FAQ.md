# Часто задаваемые вопросы (ЧАВО/FAQ) по Docker

## Оглавление

1. [Как сделать чтобы контейнер автоматически запускался при старте сервера?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q1)
2. [Error response from daemon: Get <docker registry>/v1/_ping: x509: certificate signed by unknown authority](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q2)
3. [Как зайти в запущенный контейнер Docker?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q3)
4. [Как зайти в запущенный контейнер Docker под пользователем root?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q4)
5. [Как сделать экспорт и импорт контейнера?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q5)
6. [Как изменить параметры контейнера?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q6)
7. [Как узнать, где физически расположена папка тома?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q7)
8. [Как удалить неиспользуемые контейнеры и образы?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q8)
9. [Доступен ли Docker API?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q9)
10. [Как настроить приватный Registry Docker и настроить обращение Docker-а к нему?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q10)
11. [Какие популярные Registry Docker есть?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q11)
12. [Как сделать сохранение образа в архив и загрузку на другом сервере из архива?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q12)
13. [Почему не вижу вывод команд RUN, CMD при сборке Dockerfile?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q13)
14. [Почему вместо вывода операторов RUN, CMD при сборке Dockerfile вижу только "CACHED"?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q14)
15. [Как из запущенного контейнера сохранить вывод операторов RUN/CMD, использованных при сборке, в файл на хостовом сервере?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q15)
16. [Как посмотреть, какие Volumes к каким контейнерам подключены?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Docker/Docs/FAQ.md#q16)


## Вопросы и ответы

---

### ВОПРОС: 1. Как сделать чтобы контейнер автоматически запускался при старте сервера? <a name="q1"></a>

**ОТВЕТ:** Добавить при запуске контейнера в командную строку "--restart=always"

```shell
docker run --name nginx -p 80:80 -d --restart=always nginx
```

---

### ВОПРОС: 2. Error response from daemon: Get <docker registry>/v1/_ping: x509: certificate signed by unknown authority <a name="q2"></a>

При попытке залогиниться, скачать или закачать что то в docker registry получаем ошибку сертификата

**ОТВЕТ:** Есть 2 варианта: быстрый и правильный 
1. Быстрый. Создать файл или добавить в существующий /etc/docker/daemon.json вот такой блок:

```
{
    "insecure-registries" : ["<your_registry_address>:<port>"]
}
```

И рестартуем сервис:

```shell
# systemctl restart docker
```

2. Правильный: скачать серт

```shell
# echo 1 | openssl s_client -showcerts -connect <your_registry_address>:<port> >ca.crt
```

Из полученного файла удаляем строки перед "-----BEGIN CERTIFICATE-----" и "-----END CERTIFICATE-----". И перенести файл в /etc/docker/certs.d/<your_registry_address>/docker_registry.crt
И рестартуем сервис:

```shell
# systemctl restart docker
```

---

### ВОПРОС: 3. Как зайти в запущенный контейнер Docker? <a name="q3"></a>

**ОТВЕТ:** Вот такую команду вводим для подключения к контейнеру с заданным именем "nginx". Если имя не задавалось, то можно указать просто ID контейнера.

```shell
# docker exec -it nginx bash
```

---

### ВОПРОС: 4. Как зайти в запущенный контейнер Docker под пользователем root? <a name="q4"></a>

**ОТВЕТ:** Добавляем к стандартной команде подключения еще указание идентификатора пользователя root (или другого) "-u root"

```shell
# docker exec -it -u root nginx bash
```

### ВОПРОС: 5. Как сделать экспорт и импорт контейнера? <a name="q5"></a>

**ОТВЕТ:** Это может, например, потребоваться для изменения открытых портов в контейнере, бэкапа перед опасными операциями или переноса его на другой сервер

```shell
# docker stop CONTAINER_ID 
# docker export CONTAINER_ID > OUTPUT_FILE.tar 
# cat ARCHIVE_FILE.tar | docker import - NEW_IMAGE_NAME 
# docker run -d --name NEW_CONTAINER_NAME [OPTIONS PORTS] NEW_IMAGE_NAME
```

Подробнее (тут)[https://tecadmin.net/export-and-import-docker-containers/]

---

### ВОПРОС: 6. Как изменить параметры контейнера? <a name="q6"></a>

**ОТВЕТ:** Назначенный порт можно попробовать изменить через правку конфига

Изменение других свойств контейнера
Используйте команду docker stop my-container, чтобы остановить контейнер, который вы хотите отредактировать, а затем продолжайте вносить свои изменения.

Конфигурационные файлы контейнеров имеют следующий путь

/var/lib/docker/containers/<container id>/config.v2.json
Вам нужно знать полный ID контейнера, а не усеченную версию, которую показывает docker ps.

Для этого можно использовать команду "docker inspect":

```shell
# docker inspect <short id or name> | jq | grep Id
```

Добравшись до config.v2.json контейнера, вы можете открыть его в текстовом редакторе, чтобы внести необходимые изменения. В JSON хранится конфигурация контейнера, созданная при запуске docker run. Вы можете изменить его содержимое, чтобы изменить такие 
свойства, как привязка портов, переменные окружения, тома, а также точку входа контейнера и команду. Чтобы добавить привязку к порту, найдите в файле ключ PortBindings, затем вставьте новый элемент в объект:

```shell
{
    "PortBindings": {
        "80/tcp": {
            "HostIp": "",
            "HostPort": "8080"
        }
    }
}
```

Здесь порт 80 в контейнере привязан к порту 8080 на хосте. После завершения редактирования перезапустите службу Docker и ваш контейнер:

```shell
# service docker restart
# docker start my-container
```

---

### 7. Как узнать, где физически расположена папка тома? <a name="q7"></a>

**ОТВЕТ:** Сначала командой "docker volume ls" просматриваем список имеющихся томов и находим интересующий, например "jenkins_home". И потом смотрим данные конкретно по нему:

```shell
# docker volume inspect jenkins_home
[
    {
        "CreatedAt": "2024-04-11T10:53:27+03:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/jenkins_home/_data",
        "Name": "jenkins_home",
        "Options": null,
        "Scope": "local"
    }
]
```

---

### 8. Как удалить неиспользуемые контейнеры и образы? <a name="q8"></a>

**ОТВЕТ:** Командами:

```shell
# docker rm $(docker ps -qa)

# docker image prune -a
или
# docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
```

---

### 9. Доступен ли Docker API? <a name="q9"></a>

**ОТВЕТ:** Можно посмотреть открытые порты и стукнуться в порт API командами:

```shell
# netstat -tunlp
# curl http://10.15.253.223:2375/version
```

---

### 10. Как настроить приватный Registry Docker и настроить обращение Docker-а к нему? <a name="q10"></a>

**ОТВЕТ:** 1. Устанавливаем Sonartype Nexus. 
2. Настраиваем 3 репозитория:

| Имя | Тип | Формат |
|---|---|---|
| baseimages | hosted | docker |
| docker-proxy | proxy | docker |
| docker-group | group | docker |

baseimages - локальное хранилище для самостоятельно закачиваемых образов.
docker-proxy - прокси, который перенаправляет запросы в интернет, к докерхабу, если чего то локально не нашел
docker-group - единый эндпоинт для запросов docker-а, который отдает и локальные образы, и перебрасывает пакеты из интернета. Включаем в группу и baseimages, и docker-proxy, и другие репозитории с образами docker

3. Далее переходим к настройке самой машины/сервера. Редактируем файл /etc/docker/daemon.json, если такого нет — то создайте. 

```shell
{
        "insecure-registries": ["адрес_nexus:порт_репозитория"],
        "registry-mirrors": ["http://адрес_nexus:порт_репозитория"]
}
```

4. Сохраняем и перезапускаем docker, что бы изменения применились: 

```shell
# systemctl restart docker
```

---

### 11. Какие популярные Registry Docker есть? <a name="q11"></a>

**ОТВЕТ:** 

* docker.io
* quay.io
* gcr.io
* k8s.gcr.io
* ghcr.io
* mcr.microsoft.com
* registry.gitlab.com

---

### ВОПРОС: 12. Как сделать сохранение образа в архив и загрузку на другом сервере из архива? <a name="q12"></a>

**ОТВЕТ:** 

```shell
# docker image save myimage:v1.0.0 | gzip > myimage.tar.gz
# docker image load --input myimage.tar.gz
```

---

### ВОПРОС: 13. Почему не вижу вывод команд RUN, CMD при сборке Dockerfile? <a name="q13"></a>

**ОТВЕТ:** Добавить при запуске контейнера в командную строку "--progress=plain":

```
docker build --progress=plain .
```

---

### ВОПРОС: 14. Почему вместо вывода команд RUN, CMD при сборке Dockerfile вижу только "CACHED"? <a name="q14"></a>

**ОТВЕТ:** При нескольких последовательных сборках, результаты одинаковых команд кэшируются. Чтобы увидеть результаты некэшированной обработки команд, нужно добавить при запуске контейнера в командную строку "--no-cache":

```
docker build --progress=plain --no-cache .
```

---

### ВОПРОС: 15. Как из запущенного контейнера сохранить вывод операторов RUN/CMD, использованных при сборке, в файл на хостовом сервере? <a name="q15"></a>

**ОТВЕТ:** 

Сначала добавим в Dockerfile инструкцию для перенаправления вывода в файл. Он попадет в собранный образ. **Используем только Shell форму команды, т.к. Exec форма перенаправлять в файл не умеет!**

```
FROM alpine:latest
CMD cat /etc/alpine-release > /home/output.txt
```

при старте контейнера подмонтируем текущий (к примеру) каталог, в него при запуске и будет сохранен файл вывода

```
# docker run -v $(pwd):/home <имя_образа>
```

---

### ВОПРОС: 16. Как посмотреть, какие Volumes к каким контейнерам подключены? <a name="q16"></a>

**ОТВЕТ:**

Чтобы получить просто список контейнер-том, можно использовать вот такой bash-скрипт:

```
for container in $(docker ps -q); do echo $container; docker inspect $container | jq '.[].Mounts[] | {Volume: .Name, Container: "'"$container"'"}'; done
```

Или можно изучать информацию по какому то конкретному контейнеру. Сначала выведем список всех контейнеров:

```
# docker ps -a
```

Выбрали имя или идентификатор контейнера и запросили информацию конкретно по нему:

```
# docker inspect <имя/ID>
```

Чтобы вручную не просматривать весь список параметров, можно выбрать с помощью "sed" только блок с монтированными томами:

```
# docker inspect <имя/ID> | sed -n '/Mounts/,/]/p'
```

Или с помощью "grep":

```
# docker inspect <имя/ID> | grep -Pzo '(?s)Mounts.*?]'
```


