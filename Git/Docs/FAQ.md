# Часто задаваемые вопросы (ЧАВО/FAQ) по Git

## Оглавление

* [Как из GIT-репозитория удалить все коммиты, кроме последнего?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#kill_old_commits)
* [Как настроить Git чтобы он не спрашивал пароль при доступе по SSH?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#ssh-config)
* [Как настроить Git чтобы для каждого репозитория были свои настройки пользователей?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#few_repos)
* [Как поставить pull --rebase в качестве настройки по-умолчанию?](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#pull_rebase_default)
* [CONFLICT (add/add): Merge conflict in .gitlab-ci.yml error: could not apply <commit_hash>](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#merge_conflict)
* [Как обновить текущую ветку из dev](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#update_from_dev)
* [Updates were rejected because the remote contains work that you do not have locally](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md#updates_rejected)


###  Как из GIT-репозитория удалить все коммиты, кроме последнего? <a name="ssh-config"></a>

**Вопрос:** 
Как сделать так, чтобы вся история ветки master сократилась только до последнего коммита? Должно получится так, будто мы создали новый репозиторий, куда залили файлики и все.

**Ответ:**
Это можно сделать через 

```shell
# git rebase --root -i
```

откроется редактор.
Если вы хотите сохранить историю изменений, то у первого коммита нужно указать "pick", а у остальных "squash" или "fixup". Squash объединяет все коммиты после pick в один.
Если вы хотите удалить историю изменений и коммиты полностью, то у первого коммита нужно указать "drop", а у остальных "squash" или "fixup". Squash объединяет все коммиты после pick в один.

### Вопрос: Как настроить Git чтобы он не спрашивал пароль при доступе по SSH? <a name="ssh-config"></a>

**Ответ:**

1. Генерируем SSH-ключ

```shell
# ssh-keygen -t ecdsa -b 521
```

2. Прописываем свой публичный ключ в настройках своего профиля (например в Gitlab-е)

3. В настройках /root/.ssh/config указываем сервер и файл ключа

```shell
# vi /root/.ssh/config
# GitLab.com
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ecdsa
# GitHub.com
Host github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_ecdsa
```

### Вопрос: Как настроить Git чтобы для каждого репозитория были свои настройки пользователей? <a name="few_repos"></a>

**Ответ:**

1. Создаем в корне папку /git

2. Создаем в ней подпапки для кадого из репозиториев (например gitlab.com и bitbucket.my-company.ru)

3. В каждой папке инициализируем репозиторий и прописываем уникальные настройки

```shell

# cd /git/gitlab.com 
# git init
# git config --local user.name "<UserName1>"
# git config --local user.email "<email1>"
# cd /git/bitbucket.my-company.ru
# git init
# git config --local user.name "<UserName1>"
# git config --local user.email "<email1>"

```
Теперь можно сюда клонировать что надо

### Как поставить pull --rebase в качестве настройки по-умолчанию <a name="pull_rebase_default"></a>

[https://stackoverflow.com/questions/13846300/how-to-make-git-pull-use-rebase-by-default-for-all-my-repositories](https://stackoverflow.com/questions/13846300/how-to-make-git-pull-use-rebase-by-default-for-all-my-repositories)

Существует 3 различных уровня конфигурации для поведения извлечения по умолчанию. От самых общих до самых мелких:
1. pull.rebase
Установка для этого параметра значения true означает, что git pull всегда эквивалентен git pull --rebase (если только для Branch.<branchname>.rebase явно не установлено значение false). Это также можно установить для каждого репозитория или глобально.

2. branch.autosetuprebase

```shell
# git config branch.autosetuprebase always
```

Установка значения «always» означает, что при создании ветки отслеживания для нее будет создана запись конфигурации, подобная приведенной ниже. Для более детального управления для этого параметра также можно установить значение "never", "local" или "local", а также установить его для каждого репозитория или глобально. Дополнительную информацию см. в "git config --help".

3. branch.<branchname>.rebase
Установка этого параметра в значение "true" означает, что эта конкретная ветвь всегда будет извлекать данные из восходящего потока посредством перебазирования, если только "git pull --no-rebase" не используется явно.

Таким образом, хотя вы не можете изменить поведение по умолчанию для всех будущих клонов репозитория, вы можете изменить поведение по умолчанию для всех репозиториев текущего пользователя (существующих и будущих) с помощью "git config --global pull.rebase true".

### CONFLICT (add/add): Merge conflict in .gitlab-ci.yml error: could not apply <commit_hash> <a name="merge_conflict"></a>

При попытке обновить свою ветку данными из ветки "dev" получаем сообщение об ошибке. Данные из удаленного репозитория конфликтуют с нашими. В моем случае было так:

```shell
# git pull --rebase origin dev
From <ссылка на репозиторий>
 * branch            dev        -> FETCH_HEAD
Auto-merging .gitlab-ci.yml
CONFLICT (add/add): Merge conflict in .gitlab-ci.yml
error: could not apply e3d30b5... test
hint: Resolve all conflicts manually, mark them as resolved with
hint: "git add/rm <conflicted_files>", then run "git rebase --continue".
hint: You can instead skip this commit: run "git rebase --skip".
hint: To abort and get back to the state before "git rebase", run "git rebase --abort".
Could not apply e3d30b5... test
```

Для разрешения конфликта слияния в .gitlab-ci.yml с сообщением «CONFLICT (add/add): Merge conflict in .gitlab-ci.yml error: could not apply» можно следовать таким шагам:

* Найти файлы, которые не удалось слить. Для этого нужно запустить команду git status. В полученном сообщении обратить внимание на файлы, перечисленные в заголовке «Неслитые пути». Это те файлы, которые нужно исправить вручную на следующем этапе. 1
* Открыть каждый файл и вручную разрешить конфликт слияния. Для этого нужно открыть файл в текстовом редакторе. Найти маркер конфликта (<<<<<<<), который git добавил в файл во время неудачного слияния. Этот маркер указывает конкретные места в файле, которые нужно отредактировать вручную. 1
* Поставить на стадию каждый отредактированный файл. Это сообщит git, что конфликты в этих файлах разрешены. Например, для этого можно использовать команду git add. 1
* Продолжить операцию слияния. Для этого нужно запустить команду git cherry-pick continue. 1

Также для разрешения конфликтов в GitLab можно использовать пользовательский интерфейс. 23 Для этого нужно на левой боковой панели сверху выбрать «Поиск GitLab», чтобы найти свой проект. Затем выбрать «Код» > «Запросы на слияние» и найти запрос на слияние. После этого выбрать «Обзор» и прокрутить до раздела отчётов о запросах на слияние. Найти сообщение о конфликтах при слиянии и выбрать «Разрешить конфликты». 2
Выбор метода зависит от сложности конфликта и личных предпочтений пользователя.

### Как обновить текущую ветку из dev <a name="update_from_dev"></a>

```shell
git pull --rebase origin dev
```

После исправления конфликтов, если есть:

```shell
git push --force
```

[https://habr.com/ru/post/337302/](https://habr.com/ru/post/337302/)

### Updates were rejected because the remote contains work that you do not have locally <a name="updates_rejected"></a>

При попытке запушить в удаленный репозиторий мы получаем ошибку. В моем случае полное собщение выглядело так:

```shell
# git push
To gitlab.com:technorat/devops-manuals.git
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'gitlab.com:technorat/devops-manuals.git'
hint: Updates were rejected because the remote contains work that you do not
hint: have locally. This is usually caused by another repository pushing to
hint: the same ref. If you want to integrate the remote changes, use
hint: 'git pull' before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Почему такое происходит? Кто то внес в удаленну ветку изменения, которых у нас на диске нет. В этом случае сначала надо скачать обновления с ребейзингом того, что на диске и запушить в удаленный репозиторий:

```shell
# git pull --rebase
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 2), reused 0 (delta 0), pack-reused 0 (from 0)
Unpacking objects: 100% (3/3), 278 bytes | 92.00 KiB/s, done.
From gitlab.com:technorat/devops-manuals
   8e70190..ba73ee7  main       -> origin/main
Successfully rebased and updated refs/heads/main.

# git push
Enumerating objects: 16, done.
Counting objects: 100% (16/16), done.
Delta compression using up to 24 threads
Compressing objects: 100% (9/9), done.
Writing objects: 100% (9/9), 4.08 KiB | 1.36 MiB/s, done.
Total 9 (delta 3), reused 0 (delta 0), pack-reused 0
To gitlab.com:technorat/devops-manuals.git
   ba73ee7..7aec0b0  main -> main
```

----------------
Дописать!
Фикс для git чтобы не спрашивал по 100500 раз пароли
Выполняем команду:

```shell
git config --global credential.helper store
```

потом делаем git pull и вводим логин с паролем, они сохранятся в ~/.git-credentials и для этой репы гит больше не будет спрашивать пароля.
Снятие ограничения на длину пути в GIT:

sudo git config --system core.longpaths true
