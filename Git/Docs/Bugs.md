# Баги, возникающие при работе с Git

## You have divergent branches...
**Описание:**
При переключении на какую то ветку сначала получаем сообщение:
Переключились на ветку «develop»
Ваша ветка опережает «origin/develop» на 1 коммит.
Потом при запуске команды 
# git pull
получаем сообщение:
подсказка: You have divergent branches and need to specify how to reconcile them.
подсказка: You can do so by running one of the following commands sometime before
подсказка: your next pull:
подсказка: 
подсказка:   git config pull.rebase false  # merge (the default strategy)
подсказка:   git config pull.rebase true   # rebase
подсказка:   git config pull.ff only       # fast-forward only
подсказка: 
подсказка: You can replace "git config" with "git config --global" to set a default
подсказка: preference for all repositories. You can also pass --rebase, --no-rebase,
подсказка: or --ff-only on the command line to override the configured default per
подсказка: invocation.
**Как исправить:**

