# Выдержки из доков по Git

## Оглавление
* [Глоссарий](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#glossary)
* [Наиболее распространенные внешние репозитории](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#remote_repos)
* [Клонирование существующего репозитория](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#cloning)
* [Конфиг](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#config)
* [Статусы файлов](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#file_status)
* [Origin](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#origin)
* [Коммит](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#commit)
* [Сообщение коммита](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#commit_message)
* [Начало работы с внешним репозиторием](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#remote_repo_begin)
* [Чем отличается "git fetch" от "git pull"](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#fetch_pull)
* [Pull Request-ы](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#pull_request)
* [Нюансы и хитрости](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Short.md#tricks)

## Глоссарий <a name="glossary"></a>

* Git или Гит — система контроля и управления версиями файлов.
* GitHub, GitLab, BitBucket и др. — это веб-сервисы для централизованного размещения репозиториев и совместной разработки проектов.
* Git Bash – это оболочка, основанная на Bash и реализующая все его стандартные функции.
* Ветка (Brunch) – это последовательность коммитов, параллельная версия репозитория. Технически, ветка – это именованный коммит, имеющий несколько дочерних коммитов. Она включена в этот 
репозиторий, но не влияет на главную версию, тем самым позволяя свободно работать в параллельной. Когда вы внесли нужные изменения, то вы можете объединить их с главной версией (merge). 
Преимущество веток в их независимости. Вы можете вносить изменения в файлы на одной ветке и они никак не скажутся на файлах в другой ветке.
* Индекс – файл, в котором содержатся изменения, подготовленные для добавления в коммит. Вы можете добавлять и убирать файлы из индекса.
* Кодревью — процесс проверки кода на соответствие определённым требованиям, задачам и внешнему виду.
* Клонирование (Clone) — скачивание репозитория с удалённого сервера на локальный компьютер в определённый каталог для дальнейшей работы с этим каталогом как с репозиторием.
* Коммит – фиксация изменений, внесенных в индекс. Другими словами, коммит – это единица изменений в вашем проекте. Коммит хранит измененные файлы, имя автора коммита и время, в которое был 
сделан коммит. Кроме того, каждый коммит имеет уникальный идентификатор, который позволяет в любое время к нему откатиться.
* Локальный репозиторий — репозиторий, расположенный на локальном компьютере разработчика в каталоге. Именно в нём происходит разработка и фиксация изменений, которые отправляются на удалённый 
репозиторий.
* Мастер (Master) — главная или основная ветка репозитория. 
* Мёрдж (Merge) — слияние изменений из какой-либо ветки репозитория с любой веткой этого же репозитория. Чаще всего слияние изменений из ветки репозитория с основной веткой репозитория.
* Обновиться из апстрима — обновить свою локальную версию форка до последней версии основного репозитория, от которого сделан форк.
* Пул (Pull) — получение последних изменений с удалённого сервера репозитория.
* Пулреквест (Pull Request) — запрос на слияние форка репозитория с основным репозиторием. Пулреквест может быть принят или отклонён вами, как владельцем репозитория.
* Пуш (Push) — отправка всех неотправленных коммитов на удалённый сервер репозитория.
* Обновиться из ориджина — обновить свою локальную версию репозитория до последней удалённой версии этого репозитория.
* Репозиторий Git– каталог файловой системы, рабочая папка проекта, отслеживаемого Git. Она содержит дерево изменений проекта в хронологическом порядке, файлы конфигурации, файлы журналов 
операций, выполняемых над репозиторием, индекс расположения файлов и хранилище, содержащее сами контролируемые файлы. Все файлы истории хранятся в специальной папке .git/ внутри папки проекта.
* Указатели HEAD, ORIGHEAD и т. д. – это ссылка на определенный коммит. Ссылка – это некоторая метка, которую использует Git или сам пользователь, чтобы указать на коммит.
* Удалённый или внешний репозиторий — репозиторий, находящийся на удалённом сервере. Это общий репозиторий, в который приходят все изменения и из которого забираются все обновления.
* Форк (Fork) — копия репозитория. Его также можно рассматривать как внешнюю ветку для текущего репозитория. Копия вашего открытого репозитория на Гитхабе может быть сделана любым 
пользователем, после чего он может прислать изменения в ваш репозиторий через пулреквест.


## Наиболее распространенные внешние репозитории <a name="remote_repos"></a>

* [**GitLab**](https://gitlab.com) - До определенных ограничений бесплатен. Нужно больше - бери тариф. Есть встроенная хорошая автоматизация деплоя - Gitlab-CI. Есть бесплатная (Community) с 
ограниченными возможносями и корпоративная версия с расширенными функциями, которые можно развернуть у себя локально на сервере. Часть русских проектов закрыли с началом СВО, могут бахнуть 
когда угодно кого угодно. Имеет хорошую интеграцию с Jira. Есть встроенный Docker registry и Packet Registry для образов Docker и готовых пакетов, зависимостей. Не нужен Dockerhub, Nexus. 
Большой бесплатный lfs. Не очень хорошая интеграция со сторонними решениями. Есть GitLab Pages для публикации веб-страниц.
* [**GitHub**](https://github.com) - Есть встроенная не очень хорошая автоматизация деплоя - github actions, лучше используйте Jenkins, Teamcity, CircleCI, Bamboo. Хорошая интеграция 
со сторонними сервисами. Есть GitHub Pages для публикации веб-страниц.
* [**Bitbucket**](https://bitbucket.org) - Есть встроенная автоматизация деплоя - Bitbucket Pipelines (говно полное), лучше используйте Jenkins, CircleCI, Bamboo. Часть экосистемы Atlassian, 
поэтому имеет хорошую интеграцию с Jira, Confluence. Есть усеченная возможность работать бесплатно. Маленький lfs. Народ на него плюется.
* [**Mos.Hub**](https://hub.mos.ru/) - Российский государственный проект на базе GitLab.
* [**Azure DevOps**]() - Целая экосистема продуктов Microsoft. Контроль версий, автодеплой, сбор данных и построение отчетов. Вроде удобно.

## Клонирование существующего репозитория <a name="cloning"></a>

Для получения копии существующего Git-репозитория, используют команду:

'''
# git clone <URL репозитория>
'''

Будет создан каталог с названием проекта/репозитория. Если хотите, чтобы на компьютере имя папки отличалось от имени репозитория, то указываем так:

'''
# git clone <URL репозитория> <каталог>
'''

**Git следит за тем, что происходит на компьютере и если будут найдены 2 папки с одинаковым проектом на диске, то он создаст жесткие ссылки.** Это приведет к тому, что изменение одних файлов 
в одной папке, может повлиять на файлы в другой папке. Чтобы этого не произошло (и для бэкапов), клонировать лучше так:

'''
# git clone --no-hardlinks <URL репозитория>
'''

Клинирование может проводиться по протоколам HTTPS, SSH или собственному GIT. Иногда SSH и GIT могут наружу не пропускать фаерволлы, тогда остается один рабочий вариант: HTTPS.

## Конфиг <a name="config"></a>

В состав Git входит утилита git config, которая позволяет просматривать и настраивать параметры, контролирующие все аспекты работы Git, а также его внешний вид. Эти параметры могут быть 
сохранены в трёх местах:
* Представляют собой настройки на уровне всей системы, то есть они распространяются на всех пользователей. Файл с этими настройками хранится по следующему пути: 
C:\Program Files\Git\etc\gitconfig для Windows и /etc/gitconfig для пользователей Linux/MacOS. Файл [path]/etc/gitconfig содержит значения, общие для всех пользователей системы и для всех 
их репозиториев. Если при запуске git config указать параметр --system, то параметры будут читаться и сохраняться именно в этот файл. Так как этот файл является системным, то вам потребуются 
права суперпользователя для внесения изменений в него.
* Файл ~/.gitconfig или ~/.config/git/config хранит настройки конкретного пользователя. Этот файл используется при указании параметра --global и применяется ко всем репозиториям, с которыми 
вы работаете в текущей системе.
* Файл config в каталоге Git (т. е. .git/config) репозитория, который вы используете в данный момент, хранит настройки конкретного репозитория. Вы можете заставить Git читать и писать в этот 
файл с помощью параметра --local, но на самом деле это значение по умолчанию. Неудивительно, что вам нужно находиться где-то в репозитории Git, чтобы эта опция работала правильно.
Настройки на каждом следующем уровне подменяют настройки из предыдущих уровней, то есть значения в .git/config перекрывают соответствующие значения в [path]/etc/gitconfig.

## Статусы файлов <a name="file_status"></a>

Запомните, каждый файл в вашем рабочем каталоге может находиться в одном из двух состояний: под версионным контролем (отслеживаемые) и нет (неотслеживаемые).

* Отслеживаемые файлы — это те файлы, о которых знает Git, т.е. которые были в последнем снимке состояния проекта. Они могут быть неизменёнными, изменёнными или подготовленными к коммиту. 
* Неотслеживаемые файлы — это всё остальное, любые файлы в вашем рабочем каталоге, которые не входили в ваш последний снимок состояния и не подготовлены к коммиту. Когда вы впервые клонируете 
репозиторий, все файлы будут отслеживаемыми и неизменёнными, потому что Git только что их извлек и вы ничего пока не редактировали.

Когда вы создадите файлы, они будут в статусе "неотслеживаемые".
Когда вы отредактируете файлы, которые уже были в зафиксированы и сохранены в репозитории, они станут в статусе "отслеживаемые, изменённые".
Когда проиндексируете изменения командой "git add ...>, вы переводите файлы в статус "отслеживаемые, подготовленные к коммиту".
Когда зафиксируете все проиндексированные изменения командой "git commit ...", вы переводите файлы в статус "отслеживаемые".

## Origin <a name="origin"></a>

Если вы клонировали репозиторий, то origin — имя по умолчанию, которое Git даёт серверу, с которого производилось клонирование. Вы можете также использовать команду git remote с ключом -v, 
чтобы просмотреть адреса для чтения и записи, привязанные к репозиторию

```
$ git clone https://github.com/schacon/ticgit
Cloning into 'ticgit'...
remote: Reusing existing pack: 1857, done.
remote: Total 1857 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1857/1857), 374.35 KiB | 268.00 KiB/s, done.
Resolving deltas: 100% (772/772), done.
Checking connectivity... done.
$ cd ticgit
$ git remote
origin
# git remote -v
origin git@gitlab.com:technorat/devops-manuals.git (fetch)
origin git@gitlab.com:technorat/devops-manuals.git (push)
```

## Коммит <a name="commit"></a>

Коммит - это фиксация **проиндексированных** изменений. Те изменения, которые произведены после индексации, в коммит не попадут. Что можно делать с коммитами:
* Перед тем, как делать коммит, можно посмотреть и настроить, какие файлы в него попадут.
* Можно зафиксировать изменения только в одном файле из целого проекта.
* Вы можете занести в коммит только часть изменения в файле. А остальные изменения откатить, или положить в другой коммит.
* Просмотр истории коммитов и различий в файлах.
* Можно посмотреть, какие изменения были внесены в файл в разных коммитах.
* Можно посмотреть историю коммитов всей ветки, чтобы проследить, как менялись файлы.
Git сохраняет в commit содержимое всех файлов (делает слепки содержимого каждого файла и сохраняет в objects). Если файл не менялся, то будет использован старый object. Таким образом, 
в commit в виде новых объектов попадут только изменённые файлы. Не стоит хранить «тяжёлые» файлы, бинарники и прочее без явной необходимости. Они там останутся навсегда и будут в каждом 
клоне репозитория. Каждый коммит может иметь несколько коммитов-предков и несколько дочерних-коммитов. Так образуется дерево (граф).

![Дерево (граф) коммитов](../Images/Commits-tree.png)

Мы можем переходить в любую точку этого дерева с помощью команды:

```
git checkout <commit>
```

Каждое слияние двух и более коммитов в один — это merge. Если любому коммиту задать имя, то такой коммит называется "ветка" или "бранч". Аналогично и с тэгом (tag) - это просто именованный 
коммит. "HEAD" работает по такому же принципу — показывает, где мы есть сейчас. Новые коммиты являются продолжением текущей ветки, они добавляются туда, куда и смотрит HEAD. Указатели можно 
свободно перемещать на любой коммит, если это не tag. Tag для того и сделан, чтобы раз и навсегда запомнить коммит и никуда не двигаться. Но его можно удалить.

## Сообщение коммита <a name="commit_message"></a>

Как правило, ваши сообщения должны начинаться кратким однострочным описанием не более 50 символов, затем должна идти пустая строка, после которой следует более детальное описание. Проект Git 
требует, чтобы детальное описание включало причину внесения изменений и сравнение с текущей реализацией — это хороший пример для подражания. Пишите сообщение коммита в императиве: «Fix bug» а 
не «Fixed bug» или «Fixes bug». Вот шаблон хорошего сообщения коммита:

Краткое (не более 50 символов) резюме с заглавной буквы

Более детальный, поясняющий текст, если он требуется. Старайтесь не превышать длину строки в 72 символа. Пустая строка, отделяющая сводку от тела, имеет решающее значение, в противном случае 
такие инструменты, как rebase, могут вас запутать. Сообщения коммитов следует писать используя неопределённую форму глагола совершенного вида повелительного наклонения: «Fix bug» (Исправить 
баг), а не «Fixed bug» (Исправлен баг) или «Fixes bug» (Исправляет баг). Это соглашение соответствует сообщениям коммитов, генерируемых такими командами, как `git merge` и `git revert`.
Последующие абзацы идут после пустых строк. - Допускаются обозначения пунктов списка. - Обычно, элементы списка обозначаются с помощью тире или звёздочки, с одним пробелом перед ними, а 
разделяются пустой строкой, но соглашения могут отличаться.  - Допускается обратный абзацный отступ.
В проекте Git все сообщения к коммитам имеют расширенное форматирование — выполните команду git log --no-merges, чтобы увидеть как выглядит хорошо отформатированная история коммитов.

## Начало работы с внешним репозиторием <a name="remote_repo_begin"></a>

Есть 2 основных сценария для начала работы с внешним репозиторием:
1. **Создаем проект в gitlab.com (или любой аналогичный) и в него загружаем имеющийся локальный репозиторий**:
* Создали проект в gitlab.com, например "devops-manuals". Копируем команду для клонирования кода
* Создали необходимую структуру папок на локальном диске под дерево проектов/репозиториев и в ней папку для локального репозитория, например "devops-manuals".

```
# mkdir -p /git/gitlab.com/technorat/devops-manuals
```

* Переходим в папку, созданную под проект и инициализируем репозиторий локально локально

```
# cd /git/gitlab.com/technorat/devops-manuals
# git init
```

* Накидываем в папку/репозиторий нужные файлы, указываем внешний репозиторий, индексируем файлы, коммитим и пушим в внешний репозиторий

```
# git remote add origin git@gitlab.com:technorat/devops-manuals.git
# git add --all
# git commit -m "First commit"
# git push -u origin master
```

2. **Клонируем имеющийся проект из gitlab.com в локальную папку**:
* Создали проект в gitlab.com, например "devops-manuals". Копируем команду для клонирования кода
* Создали необходимую структуру папок на локальном диске под дерево проектов/репозиториев

```
# mkdir -p /git/gitlab.com/technorat
```

* Переходим в созданную папку, вставляем скопированную ранее команду и клонируем туда нужный код

```
# cd /git/gitlab.com/technorat
# git clone git@gitlab.com:technorat/devops-manuals.git
```

## Чем отличается "git fetch" от "git pull" <a name="fetch_pull"></a>

git fetch

git pull
=======
## Как проверить, работает ли SSH до удаленного репозитория

```
# ssh -vvv git@github.com
```


## git clone --no-hardlinks origin dev1
Теперь клонируем его от имени разработчиков. Тут есть только один нюанс, которого не будет при работе с сервером: git, понимая, что репозитории локальные и находятся на одном разделе, будет создавать ссылки, а не делать полную копию. А нам для изучения нужна полная копия. Для этого можно воспользоваться ключом --no-hardlinks или явно указать протокол:

$ git clone --no-hardlinks origin dev1
Cloning into 'dev1'...
warning: You appear to have cloned an empty repository.
done.
$ git clone --no-hardlinks origin dev2
Cloning into 'dev2'...
warning: You appear to have cloned an empty repository.
done.


Git не хранит папки
Git не хранит файлы
Ревизии (Revision) не имеют порядкового номера
Редакции (правки, commits) могут идти не по-порядку
В Git нет веток* (с небольшой оговоркой)

Git сохраняет в commit содержимое всех файлов (делает слепки содержимого каждого файла и сохраняет в objects). Если файл не менялся, то будет использован старый object. Таким образом, в commit в виде новых объектов 
попадут только изменённые файлы, что позволит хорошо экономить место на диске и даст возможность быстро переключиться на любой commit.

## Pull Request-ы <a name="pull_request"></a>

Pull Request — это механизм, с помощью которого разработчик уведомляет участников команды о том, что он подготовил некий функционал. Закончив работу над веткой, разработчик создает Pull Request. Так все участники 
процесса узнают, что требуется проверить код и выполнить слияние с указанной веткой. Pull Request — это не просто уведомление, а специальный форум для обсуждения предлагаемых изменений. Если с изменениями возникли 
какие-либо проблемы, участники команды могут публиковать в запросе pull отзывы и даже изменять код Pull Request-а с помощью дополнительных коммитов. Все эти действия отслеживается непосредственно внутри запроса pull.

## Нюансы и хитрости <a name="tricks"></a>

### Клонирование нескольких копий на один раздел

При клонировании нескольких копий одного внешнего репозитория на один раздел, но в разные папки, git понимает это и экономит место, создавая хард-линки на файлы во всех последующих копиях. Если вы хотите этого избежать, 
то надо добавлять ключ "--no-hardlinks". В документации формат команды вот такой (**надо уточнять**):

```
# git clone --no-hardlinks origin dev1
```


