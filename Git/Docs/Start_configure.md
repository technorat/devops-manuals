# Подготовка машины к работе с несколькими серверами и проектами

## 1. Создаем корневой каталог для всех проектов

Например /git. Даем на него права вашему непривилегированному пользователю

## 2. Делаем главные настройки

Они пределяют в общем конфиге стартовую ветку и редактор по умолчанию (ed, vi, emacs, mcedit)

```shell
# git config --system init.defaultBranch main
# git config --system core.editor mcedit
```

Это запишет настройку для всех пользователей и репозиториев на компе. Если нужно сделать настройку только для текущего пользователя:

```shell
# git config --global init.defaultBranch main
# git config --global core.editor mcedit
```

Можете сразу проверить, что изменения применились

```shell
# cat /etc/gitconfig 
[core]
        editor = mcedit
[init]
        defaultBranch = main

# git config --list --show-origin
```

## 3. Делаем настройку вашего личного пользователя по умолчанию.

Если вне работы пользуетесь одним именем пользователя и почтой на разных git-серверах
(gitlab.com, github.com, bitbucket.com и т.д.), то удобно вписать сюда личную учетку. А для рабочих проектов сделаем отдельные настройки

```shell
# git config --global user.name <имя_пользователя>
# git config --global user.email <электронная_почта>
```

Можете сразу проверить, что изменения применились

```shell
# cat ~/.gitconfig 

# git config --list --show-origin
```

## 4. Настройка отдельных конфигов для разных работ и серверов

Предполагаем, К ПРИМЕРУ, что вы работаете на двух работах, у вас есть: 
* одинаковые учетки "PrivateUser" c почтой "private-user@mail.ru" на серверах gitlab.com и github.com
* компания "company1" с сервером gitlab.company1.ru, один проект "Project", учетка на нем "WorkUser" с почтой "workuser@company1.ru"
* компания "company2", с серверами gitlab.company2.ru и bb.company2.ru. Один проект "Project1" на сервере gitlab.company2.ru, учетка на нем "WorkUser1" 
с почтой "workuser1@company2.ru". Второй проект "Project2" на сервере bb.company2.ru, учетка на нем "WorkUser2" с почтой "workuser1@company2.ru".

Создаем каталоги для личных и корпоративных учеток:
* /git/gitlab.com/PrivateUser
* /git/github.com/PrivateUser
* /git/company1.ru/gitlab.company1.ru
* /git/company2.ru/gitlab.company2.ru
* /git/company2.ru/bb.company2.ru

Переходим в каждый из этих каталогов и делаем там git init. В каждом из рабочих каталогов создаем файл cat .gitconfig. Дописываем туда учетки 
пользователей:

```shell
# cat /git/company1.ru/gitlab.company1.ru/.gitconfig

[user]
    name = WorkUser
    email = workuser@company1.ru

# cat /git/gitlab.company2.ru/.gitconfig

[user]
    name = WorkUser1
    email = workuser1@company2.ru

# cat /git/bb.company2.ru/.gitconfig

[user]
    name = WorkUser2
    email = workuser1@company2.ru
```

В вашем глобальном конфиге пользователя добавляем использование конкретных конфиг-файлов в зависимости от каталога

```shell
# cat ~/.gitconfig
[user]
        name = PrivateUser
        email = private-user@mail.ru

[includeIf "gitdir:/git/company1.ru/gitlab.company1.ru/"]
  path = /git/gitlab.com/technorat/.gitconfig

[includeIf "gitdir:/git/gitlab.company2.ru/"]
  path = /git/gitlab.company2.ru/.gitconfig

[includeIf "gitdir:/git/bb.company2.ru/"]
  path = /git/bb.company2.ru/.gitconfig
```

**Если на компе работает несколько человек, то вместо файлов /git/<сервер_или_компания>/<сервер_или_пользователь>/.gitconfig, лучше наделать все конфиги 
в ~/, типа ~/.gitconfig.company2.gitlab.company2 и ~/.gitconfig.company2.bb.company2**

## Настройка отдельных ключей SSH для разных серверов

Использовать HTTPS для доступа к git-серверам неудобно и непрофессионально. Учимся работать по SSH. Для нерабочих учеток на публичных серверах мы можем
использовать один ключ. Но для каждой компании генерируем отдельный ключ. Чтобы удобно работать с обычными и Git-серверами по SSH, надо добавить настройки
в файл ~/.ssh/config

```shell
# cat ~/.ssh/config

# GitLab.com личная учетка PrivateUser
Host gitlab.com
  User PrivateUser
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/PrivateUser

# GitHub.com личная учетка PrivateUser
Host github.com
  User PrivateUser
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/PrivateUser

# company1, gitlab.company1.ru, корпоративная учетка WorkUser1 (настройка одной учетки на все сервера)
Host *.company1.ru
  User WorkUser1
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/WorkUser1

# company2, gitlab.company2.ru, корпоративная учетка WorkUser1 (настройка конкретной учетки на конкретный сервер)
Host gitlab.company2.ru
  User WorkUser1
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/WorkUser1

# company2, bb.company2.ru, корпоративная учетка WorkUser2 (настройка конкретной учетки на конкретный сервер)
Host bb.company2.ru
  User WorkUser2
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/WorkUser2

```

