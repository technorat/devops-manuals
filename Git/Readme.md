# Дока по Git

## Оглавление

* [Некоторые команды Git](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Commands.md)
* [Подготовка машины к работе с несколькими серверами и проектами](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Start_configure.md)
* [Баги и как бороться с ними](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Bugs.md)
* [Некоторые заметки из официальной доки](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/Shorts.md)
* [Часто задаваемые вопросы (ЧАВО/FAQ) по Git](https://gitlab.com/technorat/devops-manuals/-/blob/main/Git/Docs/FAQ.md)



