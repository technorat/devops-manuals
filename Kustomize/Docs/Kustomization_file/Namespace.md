

Поле namespace предписывает kustomize'у добавлять заданное пространство имен ко всем ресурсам. В данном случае, Deployment и Service попадут в пространство имен development.