

Поле namePrefix предписывает kustomize'у добавлять определенный префикс (в данном случае — dev-) к атрибуту name всех ресурсов, определенных в поле resources. Таким образом, если в Deployment'е есть name со значением nginx-deployment, kustomize сделает из него dev-nginx-deployment.