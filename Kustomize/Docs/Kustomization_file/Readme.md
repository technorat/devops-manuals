# [Файл kustomization.yaml](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/)

На практике, файлы kustomization.yaml могут содержать только часть указанной ниже информации. Но в идеале, этот манифест содержит 4 списка: resources, generators, transformers, validators. Выглядит минимальный шаблон манифеста [так](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Examples/Clear_basic_template.md?ref_type=heads). 

## Поля

* [bases](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Bases.md?ref_type=heads)

* [buildMetadata](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/BuildMetadata.md?ref_type=heads)

* [commonAnnotations](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/CommonAnnotations.md?ref_type=heads)

* [commonLabels](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/CommonLabels.md?ref_type=heads)

* [components](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/CommonLabels.md?ref_type=heads)

* [configMapGenerator](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/ConfigMapGenerator.md?ref_type=heads)

* [crds](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Crds.md?ref_type=heads)

* [generatorOptions](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/GeneratorOptions.md?ref_type=heads)

* [helmCharts](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/GeneratorOptions.md?ref_type=heads)

* [images](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Images.md?ref_type=heads)

* [labels](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Labels.md?ref_type=heads)

* [namePrefix](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/NamePrefix.md?ref_type=heads)

* [namespace](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Namespace.md?ref_type=heads)

* [nameSuffix](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/NameSuffix.md?ref_type=heads)

* [openapi](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/NameSuffix.md?ref_type=heads)

* [patches](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Patches.md?ref_type=heads)

* [patchesJson6902](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Patches.md?ref_type=heads)

* [patchesStrategicMerge](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Patches.md?ref_type=heads)

* [replacements](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Replacements.md?ref_type=heads)

* [replicas](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Replicas.md?ref_type=heads)

* [resources](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Resources.md?ref_type=heads)

* [secretGenerator](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/SecretGenerator.md?ref_type=heads)

* [sortOptions](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/SortOptions.md?ref_type=heads)

* [vars](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Docs/Kustomization_file/Vars.md?ref_type=heads)
