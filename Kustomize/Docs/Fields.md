# Поля в файле kustomization.yaml

### commonLabels

Содержит набор лейблов, который будет добавлен ко всем ресурсам. В [примере](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Examples/deployment-svc.md) kustomize присвоит ресурсам метку с названием environment и значением "development".

### namePrefix

Предписывает kustomize'у добавлять определенный префикс. В [примере](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Examples/deployment-svc.md) это "dev-" к атрибуту name всех ресурсов, определенных в поле resources. Таким образом, если в Deployment'е есть name со значением nginx-deployment, kustomize сделает из него "dev-nginx-deployment".

### namespace

Предписывает kustomize'у добавлять заданное пространство имен ко всем ресурсам. В [примере](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Examples/deployment-svc.md), Deployment и Service попадут в пространство имен "development".

### resources

Указывает, что (какие ресурсы) будет менять kustomize. В [примере](https://gitlab.com/technorat/devops-manuals/-/blob/main/Kustomize/Examples/deployment-svc.md) он будет искать базовые ресурсы файлов deployment.yaml и service.yaml, лежащие в каталоге "base".

