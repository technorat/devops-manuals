# Установка Kustomize

## Установка из бинарников

Скачиваем файл скрипта, который определяет версию и в папку запуска скачивает нужный бинарный файл.

```shell
# cd /usr/bin
# curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
```