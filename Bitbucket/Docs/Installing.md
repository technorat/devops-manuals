# Установка Bitbucket

## Оглавление

* Требования к софту и железу
* Установка вручную
*

## Требования к софту и железу

* Архитектура CPU: x86-64
* Количество ядер CPU: рекомендуемый минимум - 4, желательно больше.
* Объем ОЗУ: рекомендуемый минимум - 16 Гб, желательно больше.
* ОС: Linux
* Java: 17
* Библиотека glibc: >2.28
* Perl: 5.8.8+
* OpenSSH: 7.2+
* Git – server: 2.31.x - 2.42.x
* Git – client: 1.6.6+


**Совместимость с БД**
* PostgreSQL: 13-16
* Microsoft SQL Server: 2014-2022
* Oracle: 19

**Хранение паролей/секретов**
* HashiCorp Vault - KV Secrets Engine V2 (единственная версия, которую можно использовать).

**CI/CD**
* Встроенный CI/CD: Bitbucket Pipelines
* Jenkins: 2.162+ (Для интеграции требуется [плагин интеграции Bitbucket](https://plugins.jenkins.io/atlassian-bitbucket-server-integration/) для Jenkins. Чтобы получить доступ к полному 
набору интегрированных функций CI/CD, вам необходимо [настроить ссылку на приложение](https://confluence.atlassian.com/bitbucketserver/link-bitbucket-with-jenkins-1032258003.html).
* Bamboo: 5.6+

**Поисковый движок**
* OpenSearch: 1.2-1.3
* OpenSearch (Amazon OpenSearch Service): 1.1-1.3
* ~~Elasticsearch~~: Deprecated

## Примечания

* Установщик Bitbucket Data Center включает Java (JRE) и Tomcat, поэтому вам не нужно устанавливать их отдельно.
* Вам следует использовать установщик Linux, если вы хотите запустить Bitbucket Data Center как службу.
* Вы можете запускать и останавливать Bitbucket Data Center, запустив файл start-bitbucket.sh в каталоге установки Bitbucket Data Center.
* Bitbucket Data Center будет запускаться от имени учетной записи пользователя, которая использовалась для установки Bitbucket Data Center, или вы можете выбрать запуск от имени выделенного 
пользователя "bitbucket". Bitbucket Data Center необходимо будет перезапустить вручную, если ваш сервер перезапускается.
* Bitbucket Data Center работает на портах 7990, 7992, 7993
* Для запуска Bitbucket в рабочей среде вам понадобится внешняя база данных.
* Bitbucket Data Center требует наличия Git на компьютере, на котором будет работать Bitbucket Data Center.

## Установка вручную

1. **Создаем выделенную системную технологическую учетку пользователя "bitbucket".**

```shell
# useradd --create-home --home-dir /home/bitbucket --system --shell /bin/bash bitbucket
```

2. **Установим и настроим Git**

```shell
# add-apt-repository ppa:git-core/ppa
# apt update
# apt install git
# git config --global user.name "Technorat"
# git config --global user.email sergey_privacy@mail.ru
```

3. **Установим пакеты, необходимые для работы Bitbucket**

```shell
# apt install -y postgresql apache2 apt-transport-https certbot python3-certbot-apache fontconfig
```

4. **Настроим Apache для последующей работы с Bitbucket.**
Включаем модули веб-сервера Apache под названием “proxy_http", “rewrite”.

```shell
# a2enmod proxy_http
# a2enmod rewrite
# systemctl enable apache2
```

5. **Создаем конфиги апача для битбакета**
Потом рестарт апача

```shell
# cat /etc/apache2/sites-available/bitbucket.darksoft.local.conf
<VirtualHost *:80>
    ServerAdmin sergey_privacy@mail.ru
    ServerName bitbucket.darksoft.local
</VirtualHost>

 cat /etc/apache2/sites-available/bitbucket.darksoft.local-ssl.conf
<IfModule mod_ssl.c>
<VirtualHost *:443>
    ServerAdmin sergey_privacy@mail.ru
    ServerName bitbucket.darksoft.local

    ProxyRequests Off

    <Proxy *>
    Require all granted
    </Proxy>

    ProxyPass / http://localhost:7990/
    ProxyPassReverse / http://localhost:7990/

    SSLEngine On
    SSLCertificateFile /etc/letsencrypt/live/bitbucket.darksoft.local/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/bitbucket.darksoft.local/privkey.pem
    Include /etc/letsencrypt/options-ssl-apache.conf
</VirtualHost>
</IfModule>
# systemctl restart apache2
```

6. **Активируем созданные виртуальные хосты**

```shell
# a2ensite /etc/apache2/sites-available/bitbucket.darksoft.local.conf
# a2ensite /etc/apache2/sites-available/bitbucket.darksoft.local-ssl.conf
```

7. **Деактивируем виртуальный хост, созданный по умолчанию**

```shell
# a2dissite 000-default.conf
```

8. **Добавляем в /etc/apache2/apache2.conf параметр ServerName**

```shell
# grep bitbucket.darksoft.local apache2.conf 
ServerName bitbucket.darksoft.local
```

9. **Запросим криптографический сертификат с Let’s Encrypt**
(Этот пункт пока пропускаем, надо сначала открыть пропуск из интернета)

```shell
# certbot --apache -d bitbucket.technorat.ru
```

. **Переключаемся на пользователя “postgres”, который обладает правами администратора в PostgreSQL**

```shell
# su - postgres
```

. **Далее переключаемся на командную строку PostgreSQL**

```shell
# psql
```

. **Создаем нового пользователя bitbucketdbadmin**

```shell
postgres=# CREATE USER bitbucketdbadmin WITH PASSWORD '2221222';
```

. **Создаем новую базу данных и выдаем на нее права пользователю bitbucketdbadmin*

```shell
postgres=# CREATE DATABASE bitbucketdb WITH OWNER "bitbucketdbadmin" ENCODING='UTF8' CONNECTION LIMIT=-1;
```

. **Теперь необходимо загрузить и запустить установщик Bitbucket**
Свежую версию смотрим здесь: https://www.atlassian.com/software/bitbucket/download-archives
Сейчас это 8.18.0

```shell
# cd /tmp
# wget https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-8.18.0-x64.bin
# chmod 777 /tmp/atlassian-bitbucket-8.18.0-x64.bin
# /tmp/atlassian-bitbucket-8.18.0-x64.bin
Unpacking JRE ...
Starting Installer ...

Would you like to install or upgrade an instance?
Install a new instance [1, Enter], Upgrade an existing instance [2]
2024-02-15 13:45:39,022 WARN  [Thread-2]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22998369b6-cad8-50a2-537b-d5a240067e1f%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:45:39 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:45:39,027 WARN  [Thread-2]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="322d4c08-2851-3db4-b188-fde7e01db0b2", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:50:39 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
1

What type of instance are you looking to install?
Install a single node instance [1, Enter], Install a clustered instance [2], Install a mirror instance [3]
2024-02-15 13:46:42,323 WARN  [Thread-3]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22742ca475-617e-fa09-d788-008753017f4a%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:46:42 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:46:42,324 WARN  [Thread-3]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="857e9af3-31c9-40b9-daf5-2e6c70459e3c", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:51:42 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
1


The bundled search server will be installed with this option.

[Enter]

Where should Bitbucket be installed?

Select the folder where you would like Bitbucket 8.18.0 to be installed,
then click Next.
[/opt/atlassian/bitbucket/8.18.0]
2024-02-15 13:47:30,976 WARN  [Thread-7]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%226045e5bd-8a1a-2046-1593-559d1bfeafc1%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:47:30 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:47:30,978 WARN  [Thread-7]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="bbec779b-5c1d-8d99-05a2-781fcdae9baa", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:52:30 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:47:31,060 WARN  [Thread-6]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%2287d3df31-e1b3-7b48-5616-e6474aa135c8%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:47:31 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:47:31,061 WARN  [Thread-6]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="b22f730d-e909-3e33-c81b-8b2385f56c2b", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:52:31 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
/bitbucket

Default location for Bitbucket home directory

The location for Bitbucket data.
This will be the default location for repositories, plugins, and other data.

Ensure that this location is not used by another Bitbucket installation.
[/var/atlassian/application-data/bitbucket]
2024-02-15 13:48:05,854 WARN  [Thread-12]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22e39a873a-614d-30b9-93a0-6a466f7c66a8%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:48:05 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:48:05,856 WARN  [Thread-12]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="c64d66a7-4ae1-7b1c-5d91-8c6915f90353", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:53:05 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

/bitbucket/data

The installation directory cannot be a parent of the home directory. Please choose another location.
Default location for Bitbucket home directory

The location for Bitbucket data.
This will be the default location for repositories, plugins, and other data.

Ensure that this location is not used by another Bitbucket installation.
[/bitbucket/data]
2024-02-15 13:49:32,361 WARN  [Thread-13]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%2229122bf3-9ccb-763d-eebd-13a33727dcd1%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:49:32 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:49:32,361 WARN  [Thread-13]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="ea4f37ba-5332-0975-4f71-b76426067496", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:54:32 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

/bitbucket_data            
Configure which ports Bitbucket will use.

Bitbucket requires a TCP port that isn't being used by other applications.

The HTTP port is where users access Bitbucket through their browsers.

Bitbucket also requires ports 7992 and 7993 are available to run the bundled
search server that provides search functionality to Bitbucket.
HTTP Port Number
[7990]
2024-02-15 13:52:38,307 WARN  [Thread-14]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22cc11d684-ae92-5bdd-8504-3374e6adb275%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:52:38 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:52:38,308 WARN  [Thread-14]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="c0eec34a-3526-a0d8-161c-ca590908708c", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:57:38 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

For a production server we recommend that you run Bitbucket as a
Windows/Linux service because Bitbucket will restart automatically when the
computer restarts.
Install Bitbucket as a service?
Yes [y, Enter], No [n]
2024-02-15 13:53:34,054 WARN  [Thread-15]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22248ed154-3c74-caec-4b62-0c85ecdca86b%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:53:34 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:53:34,055 WARN  [Thread-15]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="f404cce3-36b6-a0e3-7767-a87264104c24", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:58:34 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

y
Please review your Bitbucket installation settings

Installation Directory: /bitbucket 
Home Directory: /bitbucket_data 
HTTP Port: 7990 
Install as a service: Yes 

Install [i, Enter], Exit [e]
2024-02-15 13:53:58,652 WARN  [Thread-16]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22a883f767-7047-c2ef-535f-9a65d66a22ee%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:53:58 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:53:58,653 WARN  [Thread-16]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="5483e560-11ac-8638-cf4a-135e910b33f0", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:58:58 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

Extracting files ...

Would you like to launch Bitbucket?
Yes [y, Enter], No [n]
2024-02-15 13:54:50,989 WARN  [Thread-31]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%227d5a73fa-3e5e-78d1-bd40-6e0f75745c4f%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:54:50 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:54:50,990 WARN  [Thread-31]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="e9e623fb-4be7-a3a8-eb5c-2824899bb61a", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 13:59:50 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

Please wait a few moments while Bitbucket starts up.
Launching Bitbucket ...


2024-02-15 13:55:21,501 WARN  [Thread-34]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22add38576-b770-b04c-b11f-e8c2e0d5132c%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:55:21 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:55:21,501 WARN  [Thread-34]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="4aad1e79-c56f-5fa6-8402-71b4f0226c6f", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 14:00:21 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"


Your installation of Bitbucket 8.18.0 is now ready and can be accessed via
your browser.
Bitbucket 8.18.0 can be accessed at http://localhost:7990
Launch Bitbucket 8.18.0 in browser?
Yes [y, Enter], No [n]
2024-02-15 13:55:21,501 WARN  [Thread-34]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [ajs_anonymous_id="%22add38576-b770-b04c-b11f-e8c2e0d5132c%22", version:0, domain:wac-web, path:/, expiry:Sun Feb 12 13:55:21 MSK 2034] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"
2024-02-15 13:55:21,501 WARN  [Thread-34]  o.a.h.c.p.ResponseProcessCookies Cookie rejected [bxp_gateway_request_id="4aad1e79-c56f-5fa6-8402-71b4f0226c6f", version:0, domain:wac-web, path:/, expiry:Thu Feb 15 14:00:21 MSK 2024] Illegal 'domain' attribute "wac-web". Domain of origin: "www.atlassian.com"

    n
Finishing installation ...

```

**Создаем конфиг-файл /bitbucket_data/shared/bitbucket.properties**

```
server.port=7990
server.secure=false
#server.secure=true # Whether the connector is secured. Note that setting this to "true" does not enable SSL; SSL is enabled using server.ssl.enabled instead. One use case for setting this to "true" is when the system is behind a reverse proxy and SSL is terminated at the proxy.
server.scheme=http
#server.scheme=https # The connector scheme, either "http" or "https". Defaults to "http" unless "secure" is set to "true"; then it defaults to "https". In general, this property should not need to be set.
server.proxy-port=7990
#server.proxy-port=443 # The proxy port, used to construct proper redirect URLs when a reverse proxy is in use. If a proxy port is not set, but a name is, the connector's port will be used as the default.
server.proxy-name=bitbucket.darksoft.local
```

**Рестартуем битбакет**

```
# systemctl restart atlbitbucket
```

**Заходим по адресу веб-морды битбакета (у меня )**
Настраиваем параметры БД, генерим тестовый лицензионный ключ. Задаем параметры админа и другие

Получаем лицензию

SEN 			Product 			Name 		Support Expires 	Support
SEN-L20101631 		Bitbucket (Data Center): Trial 	Sibontek-soft 	19 мар 2024 		Request Support
Server ID: B6HB-OKAV-WSHG-BSH8
SEN: SEN-L20101631

License Key:
    
AAABmQ0ODAoPeNp9Ul1v2jAUffeviLSXTZM7JxnhQ7LUxIQFFchad6UPSJOTXYoFBOaPdNmvnyFB3
Sp1b/bxufece67fcVt5Eyg8f+CRwSjsj3o9j/F7LyDBZ8QUCCMP1VgYoCcEkwD7QzQGXSp5PD3RR
JrCllsw3ntHEx6DyoD6sBp5aS129lyP5kI6tBJVCemvo1TNXx1DHBCkjdCbK26EcsV0LXYa0EyWU
On/FojSyBqoURbQrqU/gNInXwHitnixeaa8OGrvuXoSldQtwmVxcCa3WB/WBjF3dt1T53xHNagna
L4flaxF2VzvHXal7EUxc07onD2zSczIIhfy8Kw/RU0vNPH6rn+7X0LakHhze7O09eQ3M1GQbIbbp
R7+fMx+BLH+WMYruqLdSAu7L0Dl629OVFPsX2KYjulsOubpAs8C4hM/Cv2uoiPcN0dYiD1Qls/n6
R2bxrOOcNpLu5Z2bA6qBuUaJlGW4PwmfsBLnn3BCc8GaAvNJUI/IqRPBqFT+mpVuREaXv+ELqaz7
hspOcv0X9t/ADbO0TgwLQIVAI+resoQ0gScDLtf0OfmdDPlJR6tAhRuiU7cvNj56oHQPIHUPDVTQ
d4FUQ==X02jr








