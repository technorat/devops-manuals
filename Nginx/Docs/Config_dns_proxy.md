# Настройка Nginx для перенаправления запросов с домена aaa.bbb.ccc на IP:Port

1. Устанавливаем Nginx

2. Создадим пользователя "nginx". Настроим запуск юнита от его имени

3. Создадим каталог для логов Nginx /var/log/nginx

4. В каталоге /etc/nginx/sites-available создаем конфиг-файл с именем как у нужного домена (aaa.bbb.ccc) /etc/nginx/sites-available/aaa.bbb.ccc

5. В каталоге /etc/nginx/sites-enabled создаем ссылку на созданный в предыдущем пункте конфиг-файл 

```
# ln -s /etc/nginx/sites-available/aaa.bbb.ccc /etc/nginx/sites-enabled/aaa.bbb.ccc