# Inventory - файл инвентаря компьютеров и общие переменные

## Что это и зачем?
Файл инвентаря содержит имена машин, с которыми будет работать Ansible. Он содержит имя, адрес, может содержать какие то групповые или специфичные для машины переменные.
Есть несколько форматов написания инвентаря, я расскажу про один, самый удобный для меня. Остальные можете изучить сами в официальной документации. 
Файл инвентаря и описания переменных правильно хранить в подпапке, соответствующей типу окружения. В папке /Ansible создается папка /Ansible/environments и внутри 
нее папки, соответствующие названиям окружений: dev, uat, prod, preprod. Обычно файл инвентаря называется inventory.ini

