# Переменные

## Где могут появляться переменые?

Переменные в Ansible могут создаваться автоматически (получать их с хоста) или объявляться в различных файлах:

1. Переменные в командной строке (например "-u <username>")
2. **Role defaults (role/defaults/main.yaml)**
3. Inventory file or script group vars
4. **Inventory group_vars/all**
5. Playbook group_vars/all
6. **Inventory group_vars/***
7. Playbook group_vars/*
8. Inventory файл или script host vars
9. **Inventory host_vars/***
10. Playbook host_vars/*
11. **Host facts, cashed set_facts**
12. Play vars
13. Play vars_promt
14. Play vars_files
15. Role vars (/role/vars/main.yaml)
16. Block vars (только для задач в блоке)
17. **Task vars (только для задач в таске)**
18. **Include_vars**
19. **Set_facts/registered vars**
20. Role (и include_role) params
21. Include params
22. **Extra vars в команной строке (например -e "env=prod")**
