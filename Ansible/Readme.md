# Ansible

## Оглавление
### Основы
- [Основные определения](https://gitlab.com/technorat/devops-manuals/-/blob/main/Ansible/docs/Basics.md)
- [Inventory - файл инвентаря компьютеров и общие переменные](https://gitlab.com/technorat/devops-manuals/-/blob/main/Ansible/docs/Inventory.md)
- [Структура папок](https://gitlab.com/technorat/devops-manuals/-/blob/main/Ansible/docs/Directory_structure.md)
